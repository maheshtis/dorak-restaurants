<?php

/**
 * Generates link to a Module CSS file
 * With the help of this function you can add a style sheet
 * @access	public
 * @param	stylesheet, module
 * @return	link your stylesheet 
 */
function load_module_stylesheet($module, $stylesheet) {
    $css_path = '/assets/css/';
    $object = &get_instance();
    $style = '';
    if (is_array($stylesheet)) {
        foreach ($stylesheet as $stylesheet) {
            $style .= link_tag($module . $css_path . $stylesheet);
        }
    } else {
        $style = link_tag($module . $css_path . $stylesheet);
    }

    return $style;
}

//----------------------------------------------------------------------------------------------
/*
 * Generates link to a Module script file
 * With the help of this function you can add a script
 * @access	public
 * @param	script file name
 * @return	link your js 
 */
if (!function_exists('load_module_script')) {

    function load_module_script($module, $script) {
//	echo "jeevan";die;
        $js_path = '/assets/js/';
        $object = &get_instance();
        $script_file = '';
        if (is_array($script)) {
            foreach ($script as $script) {
                $script_file .= script_tag($module . $js_path . $script);
            }
        } else {
            $script_file = script_tag($module . $js_path . $script);
        }
        return $script_file;
    }

}

if (!function_exists('script_tag')) {

    function script_tag($src = '', $language = 'javascript', $type = 'text/javascript', $index_page = FALSE) {
        $CI = & get_instance();
        $script = '<script ';

        if (is_array($src)) {
            foreach ($src as $k => $v) {
                if ($k == 'src' AND strpos($v, '://') === FALSE) {
                    if ($index_page === TRUE) {
                        $script .= 'src="' . $CI->config->site_url($v) . '" ';
                    } else {
                        $script .= 'src="' . $CI->config->slash_item('base_url') . $v . '" ';
                    }
                } else {
                    $script .= "$k=\"$v\" ";
                }
            }
            $script .= "></script>";
        } else {
            if (strpos($src, '://') !== FALSE) {
                $script .= 'src="' . $src . '" ';
            } elseif ($index_page === TRUE) {
                $script .= 'src="' . $CI->config->site_url($src) . '" ';
            } else {
                $script .= 'src="' . $CI->config->slash_item('base_url') . $src . '" ';
            }
            $script .= 'language="' . $language . '" type="' . $type . '" ';

            $script .= '></script>';
        }
        return $script;
    }

}
/**
 * Link
 * Generates link to a CSS file
 * @access	public
 * @param	mixed	stylesheet hrefs or an array
 * @param	string	rel
 * @param	string	type
 * @param	string	title
 * @param	string	media
 * @param	boolean	should index_page be added to the css path
 * @return	string
 */

if (!function_exists('link_tag')) {

    function link_tag($href = '', $rel = 'stylesheet', $type = 'text/css', $title = '', $media = '', $index_page = FALSE) {
        $CI = & get_instance();

        $link = '<link ';

        if (is_array($href)) {
            foreach ($href as $k => $v) {
                if ($k == 'href' AND strpos($v, '://') === FALSE) {
                    if ($index_page === TRUE) {
                        $link .= 'href="' . $CI->config->site_url($v) . '" ';
                    } else {
                        $link .= 'href="' . $CI->config->slash_item('base_url') . $v . '" ';
                    }
                } else {
                    $link .= "$k=\"$v\" ";
                }
            }

            $link .= "/>";
        } else {
            if (strpos($href, '://') !== FALSE) {
                $link .= 'href="' . $href . '" ';
            } elseif ($index_page === TRUE) {
                $link .= 'href="' . $CI->config->site_url($href) . '" ';
            } else {
                $link .= 'href="' . $CI->config->slash_item('base_url') . $href . '" ';
            }

            $link .= 'rel="' . $rel . '" type="' . $type . '" ';

            if ($media != '') {
                $link .= 'media="' . $media . '" ';
            }

            if ($title != '') {
                $link .= 'title="' . $title . '" ';
            }

            $link .= '/>';
        }
        return $link;
    }

}
?>