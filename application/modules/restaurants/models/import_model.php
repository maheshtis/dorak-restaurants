<?php
/* 
 * @author :   Original Author TIS
 * @author :   Another Author <jeevan@jeevan@tisindiasupport.com>
 * @date   :   28-mar-2016
 * @functions : 
    addCountry() :                         Data record in the form of array inserted in DB                        
	getCountryCode()                        Country code is fetched from the DB. 
	getCountryId()                      this method used to return country id from countries table
	addCity()                        this method used to add country city in country_cities table
	getCityId()                        this method is used to fetch country city in country_cities table using city name and country code
	getDistrictId()                  this method is used to fetch district id using district name and city id from city_districts table
	addDistrict()                      this method is used to fetch district id using district name and city id from city_districts table
	getRestaurantDetailsBycode()            this method is used to fetch restaurant id using restaurant code from restaurants table
	updateRestaurantDetails()              this method is used to update restaurant details in restaurants table
	getRestaurantDetails()                  this method is used to fetch restaurant id using restaurant code from restaurants table
	insertDetails()                     this method used to add restaurant record in restaurants table
	addAgeGroup()                      this method used to add group records record in entity_attributes table
	getAgeGroup()                     this method used to fetch age group id record from entity_attributes table
	addPurpose()                      this method used insert purpose into entity_attributes table
	getPurpose()                       this method used fetch purpose from entity_attributes table
	getPropertyTypeId()                this method used fetch property type id from entity_attributes table
	addPropertyType()                    this method used insert property type into entity_attributes table
	getPaymentPlan()                   this method used fetch payment plan from entity_attributes table
	getRenovationName()                this method used fetch renovation id from renovation_types table
	addRenovationName()                 this method used insert renovation types into renovation_types table
	addPaymentPlan()                     this method used insert payment plan into entity_attributes table
	getCancelBefore()                    this method used fetch cancellation id into entity_attributes table
	addCancelBefore()                     this method used insert cancellation title into entity_attributes table
	deleteRestaurantPaymentShedules()          this method used delete restaurant payment schedule from rest_payment_shedules tabl
	importRestaurantPaymentShedules()          this method used import restaurant payment schedule from rest_payment_shedules table
	addRestaurantChain()                       this method used to insert restaurant chain into entity_attributes table
	getRestaurantChain()                       this method method used to fetch restaurant chain from entity_attributes table
	addPositions()                       this method method used to insert positions into entity_attributes table
	getPositions()                        this method method used to insert positions into entity_attributes table
	deleteRestaurantContactInfo()               this method method used to insert positions into entity_attributes table
	importRestaurantContactInfo()               this method method used to insert restaurant contact info into rest_contact_details table
	addComplimentaryService()              this method method used to insert restaurant complimentary service into entity_attributes table
	getComplimentaryService()              this method method used to fetch restaurant complimentary service from entity_attributes table
	deleteComplimentaryServices()           this method method used to remove complimentary service from rest_complimentary_services table
	importComplimentaryServices()          this method method used to insert complimentary service into rest_complimentary_services table
	importRestaurantContract()                  this method method used to insert restaurant contract information into rest_contract_info table
	deleteRestaurantComlimentaryRoom()          this method method used to delete restaurant complimentary services from rest_complimentary_room table
	importRestaurantComlimentaryRoom()          this method method used to insert restaurant complimentary room into rest_complimentary_room table
	importRestaurantComlimentaryRoomExcludedDate() this method method used to insert restaurant complimentary room into rest_cmplimntry_room_excluded_dates table
	addRestaurantPropertyTypes()                   this method method used to insert restaurant property types into rest_property_types table
	deleteRestaurantBankingInfo()                this method method used to delete restaurant banking information from rest_bank_accounts table
	importRestaurantBankingInfo()                this method method used to import restaurant banking information into rest_bank_accounts table
	deleteRestaurantCancellation()                 this method method used to delete restaurant cancellation from rest_cancellation table
	importRestaurantCancellation()               this method  used to import restaurant cancellation into rest_cancellation table
	deleteRestaurantRenovationSchedule()         this method  used to delete restaurant renovation schedule from rest_renovation_schedule table
	importRestaurantRenovationSchedule()         this method  used to insert restaurant renovation schedule into rest_renovation_schedule table
	deleteRestaurantDistanceFrom()               this method  used to delete restaurant distance from rest_distance table
	importRestaurantDistanceFrom()                this method  used to import restaurant distance from rest_distance table
	addFacility()                            this method  used to add restaurant facility into entity_attributes table
	getFacility()                            this method  used to fetch restaurant facility id from entity_attributes table
	deleteRestaurantFacilities()                    this method  used to delete restaurant facility from rest_facilities table
	importRestaurantFacilities()                    this method  used to delete restaurant facility from rest_facilities table
	getRestaurantRoomsPricingData()                 this method  used to fetch restaurant pricing from rest_rooms_pricing table
	editRestaurantRoomsPricingData()              this method  used to update restaurant pricing from rest_rooms_pricing table
	importRestaurantRoomsPricingData()            this method  used to import restaurant room pricing data into rest_rooms_pricing table
	deleteRoompricing()                      this method  used to delete room pricing data from rest_rooms_pricing table
	addRestaurantRoomPricingComplimentary()        this method  used to insert restaurant room pricing complimentary into restaurant_rooms_pricing_complimentary table
	deleteRoomPricingComplimentary()          this method  used to delete restaurant room pricing complimentary from restaurant_rooms_pricing_complimentary table
	importRestaurantRroomsPricingDetails()          this method  used to import restaurant room pricing details into rest_rooms_pricing_details table
	updateRestaurantRroomsPricingDetails()          this method  used to update restaurant room pricing details into rest_rooms_pricing_details table
	addRoomType()                              this method  used to insert restaurant room type into entity_attributes table
	getRoomTypeId()                            this method  used to insert rest room type into entity_attributes table
	addMarket()                                this method  used to insert restaurant market type into entity_attributes table
	getMarket()                                this method  used to insert restaurant market into entity_attributes table
	get_room_types()                           this method  used to fetch restaurant room type details from rest_rooms_pricing table
	
 * @description : Imports all data into the database related with restaurants attributes.
 */
class Import_model extends AdminModel {

    function __construct() {
        // Call the Parent Model constructor
        parent::__construct();
    }
    
 /*
  * @method string addCountry(),it updates country table with provided value.
  * @param array $data is inserted in the DB.
  * @todo Data record in the form of array inserted in DB
  * @return Returns last inserted id (numerical value) of added country.
  */
    function addCountry($data) {
        if ($this->db->insert('countries', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
    
 /*
  * @method string Fetches countrycode in string value.
  * @param Country code is fetched through int $c_code.
  * @todo  Country code is fetched from the DB. 
  * @return Provides Country code list from  the database.
  */

    function getCountryCode($c_code) {
        $this->db->select('country_code');
        $this->db->where("country_code", $c_code);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->country_code;
        } else {
            return false;
        }
    }
/**
     * @method getCountryId() method used for get country id using country code
	 * @param int $c_code holds country code in the form of an array through which country id is fetched.
	 * @todo this method used to return country id from countries table
     * @return will return country id in the fom of an array OR return false
     */
    function getCountryId($c_code) {
        $this->db->select('id');
        $this->db->where("country_code", $c_code);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addCity() method used to add city in country_cities table 
	 * @param int $data hold country city details in the form of an array.
	 * @todo this method used to add country city in country_cities table
     * @return id|false
     */
    function addCity($data) {
        if ($this->db->insert('country_cities', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getCityId() method used to get city in country_cities table using city name or country code
	 * @param string $cityName holds country city name in DB table.
	 * @param string $country_code holds country code for fetching city details.
	 * @todo this method is used to fetch country city in country_cities table using city name and country code
     * @return id|false
     */
    function getCityId($cityName, $country_code = '') {
        $this->db->select('id');
        $this->db->where("city_name", $cityName);
        if ($country_code != "")
            $this->db->where("country_code", $country_code);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method getDistrictId() method used to fetch id from city_districts table
	 * @param string $district_name holds country city name details.
	 * @param int $cityId holds city id in DB table.
	 * @todo this method is used to fetch district id using district name and city id from city_districts table
     * @return will return id in the form of an array|false
     */
    function getDistrictId($district_name, $cityId) {
        $this->db->select('id');
        $this->db->where("city_id", $cityId);
        $this->db->where("district_name", $district_name);
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addDistrict() method used to add district from city_districts table
	 * @param array $data hold district name in the form of an array
	 * @todo this method is used to fetch district id using district name and city id from city_districts table
     * @return will return id |false
     */
    function addDistrict($data) {
        if ($this->db->insert('city_districts', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getRestaurantDetailsBycode() method used to fetch restaurant id 
	 * @param string $restcode holds restaurant code in DB.
	 * @todo this method is used to fetch restaurant id using restaurant code from restaurants table
     * @return will return rest_id |false
     */
    function getRestaurantDetailsBycode($restcode) {
        $this->db->select('rest_id');
        $this->db->where('rest_code', $restcode);
        $query = $this->db->get('restaurants');
        if ($query->num_rows() > 0) {
            return $query->row()->rest_id;
        } else {
            return false;
        }
    }

		/**
     * @method updateRestaurantDetails() method used to update restaurant record using restaurant id
	 * @param int $hid hold restaurant id - restaurant details updated using restaurant id.
	 * @param array() $data hold  restaurant record in the form of an array
	 * @todo this method is used to update restaurant details in restaurants table
     */
    function updateRestaurantDetails($hid, $data) {
        $this->db->where('rest_id', $hid);
        $this->db->update('restaurants', $data);
    }
/**
     * @method getRestaurantDetails() method used to fetch restaurant id using restaurant name,restaurant city and post code.
	 * @param string $hname hold restaurant name - contains restaurant details in DB table.
	 * @param string $hcity hold restaurant city - city name in DB.
	 * @param string $postcode hold restaurant post code
	 * @todo this method used to fetch restaurant id using restaurant name,restaurant city and post code from restaurants table
	 * @return restaurant id in the form of an array OR return false
     */
    function getRestaurantDetails($hname, $hcity, $postcode) {
        $this->db->select('rest_id');
        $this->db->where('rest_name', $hname);
        $this->db->where('city', $hcity);
        $this->db->where('post_code', $postcode);
        $query = $this->db->get('restaurants');
        if ($query->num_rows() > 0) {
            return $query->row()->rest_id;
        } else {
            return false;
        }
    }

/**
     * @method insertDetails() method used to add restaurant details.
	 * @param array() $data holds restaurant records in the form of an array
	 * @todo this method used to add restaurant record in restaurants table
	 * @return restaurant id |return false
     */

    function insertDetails($data) {
        if ($this->db->insert('restaurants', $data)) {
            return $this->db->insert_id();
        }
		echo $this->db->last_query();
        return false;
    }
/**
     * @method addAgeGroup() method used to add age group 
	 * @param array() $data holds age group records in the form of an array
	 * @todo this method used to add group records record in entity_attributes table
	 * @return restaurant id | false
     */
    function addAgeGroup($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getAgeGroup() method used to fetch age group id
	 * @param string $title holds age group title in db table.
	 * @todo this method used to fetch age group id record from entity_attributes table
	 * @return restaurant id | false
     */
    function getAgeGroup($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_child_group'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addPurpose() method used to insert purpose
	 * @param array() $data holds purpose in the form of an array in DB.
	 * @todo this method used insert purpose into entity_attributes table
	 * @return restaurant id | false
     */
    function addPurpose($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getPurpose() method used to fetch purpose
	 * @param string $title holds purpose name in DB table.
	 * @todo this method used fetch purpose from entity_attributes table
	 * @return id | false
     */
    function getPurpose($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_resturant_purpose'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method getPropertyTypeId() method used to fetch property type id
	 * @param string $title holds property type name in DB table.
	 * @todo this method used fetch property type id from entity_attributes table
	 * @return id | false
     */
    function getPropertyTypeId($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addPropertyType() method used to insert property type detail
	 * @param string $title hold property type name in DB table.
	 * @todo this method used insert property type into entity_attributes table
	 * @return id | false
     */
    function addPropertyType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getPaymentPlan() method used to fetch payment plan id
	 * @param string $title hold payment plan name in DB table.
	 * @todo this method used fetch payment plan from entity_attributes table
	 * @return id | false
     */
    function getPaymentPlan($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method getRenovationName() method used to fetch renovation id
	 * @param string $title hold renovation_type data in DB.
	 * @todo this method used fetch renovation id from renovation_types table
	 * @return id | false
     */
    function getRenovationName($title) {
        $this->db->select('id');
        $this->db->where("renovation_type", $title);
        $query = $this->db->get('renovation_types');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addRenovationName() method used to insert renovation types
	 * @param array() $data holds renovation_types in the form of an array in DB.
	 * @todo this method used insert renovation types into renovation_types table
	 * @return id | false
     */
    function addRenovationName($data) {
        if ($this->db->insert('renovation_types', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method addPaymentPlan() method used to insert payment plan
	 * @param array() $data holds payment plan in the form of an array.
	 * @todo this method used insert payment plan into entity_attributes table
	 * @return id | false
     */
    function addPaymentPlan($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getCancelBefore() method used to fetch cancellation before id
	 * @param string $data holds cancellation title data in DB.
	 * @todo this method used fetch cancellation id into entity_attributes table
	 * @return id | false
     */
    function getCancelBefore($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_duration_cancellation'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addCancelBefore() method used to insert cancellation before
	 * @param array() $data holds cancellation title in the form of an array
	 * @todo this method used insert cancellation title into entity_attributes table
	 * @return id | false
     */
    function addCancelBefore($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method deleteRestaurantPaymentShedules() method used to delete restaurant payment schedule
	 * @param int $hid hold restaurant id - holds payment schedule value.
	 * @todo this method used delete restaurant payment schedule from rest_payment_shedules table
	 * @return id | false
     */
    function deleteRestaurantPaymentShedules($hid) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_payment_shedules');
        }
        return;
    }
/**
     * @method importRestaurantPaymentShedules() method used to import restaurant payment schedule
	 * @param array() $data holds restaurant payment schedule record in DB.
	 * @todo this method used import restaurant payment schedule from rest_payment_shedules table
	 * @return true | false
     */
    function importRestaurantPaymentShedules($data) {
        if ($this->db->insert_batch('rest_payment_shedules', $data)) {
            return true;
        }
        return false;
    }
/**
     * @method addRestaurantChain() method used to insert restaurant chain
	 * @param array() $data holds restaurant chain records in the form of an array in DB.
	 * @todo this method used to insert restaurant chain into entity_attributes table
	 * @return true | false
     */
    function addRestaurantChain($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getRestaurantChain() method used to fetch restaurant chain
	 * @param string $title holds restaurant chain title - name of restaurant chain title.
	 * @todo this method method used to fetch restaurant chain from entity_attributes table
	 * @return id | false
     */
    function getRestaurantChain($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_resturant_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addPositions() method used to insert positions
	 * @param array() $data holds positions in the form of an array
	 * @todo this method method used to insert positions into entity_attributes table
	 * @return id | false
     */
    function addPositions($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getPositions() method used to fetch positions id
	 * @param string $title hold position  - name of positions.
	 * @todo this method method used to insert positions into entity_attributes table
	 * @return will return id in the form of an array | false
     */
    function getPositions($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_resturant_positions'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }

/**
     * @method deleteRestaurantContactInfo() method used to delete restaurant contact info
	 * @param int $hid hold restaurant id - holds daa in array form in DB.
	 * @todo this method method used to insert positions into entity_attributes table
	 * @return will return id in the form of an array | false
     */

    function deleteRestaurantContactInfo($hid) {

            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_contact_details');
        
    }
/**
     * @method importRestaurantContactInfo() method used to import restaurant contact info
	 * @param int $hid hold restaurant id
	 * @todo this method method used to insert restaurant contact info into rest_contact_details table
	 * @return true | false
     */
    function importRestaurantContactInfo($data) {
        if ($this->db->insert_batch('rest_contact_details', $data)) {
            return true;
        }
        return false;
    }


/**
     * @method addComplimentaryService() method used to insert complimentary service
	 * @param array() $data holds restaurant complimentary services in the form of an array
	 * @todo this method method used to insert restaurant complimentary service into entity_attributes table
	 * @return id | false
     */
    function addComplimentaryService($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getComplimentaryService() method used to fetch complimentary service id
	 * @param string $title holds restaurant complimentary service name records.
	 * @todo this method method used to fetch restaurant complimentary service from entity_attributes table
	 * @return id | false
     */
    function getComplimentaryService($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
/**
     * @method deleteComplimentaryServices() method used to remove complimentary service
	 * @param int $hid hold restaurant id - comp services details in the table DB.
	 * @todo this method method used to remove complimentary service from rest_complimentary_services table
	 * @return true | false
     */
    function deleteComplimentaryServices($hid) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_complimentary_services');
        }
        return;
    }
/**
     * @method importComplimentaryServices() method used to insert complimentary service data 
	 * @param array() $data hold complimentary service in the form of an array
	 * @todo this method method used to insert complimentary service into rest_complimentary_services table
	 * @return true | false
     */
    function importComplimentaryServices($data) {
        if ($this->db->insert_batch('rest_complimentary_services', $data)) {
            return true;
        }
        return false;
    }

  /**
     * @method importRestaurantContract() method used to insert restaurant contract information
	 * @param array() $data holds restaurant contract information in the form of an array.
	 * @todo this method method used to insert restaurant contract information into rest_contract_info table
	 * @return true | false
     */

    function importRestaurantContract($data) {
        if ($this->db->insert_batch('rest_contract_info', $data)) {
            return true;
        }
        return false;
    }

 /**
     * @method deleteRestaurantComlimentaryRoom() method used to delete restaurant complimentary services
	 * @param int $hid hold restaurant id - details in the form of array.
	 * @todo this method method used to delete restaurant complimentary services from rest_complimentary_room table
	 * @return true | false
     */

    function deleteRestaurantComlimentaryRoom($hid) {
        if ($hid != "") {
            $sql = 'DELETE rest_complimentary_room,rest_cmplimntry_room_excluded_dates
					FROM rest_complimentary_room
					INNER JOIN rest_cmplimntry_room_excluded_dates ON rest_complimentary_room.cmpl_room_id = rest_cmplimntry_room_excluded_dates.cmpl_room_id
					WHERE  rest_complimentary_room.cmpl_room_id=rest_cmplimntry_room_excluded_dates.cmpl_room_id  AND  rest_complimentary_room.rest_id =' . $hid;
            $this->db->query($sql);
        }
        return;
    }
 /**
     * @method importRestaurantComlimentaryRoom() method used to insert restaurant complimentary room
	 * @param array() $data hold restaurant complimentary room in the form of an array
	 * @todo this method method used to insert restaurant complimentary room into rest_complimentary_room table
	 * @return true | false
     */
    function importRestaurantComlimentaryRoom($data) {
        if ($this->db->insert('rest_complimentary_room', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
 /**
     * @method importRestaurantComlimentaryRoomExcludedDate() method used to insert restaurant complimentary room exclude date
	 * @param array() $data hold restaurant complimentary room exclude date the form of an array
	 * @todo this method method used to insert restaurant complimentary room into rest_cmplimntry_room_excluded_dates table
	 * @return true | false
     */
    function importRestaurantComlimentaryRoomExcludedDate($data) {
        if ($this->db->insert_batch('rest_cmplimntry_room_excluded_dates', $data)) {
            return true;
        }
        return false;
    }


/**
     * @method addRestaurantPropertyTypes() method used to insert restaurant property types
	 * @param array() $data hold restaurant property types in the form of an array
	 * @todo this method method used to insert restaurant property types into rest_property_types table
	 * @return true | false
     */

    function addRestaurantPropertyTypes($data) {
        if ($this->db->insert_batch('rest_property_types', $data)) {
            return true;
        }
        return false;
    }

  /**
     * @method deleteRestaurantBankingInfo() method used to delete restaurant banking information
	 * @param int $hid hold restaurant id - banking info details fetch through restaurant id.
	 * @todo this method method used to delete restaurant banking information from rest_bank_accounts table
	 * @return true | false
     */
    function deleteRestaurantBankingInfo($hid) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_bank_accounts');
        }
        return;
    }
/**
     * @method importRestaurantBankingInfo() method used to import restaurant banking information
	 * @param array() $data hold restaurant bank accounts in the form of an array
	 * @todo this method method used to import restaurant banking information into rest_bank_accounts table
	 * @return true | false
     */
    function importRestaurantBankingInfo($data) {
        if ($this->db->insert_batch('rest_bank_accounts', $data)) {
            return true;
        }
        return false;
    }

     /**
     * @method deleteRestaurantCancellation() method used to delete restaurant cancellation
	 * @param int $hid holds restaurant id details for restaurant cancellation.
	 * @param string $seasion hold restaurant seasion('low' or 'high')
	 * @todo this method method used to delete restaurant cancellation from rest_cancellation table
	 * @return true | false
     */

    function deleteRestaurantCancellation($hid, $seasion) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->where('seasion', $seasion);
            $this->db->delete('rest_cancellation');
        }
        return;
    }
   /**
     * @method importRestaurantCancellation() method used to import restaurant cancellation
	 * @param array() $data hold restaurant cancellation in the form of an array
	 * @todo this method  used to import restaurant cancellation into rest_cancellation table
	 * @return true | false
     */
    function importRestaurantCancellation($data) {
        if ($this->db->insert_batch('rest_cancellation', $data)) {
            return true;
        }
        return false;
    }


   /**
     * @method deleteRestaurantRenovationSchedule() method used to delete restaurant renovation schedule
	 * @param int $hid holds restaurant id - which contains schedule info.
	 * @todo this method  used to delete restaurant renovation schedule from rest_renovation_schedule table
	 * @return true | false
     */

    function deleteRestaurantRenovationSchedule($hid) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_renovation_schedule');
        }
        return;
    }
   /**
     * @method importRestaurantRenovationSchedule() method used to insert restaurant renovation schedule
	 * @param array() $data hold restaurant renovation schedule in the form of an array
	 * @todo this method  used to insert restaurant renovation schedule into rest_renovation_schedule table
	 * @return true | false
     */
    function importRestaurantRenovationSchedule($data) {
        if ($this->db->insert_batch('rest_renovation_schedule', $data)) {
            return true;
        }
        return false;
    }

 /**
     * @method deleteRestaurantDistanceFrom() method used to delete restaurant distance
	 * @param int $hid hold restaurant id - contains distance details which are to be deleted.
	 * @todo this method  used to delete restaurant distance from rest_distance table
	 * @return true | false
     */

    function deleteRestaurantDistanceFrom($hid) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_distance');
        }
        return;
    }
/**
     * @method importRestaurantDistanceFrom() method used to import restaurant distance from
	 * @param array() $data hold restaurant distance from in the form of an array
	 * @todo this method  used to import restaurant distance from rest_distance table
	 * @return true | false
     */
    function importRestaurantDistanceFrom($data) {
        if ($this->db->insert_batch('rest_distance', $data)) {
            return true;
        }
        return false;
    }

/**
     * @method addFacility() method used to add restaurant facility
	 * @param array() $data hold restaurant facility from in the form of an array
	 * @todo this method  used to add restaurant facility into entity_attributes table
	 * @return id | false
     */
    function addFacility($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getFacility() method used to fetch restaurant facility id
	 * @param string() $title holds restaurant facility title or name.
	 * @todo this method  used to fetch restaurant facility id from entity_attributes table
	 * @return id | false
     */
    function getFacility($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
/**
     * @method deleteRestaurantFacilities() method used to delete restaurant facility
	 * @param int $hid hold restaurant id -  restaurant facility detais using restaurant id.
	 * @todo this method  used to delete restaurant facility from rest_facilities table
	 * @return true | false
     */
    function deleteRestaurantFacilities($hid) {
        if ($hid != "") {
            $this->db->where('rest_id', $hid);
            $this->db->delete('rest_facilities');
        }
        return;
    }
/**
     * @method importRestaurantFacilities() method used to import restaurant facility
	 * @param array $data hold restaurant facility in the form of an array
	 * @todo this method  used to delete restaurant facility from rest_facilities table
	 * @return true | false
     */
    function importRestaurantFacilities($data) {
        if ($this->db->insert_batch('rest_facilities', $data)) {
            return true;
        }
        return false;
    }

 /**
     * @method getHotelRoomsPricingData() method used to fetch restaurant pricing data.
	 * @param int $rest_id holds restaurant id
	 * @param int $room_type holds room type 
	 * @param int $currency holds currency
	 * @param string $period_from holds period from
	 * @param string $period_to holds period to
	 * @todo method used to fetch restaurant pricing from rest_rooms_pricing table.
	 * @return pricing_ids | false
     */


    function getRestaurantRoomsPricingData($rest_id, $room_type, $currency, $period_from, $period_to) {
        $this->db->select('pricing_id');
        $this->db->where("rest_id", $rest_id);
        $this->db->where("room_type", $room_type);
        $this->db->where("curency_code", $currency);
        $this->db->where("period_from", $period_from);
        $this->db->where("period_to", $period_to);
        $query = $this->db->get('rest_rooms_pricing');
        if ($query->num_rows() > 0) {
            return $query->row()->pricing_id;
        } else {
            return false;
        }
    }
  /**
     * @method editHotelRoomsPricingData() method used to update restaurant pricing data
	 * @param int $hid holds restaurant id -  contains restaurant info tables.
	 * @param int $pricing_id holds pricing id - room pricing is edited through it.
	 * @param array() $data hold restaurant room princing in the form of an array
	 * @todo this method  used to update restaurant pricing from rest_rooms_pricing table
     */
    function editRestaurantRoomsPricingData($hid, $pricing_id, $data) {
        $this->db->where('pricing_id', $pricing_id);
        $this->db->where("rest_id", $hid);
        $this->db->update('rest_rooms_pricing', $data);
    }

	/**
     * @method importRestaurantRoomsPricingData() method used to import restaurant room pricing data
	 * @param array() $data hold restaurant room pricing in the form of an array
	 * @todo this method  used to import restaurant room pricing data into rest_rooms_pricing table
     */
    function importRestaurantRoomsPricingData($data) {
        if ($this->db->insert('rest_rooms_pricing', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method deleteRoompricing() method used to delete room pricing.
	 * @param int() $hid holds restaurant id - room pricing deleted using restaurant id.
	 * @param int() $room_type holds room type - contains room type details.
	 * @todo this method  used to delete room pricing data from rest_rooms_pricing table
     */
	function deleteRoompricing($hid,$room_type) {
		$this->db->where('rest_id', $hid);
		$this->db->where('room_type', $room_type);
        $this->db->delete('rest_rooms_pricing');
    }
	/**
     * @method addRestaurantRoomPricingComplimentary() method used to insert restaurant room pricing complimentary
	 * @param array() $data hold restaurant room complimentary services in the form of an array
	 * @todo this method  used to insert restaurant room pricing complimentary into restaurant_rooms_pricing_complimentary table
	 * @return true|false
     */
    function addRestaurantRoomPricingComplimentary($data) {
        if ($this->db->insert_batch('restaurant_rooms_pricing_complimentary', $data)) {
            return true;
        }
        return false;
    }
	/**
     * @method deleteRoomPricingComplimentary() method used to delete restaurant room pricing complimentary services.
	 * @param int  $pricingId holds princing id containing priing details.
	 * @todo method  used to delete restaurant room pricing complimentary from restaurant_rooms_pricing_complimentary table
     */
    function deleteRoomPricingComplimentary($pricingId) {
      
            $this->db->where('pricing_id', $pricingId);
            $this->db->delete('rest_rooms_pricing_complimentary');
      
    }
/**
     * @method importRestaurantRroomsPricingDetails() method used to import rest room pricing details
	 * @param array()  $data holds restaurant room pricing detail in the form of an array
	 * @todo this method  used to import restaurant room pricing details into rest_rooms_pricing_details table
	 * @return true|false
     */
    function importRestaurantRroomsPricingDetails($data) {
        if ($this->db->insert_batch('rest_rooms_pricing_details', $data)) {
            return true;
        }
        return false;
    }
/**
     * @method updateRestaurantRroomsPricingDetails() method used to update restaurant room pricing details
	 * @param array()  $data holds restaurant room pricing detail in the form of an array
	 * @param int $pricing_id holds restaurant room pricing id - primary key.
	 * @param int $market_id holds restaurant room market id 
	 * @todo this method  used to update restaurant room pricing details into rest_rooms_pricing_details table
	 * @return id|false
     */
    function updateRestaurantRroomsPricingDetails($data, $pricing_id = '', $market_id = null) {
        if ($pricing_id != "") {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->where('market_id', $market_id);
            $q = $this->db->get('rest_rooms_pricing_details');
            if ($q->num_rows() > 0) {
                $this->db->where('pricing_id', $pricing_id);
                $this->db->where('market_id', $market_id);
                $this->db->update('rest_rooms_pricing_details', $data);
                return $pricing_id;
            } else {
                $this->db->insert('rest_rooms_pricing_details', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_rooms_pricing_details', $data);
            return $this->db->insert_id();
        }
        return;
    }

   /**
     * @method addRoomType() method used to insert restaurant room type
	 * @param array()  $data holds restaurant room type details in the form of an array
	 * @todo this method  used to insert restaurant room type into entity_attributes table
	 * @return id|false
     */

    function addRoomType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getRoomTypeId() method used to fetch restaurant room type id
	 * @param string  $title contains restaurant room type name.
	 * @todo this method  used to insert restaurant room type into entity_attributes table
	 * @return id|false
     */
    function getRoomTypeId($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_room_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
 /**
     * @method addMarket() method used to insert restaurant market
	 * @param array()  $data holds restaurant market in the form of an array in DB.
	 * @todo this method  used to insert restaurant market type into entity_attributes table
	 * @return id|false
     */
    function addMarket($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
 /**
     * @method getMarket() method used to fetch restaurant market id
	 * @param array()  $data hold restaurant market in the form of an array
	 * @param string  $title hold restaurant market title
	 * @todo this method  used to insert restaurant market into entity_attributes table
	 * @return id|false
     */
    function getMarket($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_market'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
/**
     * @method get_room_types() method used to fetch restaurant room type details
	 * @param int  $rest_id hold restaurant id - data table for restaurants.
	 * @todo method  used to fetch restaurant room type details from rest_rooms_pricing table
	 * @return array |false
     */
    function get_room_types($rest_id = null) {
        if (!empty($rest_id)) {
            $this->db->select('rest_room_types.title, rest_rooms_pricing.pricing_id,market_id,inclusions,curency_code,max_adult,max_child,inventory,DATE(period_from) as period_from,DATE(period_to) as period_to,double_price,triple_price,quad_price,breakfast_price,half_board_price,all_incusive_adult_price,extra_adult_price,extra_child_price,extra_bed_price,markets.title as market');
            $this->db->from('rest_rooms_pricing');
            $this->db->join('rest_rooms_pricing_details', 'rest_rooms_pricing.pricing_id = rest_rooms_pricing_details.pricing_id');
            $this->db->join('markets', 'rest_rooms_pricing_details.market_id = markets.id');
            $this->db->join('rest_room_types', 'rest_rooms_pricing.room_type = rest_room_types.type_id');
            $this->db->where("rest_rooms_pricing.rest_id", $rest_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
        return false;
    }
	
	/**
     * Common function to check already assigned attributes.
     * @param string $table 
     * @param int $rest_id
     * @param string $where
     * @param string $selected_feild
     * @return true
     */
    function isDataExist($table = '',$rest_id = null, $where = '',$select_feild = '') {
        if(!empty($select_feild) )
        $this->db->select($select_feild);
        
        if(!empty($where) && !empty($rest_id))
        $this->db->where($where, $rest_id);
        
        if(!empty($table))
        $query = $this->db->get($table);
        
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

}
