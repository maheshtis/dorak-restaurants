<?php

/*
 * @data 15-mar-2016
 * @author tis
 * @description This controller  is perform all action like add update sevral checks which is related to restaurant.
 */

class Restaurant_model extends AdminModel {

    function __construct() {
        // Call the Model constructor.(it call the base model to hmvc to construct the instanse of a model)
        parent::__construct();
    }

    /**
     * @meathod : string getRestaurantsDetails(), get full details of a restaurants based on a restaurant id.
     * @param : int $resturantId
     * @todo : Fetch all restaurant information with all related attributes like country,chain of associative restaurant ID. 
     * @return :array(), restaurant details of restaurant in the form of array.
     */
    function getRestaurantsDetails($resturantId) {
        $this->db->select('restaurants.rest_id,
							restaurants.rest_name,
							restaurants.rest_code,
							restaurants.country_code,
							restaurants.city,
							restaurants.rest_address,
							restaurants.post_code,
							restaurants.district,
							restaurants.dorak_rating,
							restaurants.star_rating,
							restaurants.purpose,
							restaurants.currency,
							restaurants.chain_id,							
							restaurants.commisioned,
							restaurants.child_age_group_id,
							restaurants.status');
        $this->db->from('restaurants');
        $this->db->where('restaurants.rest_id', $resturantId);
        $query = $this->db->get();
        return $query->row();
    }

     /**
     * @meathod : string getRestaurantsList(), get list of resturant.
     * @param : $search_term(array), $limit(int),$offset(int).
     * @todo : Fetch all resturant information based on parameter of searching a resturant and default limit is 10 restaurants. 
     * @return :array(), resturant details of resturant in the form of array.
     */
    function getRestaurantsList($search_term = null, $limit = 10, $offset = 0) {
//        dump($search_term);
        $this->db->select('restaurants.rest_id,
							restaurants.rest_name,
							restaurants.rest_code,
							countries.country_name,
							restaurants.city,
							restaurants.dorak_rating,
							restaurants.star_rating,
							eaps.entity_title as purpose,
							restaurants.last_updated,
							cn.entity_title as chain,
							ea.entity_title as status,
							eap.entity_title as property_types');
        $this->db->from('restaurants');
        $this->db->join('entity_attributes ea', 'restaurants.status = ea.id AND ea.entity_type="ahs"');
        $this->db->join('countries', 'restaurants.country_code = countries.country_code');
        $this->db->join('entity_attributes cn', 'cn.id = restaurants.chain_id AND cn.entity_type="ahc"', 'left');
        $this->db->join('entity_attributes eaps', 'eaps.id = restaurants.purpose AND cn.entity_type="ahp"','left');
        $this->db->join('rest_property_types  hpt', 'restaurants.rest_id = hpt.rest_id', 'left');
        $this->db->join('entity_attributes  eap', 'eap.id = hpt.property_type_id AND eap.entity_type="apt"', 'left');
        if ($search_term != "" and count($search_term) > 0) {

            if ($search_term['searchByRestaurantName'] != "")
                $this->db->like('restaurants.rest_name', $search_term['searchByRestaurantName']);
            if ($search_term['searchByRestaurantCode'] != "")
                $this->db->like('restaurants.rest_code', $search_term['searchByRestaurantCode']);
            if ($search_term['searchByPurpose'] != "")
                $this->db->where('eaps.id', $search_term['searchByPurpose']);
            if ($search_term['searchByDorakRating'] != "")
                $this->db->where('restaurants.star_rating', $search_term['searchByDorakRating']);
            if ($search_term['searchByStatus'] != "")
                $this->db->where('restaurants.status', $search_term['searchByStatus']);
            if ($search_term['searchByChainId'] != "")
                $this->db->where('restaurants.chain_id', $search_term['searchByChainId']);
            if ($search_term['searchByPropertyType'] != "")
                $this->db->where('pt.id', $search_term['searchByPropertyType']);
        }
        $this->db->order_by("restaurants.rest_name", "asc");
        $this->db->group_by('restaurants.rest_id');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * string deleteRestaurants() :-(select all resturant) used to delete a resturant with all related resturant entity.
     * @param array $hids The resturant id
     * @todo it will delete resturant list(array format) with related all entities like property types,facilities,roomtypes etc.
     * @return true
     */
    function deleteRestaurants($hids) {
        $bank_del = $this->deleteRestaurantBankDetails($hids); // delete bank details first due to reference fo forien key relationship
        if ($bank_del) { // if bank details deletd successfully then it will deleted all restaurants attributes.

            $this->deleteRestaurantPropertyTypes($hids);
            $this->deleteRestaurantFacilities($hids);
            $this->deleteRestaurantDistance($hids);
            $this->deleteRestaurantContactDetails($hids);
            $this->deleteRestaurantContractInfo($hids);
            $this->deleteRestaurantRenovationSchedule($hids);
            $this->deleteRestaurantComplimentaryRoomExcludedDates($hids);
            $this->deleteRestaurantComplimentaryRoom($hids);
            $this->deleteRestaurantComplimentaryServices($hids);
            $this->deleteRestaurantRoomsPricingComplimentary($hids);
            $this->deleteRestaurantRoomsPricing($hids);
            $this->db->where_in('rest_id', $hids);
            $this->db->delete('restaurants');
        }
        return true;
    }

    /**
     * string deleteRestaurant() method delete the resturant one by one
     * @param int $hid The resturant id
     * @todo delete restaurants one by one based on resturant ids.
     * @return true
     */
    function deleteRestaurant($hid) {
        $resturantId = (int) $hid;
        $bankrecord = $this->deleteRestaurantBankDetails($resturantId);
        if ($bankrecord) {
            $this->deleteRestaurantBankDetails($resturantId);
            $this->deleteRestaurantPropertyTypes($resturantId);
            $this->deleteRestaurantFacilities($resturantId);
            $this->deleteRestaurantDistance($resturantId);
            $this->deleteRestaurantContactDetails($resturantId);
            $this->deleteRestaurantContractInfo($resturantId);
            $this->deleteRestaurantRenovationSchedule($resturantId);
            $this->deleteRestaurantComplimentaryRoomExcludedDates($resturantId);
            $this->deleteRestaurantComplimentaryRoom($resturantId);
            $this->deleteRestaurantComplimentaryServices($resturantId);
            $this->deleteRestaurantRoomsPricingComplimentary($resturantId);
            $this->deleteRestaurantRoomsPricing($resturantId);
            $this->db->where('rest_id', $resturantId);
            $this->db->delete('restaurants');
        }
        return true;
    }

    /**
     * string deleteRestaurantBankDetails() method delete the bank detail
     * @param array|int $hids The resturant id
     * @todo it delete resturant bank details associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantBankDetails($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_bank_accounts');
    }

    /**
     * string deleteRestaurantFacilities() method delete the resturant facilities.
     * @param array|int $hids The resturant id
     * @todo it delete resturant facilities associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantFacilities($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_facilities');
    }

     /**
     * string deleteRestaurantPropertyTypes() method delete the resturant property type
     * @param array|int $hids The resturant id
     * @todo it delete resturant property associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantPropertyTypes($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_property_types');
    }

    /**
     * string deleteRestaurantDistance() method delete resturant distance
     * @param array|int $hids The resturant id
     * @todo it delete resturant distance associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantDistance($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_distance');
    }

    /**
     * string deleteRestaurantContactDetails() method delete resturant contact details.
     * @param array|int $hids The resturant id
     * @todo it delete resturant contact details associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantContactDetails($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_contact_details');
    }

     /**
     * string deleteRestaurantContractInfo() method delete resturant contract information.
     * @param array|int $hids The resturant id
     * @todo it delete resturant contact details associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantContractInfo($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_contract_info');
    }

     /**
     * string deleteRestaurantRenovationSchedule() method delete resturant renovation shedule.
     * @param array|int $hids The resturant id
     * @todo it delete resturant renovation shedules associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantRenovationSchedule($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_renovation_schedule');
    }

    /**
     * string deleteRestaurantComplimentaryServices() method delete resturant complementry service.
     * @param array|int $hids The resturant id
     * @todo it delete resturant complementry service associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantComplimentaryServices($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_complimentary_services');
    }

    /**
     * string deleteRestaurantComplimentaryRoom() method delete resturant complementry rooms.
     * @param array|int $hids The resturant id
     * @todo it delete resturant complementry rooms associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantComplimentaryRoom($hids) {
        if (is_array($hids)) {
            $this->db->where_in('rest_id', $hids);
        } else {
            $this->db->where('rest_id', (int) $hids);
        }
        return $this->db->delete('rest_complimentary_room');
    }

    /**
     * string deleteRestaurantComplimentaryRoomExcludedDates() method delete resturant complementry exclude dates rooms.
     * @param array|int $hids The resturant id
     * @todo it delete resturant complementry rooms exclude dates associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantComplimentaryRoomExcludedDates($hids) {
        if (is_array($hids)) {
            $hids = implode($hids, ',');
        }
        $sql = 'DELETE rest_cmplimntry_room_excluded_dates
		FROM rest_cmplimntry_room_excluded_dates 
		INNER JOIN rest_complimentary_room ON rest_complimentary_room.cmpl_room_id = rest_cmplimntry_room_excluded_dates.cmpl_room_id
		';
        $sql.=' WHERE rest_complimentary_room.cmpl_room_id = rest_cmplimntry_room_excluded_dates.cmpl_room_id  AND rest_complimentary_room.rest_id IN (' . $hids . ')';
        $this->db->query($sql);
        return true;
    }

    /**
     * string deleteRestaurantRoomsPricingComplimentary() method delete resturant pricings rooms(complimentry).
     * @param array|int $hids The resturant id
     * @todo it delete resturant pricings rooms(complimentry) associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantRoomsPricingComplimentary($hids) {
        if (is_array($hids)) {
            $hidsD = implode($hids, ',');
        } else {
            $hidsD = $hids;
        }
        $sql = 'DELETE rest_rooms_pricing_complimentary
			FROM rest_rooms_pricing_complimentary
			INNER JOIN rest_rooms_pricing ON rest_rooms_pricing.pricing_id = rest_rooms_pricing_complimentary.pricing_id
			';
        $sql.='WHERE  rest_rooms_pricing_complimentary.pricing_id=rest_rooms_pricing.pricing_id  AND   rest_rooms_pricing.rest_id IN (' . $hidsD . ')';
        $this->db->query($sql);
        return true;
    }

    /**
     * string deleteRestaurantRoomsPricing() method delete resturant pricings rooms.
     * @param array|int $hids The resturant id
     * @todo it delete resturant pricings rooms associated to a resturant ids.
     * @return true
     */
    function deleteRestaurantRoomsPricing($hids) {
        if (is_array($hids)) {
            $hidsD = implode($hids, ',');
        } else {
            $hidsD = $hids;
        }
        $sql = 'DELETE rest_rooms_pricing,rest_rooms_pricing_details
		FROM rest_rooms_pricing
		INNER JOIN rest_rooms_pricing_details ON rest_rooms_pricing.pricing_id = rest_rooms_pricing_details.pricing_id
		';
        $sql.='WHERE  rest_rooms_pricing_details.pricing_id=rest_rooms_pricing.pricing_id  AND  rest_rooms_pricing.rest_id IN (' . $hidsD . ')';
        $this->db->query($sql);
        return true;
    }

    /**
     * string updateRestaurantDetails() method to update the resturant detail
     * @param (int)$hid, array($data) resturant id used for which resturant will deleted and data stands for resturant feilds.
     * @todo it will update the resturant feilds based on resturant id.
     * @return true
     */
    function updateRestaurantDetails($hid, $data) {
        $this->db->where('rest_id', $hid);
        $this->db->update('restaurants', $data);
    }

    /**
     * string getRestaurantDetails() method get the restaurants
     * @param (varchar)(text) $hname The resturant title(resturant name)
     * @param (varchar)(text) $hcity The city
     * @param (int)$postcode The post code
     * @todo it fetch the list of restaurants based on resturant name,resturant city and postcode.
     * @return output resturant list array()
     */
    function getRestaurantDetails($hname, $hcity, $postcode) {
        $this->db->select('rest_id');
        $this->db->where('rest_name', $hname);
        $this->db->where('city', $hcity);
        $this->db->where('post_code', $postcode);
        $query = $this->db->get('restaurants');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string insertDetails() method insert restaurants
     * @param varchar $data The resturant record
     * @todo insert the resturant data into the resturant tables.
     * @return last inserted resturant id
     */
    function insertDetails($data) {
        if ($this->db->insert('restaurants', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string getRestaurantPropertyType() method get restaurants property
     * @param varchar $title The property title
     * @todo it will fetch all property types based on property title.
     * @return id
     */
    function getRestaurantPropertyType($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }

    /**
     * string addRestaurantPropertyType() method add property
     * @param varchar $data The property title.
     * @todo add resturant property type into the database.
     * @return lastinserted id
     */
    function addRestaurantPropertyType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string addResturantPaymentShedules() method add payment schedule
     * @param varchar $data The payment schedule
     * @todo  add resturant payment shedules
     * @return true
     */
    function addResturantPaymentShedules($data) {
        if ($this->db->insert_batch('rest_payment_shedules', $data)) {
            return true;
        }
        return false;
    }

   /**
     * string getResturantPaymentShedules() method used for listing to resturant payment shedules.
     * @param int $rest_id 
     * @todo  it fetch all resturant payment shedules.
     * @return true
     */
    function getResturantPaymentShedules($rest_id) {
        $this->db->select('id,payment_option_id,payment_value');
        $this->db->where("rest_id", $rest_id);
        $this->db->order_by("payment_value", "desc");
        $query = $this->db->get('rest_payment_shedules');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteResturantPaymentShedules() method delete the payment schedule
     * @param int $resturantId (array)$arrayIds The array ids
     * @todo  int delete resturant payments data associated with resturant id
     * @return true
     */
    function deleteResturantPaymentShedules($arrayIds, $resturantId) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('rest_id', $resturantId);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('rest_payment_shedules');
        }
        return true;
    }

    /**
     * updateResturantPaymentShedules() method update the payment
     * @param int $id The payment schedule id
     * @param varchar $data The payment schedule record
     * @todo it update the resturant payment shedule informations based on resturant id.
     * @return true
     */
    function updateResturantPaymentShedules($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('rest_payment_shedules');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('rest_payment_shedules', $data);
                return $id;
            }
        } else {
            $this->db->insert('rest_payment_shedules', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addRestaurantChain() method insert chain
     * @param varchar $data The chain title
     * @todo it add the resturant chain into DB.
     * @return id
     */
    function addRestaurantChain($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string getRestaurantChain() method get the chain
     * @param varchar $title The chain title
     * @todo  it fetch the list of resturant chain based on resturant id.
     * @return id
     */
    function getRestaurantChain($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_resturant_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }

    /**
     * string getRestaurantChainList() method list the chain
     * @todo  it fetch resturant chain list for dropdown(resturant chain)
     * @return chains array()
     */
    function getRestaurantChainList() {
        $this->db->select('id,entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_resturant_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantContactInfo() method insert contract
     * @param varchar $data The contract title
     * @todo it add the resturant contact info into the DB.
     * @return id
     */
    function addResturantContactInfo($data) {
        if ($this->db->insert_batch('rest_contact_details', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getRestaurantChain() method get the chain
     * @param int $rest_id The contact information
     * @todo  it fetch the resturant contact data from the database based on resturant id.
     * @return output resturant contact info form of array()
     */
    function getResturantContactInfo($rest_id) {
        $this->db->select('contact_id,position,name,email,phone,extension');
        $this->db->where("rest_id", $rest_id);
        $query = $this->db->get('rest_contact_details');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantComplimentaryServices() method insert complimentary services
     * @param varchar $data The complimentary services title
     * @todo  it will add resturant complimentry service into the DB.
     * @return true
     */
    function addResturantComplimentaryServices($data) {
        if ($this->db->insert_batch('rest_complimentary_services', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantComplimentaryServices() method list the complimentary services
     * @param int $rest_id resturant id
     * @todo it will fetch all list of resturant complimentry service based on resturant id.
     * @return complimentary services
     */
    function getResturantComplimentaryServices($rest_id) {
        $this->db->select('complimentary_services.cmpl_service_id,complimentary_services.service_name');
        $this->db->from('complimentary_services');
        $this->db->join('rest_complimentary_services', 'complimentary_services.cmpl_service_id = rest_complimentary_services.cmpl_service_id');
        $this->db->where("rest_complimentary_services.rest_id", $rest_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantContract() method insert contract
     * @param (array)$data The contact information
     * @todo  it add resturant contract into the DB.
     * @return id
     */
    function addResturantContract($data) {
        if (!empty($data)) {
            if ($this->db->insert('rest_contract_info', $data)) {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    /**
     * string addMarketFiles() method insert contract
     * @param (array)$data The contact information
     * @todo  it add resturant contract into the DB.
     * @return id
     */
    function addMarketFiles($data) {
        if (!empty($data)) {
//            dump($data);die;
            if ($this->db->insert('res_menu_markets', $data)) {
                return $this->db->insert_id();
            }
        }
        return false;
    }

    /**
     * string getResturantContract() method get the resturant contract information
     * @param int $rest_id , id of resturant.
     * @todo it will fetch all details of resturant contract based on resturant id.ss
     * @return output list of resturant contract in the form array()
     */
    function getResturantContract($rest_id) {
        $this->db->select('id,start_date,end_date,signed_by,contract_file');
        $this->db->where("rest_id", $rest_id);
        $this->db->order_by("creared_on", "desc");
        $query = $this->db->get('rest_contract_info');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantComlimentaryRoom() method used to insert resturant complimentary room
     * @param varchar $data The complimentary record
     * @todo  it add the resturant complimentry data in the form of array into the DB.
     * @return it return the last inserted id of the resturant complimentry.
     */
    function addResturantComlimentaryRoom($data) {
        if ($this->db->insert('rest_complimentary_room', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string getResturantComlimentaryRoom() method insert resturant complimentary room
     * @param id $rest_id(resturant id).
     * @todo it fetched the resturant complimentry data based on resturant id.
     * @return output list of resturant complimentry room in the form array()
     */
    function getResturantComlimentaryRoom($rest_id) {
        $this->db->select('cmpl_room_id,room_night,room_type,start_date,end_date,upgrade');
        $this->db->where("rest_id", $rest_id);
        $query = $this->db->get('rest_complimentary_room');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * string updateResturantComlimentaryRoom().
     * @param $data(array),(int)$rest_id,$cmpl_room_id;
     * @todo it update the restaurants complimentry room data into the database based on perticular resturant id and complimentry id
     * @return id
     */
    function updateResturantComlimentaryRoom($data, $rest_id, $cmpl_room_id = '') {
        if ($cmpl_room_id != "") {
            $this->db->where("rest_id", $rest_id);
            $this->db->where('cmpl_room_id', $cmpl_room_id);
            $q = $this->db->get('rest_complimentary_room');
            if ($q->num_rows() > 0) {
                $this->db->where('cmpl_room_id', $cmpl_room_id);
                $this->db->update('rest_complimentary_room', $data);
                return $cmpl_room_id;
            } else {
                $this->db->insert('rest_complimentary_room', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_complimentary_room', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addResturantComlimentaryRoomExcludedDate() method insert Resturant Comlimentary Room Excluded Date
     * @param (array)$data The Resturant Comlimentary RoomExcluded Date title
     * @todo add resturant complimentry excludes dates into the database.
     * @return id
     */
    function addResturantComlimentaryRoomExcludedDate($data) {
        if ($this->db->insert_batch('rest_cmplimntry_room_excluded_dates', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantComlimentaryRoomExcludedDate() method get Comlimentary Room
     * @param int $resturant_cmpl_room_id The resturant complimentary id
     * @todo it fetch the list of resturant complimentry exclude date based on resturant complimentry id.
     * @return array()
     */
    function getResturantComlimentaryRoomExcludedDate($resturant_cmpl_room_id) {
        $this->db->select('id,exclude_date_from,excluded_date_to');
        $this->db->where("cmpl_room_id", $resturant_cmpl_room_id);
        $query = $this->db->get('rest_cmplimntry_room_excluded_dates');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string getResturantComlimentaryRoomExcludedDateByRestaurant() method get Comlimentary Room exclude date by resturant id
     * @param int $rest_id The resturant  id
     * @param int $rest_id The resturant  id
     * @return array()
     */
    function getResturantComlimentaryRoomExcludedDateByRestaurant($rest_id = null) {
        $this->db->select('id,exclude_date_from,excluded_date_to');
        $this->db->from('rest_cmplimntry_room_excluded_dates');
        $this->db->join('rest_complimentary_room', 'rest_complimentary_room.cmpl_room_id = rest_cmplimntry_room_excluded_dates.cmpl_room_id');
        $this->db->where("rest_complimentary_room.rest_id", $rest_id);
        $this->db->order_by("rest_cmplimntry_room_excluded_dates.exclude_date_from", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteResturantComlimentaryRoomExcludedDate() method delete the resturant complimentary room exclude date
     * @param array $arrayIds The array ids
     * @param int $cmpl_room_id The complimentary room id
     * @todo it delete the resturant complimentry room exclude dates in the form of array.
     * @return true
     */
    function deleteResturantComlimentaryRoomExcludedDate($arrayIds, $cmpl_room_id) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('cmpl_room_id', $cmpl_room_id);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('rest_cmplimntry_room_excluded_dates');
        }
        return true;
    }

    /**
     * string updateResturantComlimentaryRoomExcludedDate() method update resturant complimentary room exclude date
     * @param int $id The complimentary room exclude date id
     * @param string $data resturant complimentary room exclude date record
     * @todo it update the resturant complimentry room exclude datae based on id.
     * @return true
     */
    function updateResturantComlimentaryRoomExcludedDate($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('rest_cmplimntry_room_excluded_dates');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('rest_cmplimntry_room_excluded_dates', $data);
                return $id;
            }
        } else {
            $this->db->insert('rest_cmplimntry_room_excluded_dates', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addResturantPropertyTypes() method insert resturant property types
     * @param int $id The complimentary room exclude date id
     * @todo it add resturant property
     * @return true
     */
    function addResturantPropertyTypes($data) {
        if ($this->db->insert_batch('rest_property_types', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string addResturantBankingInfo() method insert bank information
     * @param varchar() $data bank accounts array
     * @todo  it will add resturant bank information into the DB.
     * @return true
     */
    function addResturantBankingInfo($data) {
        if ($this->db->insert_batch('rest_bank_accounts', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantBankInfo() method insert bank information
     * @param int $rest_id resturant id
     * @todo it fetched resturant bank information from the database based on resturant id.
     * @return list of resturant banking information in the form of array()
     */
    function getResturantBankInfo($rest_id) {
        $this->db->select('id,account_number,account_name,bank_name,bank_address,bank_ifsc_code');
        $this->db->where("rest_id", $rest_id);
        $query = $this->db->get('rest_bank_accounts');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantConfrence() method insert resturant conference
     * @param varchar() $data resturant conference array
     * @todo  add resturant confrence detail into the DB.
     * @return true
     */
    function addResturantConfrence($data) {
        if ($this->db->insert_batch('rest_confrences', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantConfrence() method get resturant conference
     * @param int $rest_id resturant conference array
     * @todo it fetched the list of resturant confrence using resturant id.
     * @return list of resturant resturant confrence in the form of array()
     */
    function getResturantConfrence($rest_id) {
        $this->db->select('id,confrence_name,area,celling_height,classroom,theatre,banquet,reception,conference,ushape,hshape');
        $this->db->where("rest_id", $rest_id);
        $query = $this->db->get('rest_confrences');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantCancellation() method insert resturant cancellation
     * @param varchar() $data resturant conference array
     * @todo add resturant cancellation data into the database.
     * @return true
     */
    function addResturantCancellation($data) {
        if ($this->db->insert_batch('rest_cancellation', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantCancellation() method get resturant cancellation
     * @param id $rest_id resturant id
     * @todo it fetched the list of resturant cancellation using resturant id.
     * @return list of resturant resturant cancellation in the form of array()
     */
    function getResturantCancellation($rest_id) {
        $this->db->select('id,seasion,cancelled_before,payment_request');
        $this->db->where("rest_id", $rest_id);
        $this->db->order_by("payment_request", "desc");
        $query = $this->db->get('rest_cancellation');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteResturantCancellation() method delete the resturant cancellation
     * @param int $arrayIds The array ids
     * @param int $resturantId The resturant id
     * @todo it delete the resturant cancellation data from the DB.
     * @return true
     */
    function deleteResturantCancellation($arrayIds, $resturantId) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('rest_id', $resturantId);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('rest_cancellation');
        }
        return true;
    }

    /**
     * string updateResturantCancellation() method update resturant cancellation
     * @param int $id The resturant cancellation id.
     * @param varchar $data The chain record.
     * @todo it update the resturant cancellation data based on id.
     * @return id
     */
    function updateResturantCancellation($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('rest_cancellation');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('rest_cancellation', $data);
                return $id;
            }
        } else {
            $this->db->insert('rest_cancellation', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addResturantRenovationSchedule() method insert renovation schedule
     * @param varchar $data The renovation array
     * @todo  it add the resturant renovation schedule data into the database.
     * @return id
     */
    function addResturantRenovationSchedule($data) {
        if ($this->db->insert_batch('rest_renovation_schedule', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantRenovationSchedule() method insert renovation schedule
     * @param int $rest_id resturant id
     * @todo it will fetched the all list of resturant renovation schedule based on resturant id.
     * @return array
     */
    function getResturantRenovationSchedule($rest_id) {
        $this->db->select('rnv_id,date_from,date_to,renovation_type,area_effected');
        $this->db->where("rest_id", $rest_id);
        $this->db->order_by("date_from", "desc");
        $query = $this->db->get('rest_renovation_schedule');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteResturantRenovationSchedule() method delete the renovation id
     * @param int $arrayIds The renovation ids
     * @param it delete the resturant renovation schedule data from the DB.
     * @return true
     */
    function deleteResturantRenovationSchedule($arrayIds, $hid) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('rest_id', $hid);
            $this->db->where_not_in('rnv_id', $arrayIds);
            $this->db->delete('rest_renovation_schedule');
        }
        return true;
    }

    /**
     * strign updateResturantRenovationSchedule() method update renovation schedule
     * @param int $id The resturant renovation id
     * @param varchar $data The renovation record
     * @todo it update the resturant renovation schedule data from the database using id.
     * @return id
     */
    function updateResturantRenovationSchedule($data, $id = '') {
        if ($id != "") {
            $this->db->where('rnv_id', $id);
            $q = $this->db->get('rest_renovation_schedule');
            if ($q->num_rows() > 0) {
                $this->db->where('rnv_id', $id);
                $this->db->update('rest_renovation_schedule', $data);
                return $id;
            } else {
                $this->db->insert('rest_renovation_schedule', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_renovation_schedule', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addResturantDistanceFrom() method insert distance
     * @param varchar $data The distance record
     * @todo it add the resturant distance from into the DB.
     * @return id
     */
    function addResturantDistanceFrom($data) {
        if ($this->db->insert_batch('rest_distance', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getResturantDistanceFrom() method get the distance
     * @param varchar $dtype The distance type
     * @param int $rest_id The resturant id
     * @todo it fetched the list of resturant distance from the database using resturant id and its type.
     * @return id
     */
    function getResturantDistanceFrom($rest_id, $dtype = '') {
        $this->db->select('id,distance_from,distance,distance_type');
        $this->db->where("rest_id", $rest_id);
        if ($dtype != '') {
            $this->db->where("distance_type", $dtype); /* dtype 1 for  normal, and 2 for airport */
        }
        $query = $this->db->get('rest_distance');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addResturantFacilities() method insert facilities
     * @param varchar $data facility array
     * @todo it add the data of resturant facility in batch(multiple) form.
     * @return ture
     */
    function addResturantFacilities($data) {
        if ($this->db->insert_batch('rest_facilities', $data)) {
            return true;
        }
        return false;
    }


    /**
     * string addResturantRoomsPricingData() method insert resturant pricing record
     * @param varchar $data The resturant pricing record
     * @todo it add resturant room pricing data into the DB.
     * @return id|false
     */
    function addResturantRoomsPricingData($data) {
        if ($this->db->insert('rest_rooms_pricing', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string addResturantRoomPricingComplimentary() 
     * @param varchar $data The resturant pricing record
     * @param id $pricing_id The resturant pricing id
     * @todo method insert resturant room pricing complimentary record
     * @return true
     */
    function addResturantRoomPricingComplimentary($data, $pricing_id = null) {
        if ($pricing_id != "") {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->delete('rest_rooms_pricing_complimentary');
        }
        if ($this->db->insert_batch('rest_rooms_pricing_complimentary', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string updateResturantRoomsPricingData() method update the resturant room pricing record
     * @param int $pricing_id room pricing id
     * @param varchar $data The room pricing record
     * @todo it update the resturant room pricing data into the db using pricing id.
     * @return id
     */
    function updateResturantRoomsPricingData($data, $pricing_id) {
        if ($pricing_id != "") {
            $this->db->where('pricing_id', $pricing_id);
            $q = $this->db->get('rest_rooms_pricing');
            if ($q->num_rows() > 0) {
                $this->db->where('pricing_id', $pricing_id);
                $this->db->update('rest_rooms_pricing', $data);
                return $pricing_id;
            } else {
                $this->db->insert('rest_rooms_pricing', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_rooms_pricing', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string deleteResturantRoomsPricingData() method delete room price
     * @param array $arrayIds room price id
     * @param int $hid resturant id
     * @todo int $hid resturant id
     * @return true
     */
    function deleteResturantRoomsPricingData($arrayIds, $hid) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('rest_id', $hid);
            $this->db->where_not_in('pricing_id', $arrayIds);
            $this->db->delete('rest_rooms_pricing');
        }
        return true;
    }

    /**
     * string addResturantRoomsPricingData() method insert resturant pricing record
     * @param varchar $data The resturant pricing record
     * @todo it add resturant room pricing data into the DB.
     * @return id|false
     */
    function addRestaurantRroomsPricingDetails($data) {
        if ($this->db->insert_batch('rest_rooms_pricing_details', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string updateRestaurantRroomsPricingDetails() 
     * @param varchar $data,(int)$pricingDetId,(int)$pricing_id
     * @todo it update the resturant room pricing data based on pricing id and its id.
     * @return id|false
     */
    function updateRestaurantRroomsPricingDetails($data, $pricingDetId, $pricing_id) {
        if ($pricing_id != "" && $pricingDetId != '') {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->where('id', $pricingDetId);
            $q = $this->db->get('rest_rooms_pricing_details');
            if ($q->num_rows() > 0) {
                $this->db->where('pricing_id', $pricing_id);
                $this->db->where('id', $pricingDetId);
                $this->db->update('rest_rooms_pricing_details', $data);
                return $pricing_id;
            } else {
                $this->db->insert('rest_rooms_pricing_details', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_rooms_pricing_details', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteResturantRestaurantRroomsPricingDetails()
     * @param array $arrayIds,(int) $pricing_id The resturant pricing record
     * @todo method to delete resturant room pricing data from the DB.
     * @return id|false
     */
    function deleteResturantRestaurantRroomsPricingDetails($arrayIds, $pricing_id) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('rest_rooms_pricing_details');
        }
        return true;
    }
    /**
     * string getResturantRoomsPricingData()
     * @param varchar $data The resturant pricing record
     * @todo  method to fetched the data resturant room pricing from the DB.
     * @return id|false
     */
    function getResturantRoomsPricingData($rest_id) {
        $this->db->select('pricing_id,room_type,inclusions,
		curency_code,max_adult,max_child,inventory,period_from,period_to');
        $this->db->where("rest_id", $rest_id);
        $query = $this->db->get('rest_rooms_pricing');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * string getRestaurantRroomsPricingDetails()
     * @param (int)$pricing_id resturant pricing id.
     * @todo  it fetched the list form the database of resturant room pricing using pricing id.
     * @return id|false
     */
    function getRestaurantRroomsPricingDetails($pricing_id) {
        $this->db->select('id,market_id,double_price,
		triple_price,quad_price,breakfast_price,half_board_price,
		all_incusive_adult_price,extra_adult_price,extra_child_price,extra_bed_price');
        $this->db->where("pricing_id", $pricing_id);
        $query = $this->db->get('rest_rooms_pricing_details');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * string getResturantRoomPricingComplimentary() 
     * @param varchar $data The resturant pricing record
     * @todo it fetched the list form the database of resturant room pricing complimentry using pricing id.
     * @return id|false
     */
    function getResturantRoomPricingComplimentary($pricing_id) {
        $this->db->select('cmpl_service_id');
        $this->db->where("pricing_id", $pricing_id);
        $query = $this->db->get('rest_rooms_pricing_complimentary');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string get_gallary() 
     * @todo it fetched the list form the database of resturant gallery using pricing id.
     * @return id|false
     */

    function get_gallary() {
        $this->db->select('id,entity_title');
        $this->db->order_by('id', 'ASC');
        $this->db->where('entity_type', $this->config->item('attribute_resturant_gallery'));
        $query = $this->db->get('entity_attributes');
        return $result = $query->result();
    }
    
    
     /**
     * string insert_images()
     * @param array $data 
     * @todo it add resturant gallery images data into the DB.
     * @return id|false
     */
    function insert_images($data) {
        $id = $this->db->insert('gallery_images', $data);
        return $id;
    }
    /**
     * string update_images()
     * @param array $data (int) $identity
     * @todo it update resturant gallery images data into the DB using perticular identity id.
     * @return id|false
     */
    function update_images($identity, $data) {
        $this->db->where('identifier', $identity);
        $this->db->update('gallery_images', $data);
    }
    /**
     * string get_dbidentity() 
     * @param (int)$identity
     * @todo it fetched the list form the database of resturant room gallery images  using identity.
     * @return true|false
     */
    function get_dbidentity($identity) {
        $this->db->where('identifier', $identity);
        $query = $this->db->get('gallery_images');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    /**
     * string clear_gallery_image_table() 
     * @todo it clear all images whose id is null and removed images 1 day befor form the upload dates.
     * @return true|false
     */
    function clear_gallery_image_table() {

        $this->db->query("DELETE FROM `gallery_images` where `upload_date` < DATE_SUB(NOW() , INTERVAL 24 HOUR) and rest_id IS NULL");
    }
    /**
     * string delete_images() 
     * @param (int)$galid,string($imgname)
     * @todo it delete gallery images from the db using galler id or image name.
     * @return true|false
     */
    function delete_images($imgname, $galid) {

        $this->db->query("delete from gallery_images where image_name = '" . $imgname . "' and gallery_id = '" . $galid . "'");
    }
    /**
     * string gallery_detail() 
     * @todo it fetched the list form the database of resturant gallery.
     * @return true|false
     */
    function gallery_detail() {
        $this->db->select('id,entity_title');
        $this->db->from('entity_attributes');
        $this->db->where("entity_type", $this->config->item('attribute_resturant_gallery'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /**
     * string get_gallaryimg() 
     * @param int($hid) 
     * @todo it fetched the list form the database of gallery imagges using resturant id.
     * @return id|false
     */
    function get_gallaryimg($hid) {
        $this->db->select('gi.gallery_id, gi.id, gi.image_name');
        $this->db->from('gallery_images gi');
        $this->db->where("gi.rest_id", $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * string deleteresturantgalleryimg() 
     * @param int($imgid),int($resturantgalid),int($resturantid)
     * @todo it deleted resturant gallery images from the database from the DB.
     * @return id|false
     */
    function deleteresturantgalleryimg($imgid, $resturantgalid, $resturantid) {
        $this->db->where('gallery_id', $resturantgalid);
        $this->db->where('id', $imgid);
        $this->db->where('rest_id', $resturantid);
        $this->db->delete('gallery_images');
        return $imgid;
    }
    /**
     * string getRestaurantbyid() 
     * @param $hid(int)
     * @todo it fetched the list of resturant by its id.
     * @return id|false
     */
    function getRestaurantbyid($hid) {
        $this->db->select('restaurants.rest_id,
							restaurants.rest_name,
							restaurants.rest_address,
							restaurants.rest_code,
							countries.country_name,
							restaurants.city,
							restaurants.district,
							restaurants.currency,
							restaurants.post_code,
							restaurants.dorak_rating,
							restaurants.star_rating,
							restaurants.child_age_group_id,
							restaurants.purpose,
							restaurants.last_updated,
							restaurants.country_code,
							restaurants.chain_id as chain,
							restaurants.status,
							
							hpt.property_type_id as property_types');
        $this->db->from('restaurants');
        $this->db->join('countries', 'restaurants.country_code = countries.country_code');
        $this->db->join('rest_property_types  hpt', 'restaurants.rest_id = hpt.rest_id', 'left');
        $this->db->where('restaurants.rest_id', $hid);
        $query = $this->db->get();
        $results = $query->row();
        if ($results) {
            return $results;
        } return false;
    }
    /**
     * string deleteResturantpropertytype()
     * @param int $hid
     * @todo method to delete resturant room property type data from the DB.
     * @return id|false
     */
    function deleteResturantpropertytype($hid) {
        $this->db->where('rest_id', $hid);
        $this->db->delete('rest_property_types');
        return true;
    }
    /**
     * string getpropertyid() 
     * @param int $hid
     * @todo it fetched the list form the database of property using pricing id.
     * @return id|false
     */
    function getpropertyid($hid) {

        $this->db->select('hpt.property_type_id');
        $this->db->from('rest_property_types hpt');
        $this->db->where('hpt.rest_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $re) {
                return $ids = array_column($result, 'property_type_id');
            }
        } else {
            return false;
        }
    }

    /**
     * string getcontactinfobyid() 
     * @param int $hid resturant id
     * @todo it fetched the list form the database of resturant contact information using pricing id.
     * @return id|false
     */
    function getcontactinfobyid($hid) {
        $this->db->select('hcd.position as designation, hcd.name, hcd.email, hcd.phone, hcd.extension, hcd.contact_id');
        $this->db->from('rest_contact_details hcd');
        //$this->db->join('entity_attributes as p','p.id = hcd.position AND entity_type="ahpo"');
        $this->db->where('hcd.rest_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return false;
        }
    }
    /**
     * string updateResturantContact()
     * @param $data array format
     * @todo it update resturant contact data from the database.
     * @return id|false
     */
    function updateResturantContact($data, $id = '') {
        if ($id != "") {
            $this->db->where('contact_id', $id);
            $q = $this->db->get('rest_contact_details');
            if ($q->num_rows() > 0) {
                $this->db->where('contact_id', $id);
                $this->db->update('rest_contact_details', $data);
                return $id;
            } else {
                $this->db->insert('rest_contact_details', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_contact_details', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteResturantContact()
     * @param $data(array),$hid(int)
     * @todo it delete resturant contact data from the database.
     * @return id|false
     */
    function deleteResturantContact($arrayIds, $hid) {
        $this->db->where('rest_id', $hid);
        $this->db->where_not_in('contact_id', $arrayIds);
        $this->db->delete('rest_contact_details');
        return true;
    }
    /**
     * string getdistanceForCity()
     * @param $hid(int)
     * @todo it fetched the list from the database of get distance for city using resturant id.
     * @return id|false
     */
    function getdistanceForCity($hid) {

        $this->db->select('hd.distance_from as city_name_normal, hd.distance as dis_city, hd.id');
        $this->db->from('rest_distance hd');
        $this->db->where('hd.distance_type', 1);
        $this->db->where('hd.rest_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return false;
        }
    }
    /**
     * string updateResturantcity()
     * @param $hid(int),array($data)
     * @todo it update the resturant city into the database using id 
     * @return id|false
     */
    function updateResturantcity($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('rest_distance');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->where('distance_type', 1);
                $this->db->update('rest_distance', $data);
                return $id;
            } else {
                $this->db->insert('rest_distance', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_distance', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteResturantcity()
     * @param $hid(int),array($arrayIds)
     * @todo it delete resturant city using resturant id
     * @return id|false
     */
    function deleteResturantcity($arrayIds, $hid) {
        $this->db->where('rest_id', $hid);
        $this->db->where('distance_type', '1');
        $this->db->where_not_in('id', $arrayIds);
        $this->db->delete('rest_distance');
        return true;
    }
     /**
     * string getdistanceForAirport()
     * @param $hid(int)
     * @todo it fetched the list of distance for airport from the DB.
     * @return associative data row.
     */
    function getdistanceForAirport($hid) {

        $this->db->select('hd.distance_from as city_name_airport, hd.distance as dis_airport, hd.id');
        $this->db->from('rest_distance hd');
        $this->db->where('hd.distance_type', 2);
        $this->db->where('hd.rest_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return false;
        }
    }
    /**
     * string updatedistanceForAirport() method insert resturant pricing record
     * @param array $data,int $id.
     * @todo it update resturant distance into the DB.
     * @return last inserted id
     */
    function updatedistanceForAirport($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('rest_distance');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->where('distance_type', '2');
                $this->db->update('rest_distance', $data);
                return $id;
            } else {
                $this->db->insert('rest_distance', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_distance', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deletedistanceForAirport()
     * @param array $arrayIds,int $hid.
     * @todo it delete resturant distance for airport from the DB.
     * @return true or false
     */
    function deletedistanceForAirport($arrayIds, $hid) {
        $this->db->where('rest_id', $hid);
        $this->db->where('distance_type', '2');
        $this->db->where_not_in('id', $arrayIds);
        $this->db->delete('rest_distance');
        return true;
    }
    /**
     * string resturantFacility()
     * @param int $hid.
     * @todo it fetched the list form the database of resturant room facility using pricing id.
     * @return list of resturant facility.
     */
    function resturantFacility($hid) {
        $this->db->select('*');
        $this->db->from('rest_facilities');
        $this->db->where('rest_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            //echo $this->db->last_query(); die;
            foreach ($result as $re) {
                return $ids = array_column($result, 'facility_id');
            }
        } else {
            return false;
        }
    }
    /**
     * string deleteresturantFacility()
     * @param int $hid.
     * @todo it deleted resturant facility into the DB based on resturant id.
     * @return true or false.
     */
    function deleteresturantFacility($hid) {
        $this->db->where('rest_id', $hid);
        $this->db->delete('rest_facilities');
        return true;
    }

    /**
     * string roomFacility()
     * @param int $hid.
     * @todo it deleted resturant facility into the DB based on resturant id.
     * @return true or false.
     */
    function roomFacility($hid) {
        $this->db->select('hcs.cmpl_service_id');
        $this->db->from('rest_complimentary_services hcs');
        $this->db->where('hcs.rest_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $re) {
                return $ids = array_column($result, 'cmpl_service_id');
            }
        } else {
            return false;
        }
    }
    /**
     * string deleteroomFacility()
     * @param int $hid.
     * @todo it deleted resturant room facility into the DB based on resturant id.
     * @return true or false.
     */
    function deleteroomFacility($hid) {
        $this->db->where('rest_id', $hid);
        $this->db->delete('rest_complimentary_services');
        return true;
    }
    /**
     * string resturantBankAccount()
     * @param int $hid.
     * @todo it fetched resturant bank accounts into the DB based on resturant id.
     * @return true or false.
     */
    function resturantBankAccount($hid) {
        $this->db->select('*');
        $this->db->from('rest_bank_accounts hba');
        $this->db->where('hba.rest_id', $hid);
        $query = $this->db->get();
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    /**
     * string updateResturantbankaccounts()
     * @param int $hid.
     * @todo it update resturant bank accounts into the DB based on resturant id.
     * @return true or false.
     */
    function updateResturantbankaccounts($data, $id = '') {

        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('rest_bank_accounts');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('rest_bank_accounts', $data);
                return $id;
            } else {
                $this->db->insert('rest_bank_accounts', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('rest_bank_accounts', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteResturantbankaccounts()
     * @param int $hid.
     * @todo it delete(multiple) resturant bank accounts into the DB based on resturant id.
     * @return true or false.
     */
    function deleteResturantbankaccounts($arrayIds = false, $hid) {
        $this->db->where('rest_id', $hid);
        if ($arrayIds)
            $this->db->where_not_in('id', $arrayIds);
        $this->db->delete('rest_bank_accounts');
        return true;
    }
    /**
     * string deleteResturantbankaccount()
     * @param int $hid.
     * @todo it delete resturant bank accounts into the DB based on resturant id.
     * @return true or false.
     */
    function deleteResturantbankaccount($acId, $hid) {
        $this->db->where('rest_id', $hid);
        if ($acId != "")
            $this->db->where('id', $acId);
        $this->db->delete('rest_bank_accounts');
        return true;
    }
    /**
     * string updateRestaurantInformation()
     * @param int $hid.
     * @todo it update the resturant all informatin based on resturant id to associative data.
     * @return true or false.
     */
    function updateRestaurantInformation($hid, $resturant_detail) {
        $this->db->where('rest_id', $hid);
        $this->db->update('restaurants', $resturant_detail);
    }
	
	/**
     * @method insert_common_menu()
     * @param array $data 
     * @todo it add resturant common menu images data into res_menu_markets.
     * @return id|false
     */
    function insert_common_menu($data) {
        $id = $this->db->insert('res_menu_markets', $data);
        return $id;
    }
	
	/**
     * @method get_common_menu_files()
     * @param array $data 
     * @todo this method used to fetch resturant common menu files from res_menu_markets.
     * @return id|false
     */
    function get_common_menu_files($rest_id) {
		$this->db->where('market_id',0);
		$this->db->where('rest_id',$rest_id);
        $query = $this->db->get('res_menu_markets');
		$result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
	
	/**
     * @method insert_common_menu()
     * @param array $data 
     * @todo it add resturant common menu images data into res_menu_markets.
     * @return true|false
     */
    function delete_common_menu($rest_id,$cid) {
		$this->db->where('rest_id',$rest_id);
		$this->db->where('id',$cid);
        return $this->db->delete('res_menu_markets');
    }
    
     /**
     * string MarketFiles()
     * @param int $hid.
     * @todo it update the resturant all informatin based on resturant id to associative data.
     * @return true or false.
     */
    function MarketFiles($res_id = null) {
        $market = getMarkets();
//        dump($market);
        $this->db->select('entity_attributes.id as et_id,market_id,rest_id,files,entity_title,res_menu_markets.id as ID');
        $this->db->from('res_menu_markets');
        $this->db->join('entity_attributes', 'entity_attributes.id = res_menu_markets.market_id AND entity_attributes.entity_type = "am"');
        $this->db->where('rest_id',$res_id);
        $query = $this->db->get();
        $results = $query->result();
        $html = '';
        if ($query->num_rows() > 0) {
            $html .= '<div class="marketfiles"><table class="table"><thead><tr><th>Market</th><th>Menu</th></tr></thead>';
            $html .= '<tbody>';
            foreach ($market as $m) {
                $cnt = 0;
                $html2 = '';
                $cnt2 = false;
                foreach ($results as $result) {
                    if ($m->id == $result->market_id) {
                        if ($cnt != $m->id) {
                            $cnt = $m->id;
                            $html2 .= '<tr><td>' . $result->entity_title . '</td><td>';
                            $cnt2 = true;
                        }
                        $html2 .= '<span>' . $result->files . '</span>';
                        $html2 .= '<a href="javascript:void(0)" return onclick="deleteMarket('.$result->ID.')" class="close_market" id = "'.$result->ID.'"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a>';
                    }
                }
                if ($cnt2) {
                    $html2 .= '</td></tr>';
                }
                $html .=$html2;
            }
            $html .='</tbody>';
            $html .= '</table></div>';
            return $html;
        } else {
            return $html;
        }
    }
    
    function delete_market_files($id = NULL){
        $result = false;
        if(!empty($id)){
            $this->db->where('id', $id);
            $result = $this->db->delete('res_menu_markets');
            return $result;
        }
        return $result;
        
    }
    function delete_commonimg($imgid, $marketid) {

        $this->db->query("delete from res_menu_markets where files = '" . $imgid . "' and market_id = '" . $marketid . "'");
    }
}
