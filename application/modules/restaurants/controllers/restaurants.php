<?php

/**
 * @author tis
 * @author M
 * @description this controller  is for  add/ edit delete restaurant  related  information
 * @uses AdminController::Common base controller to perform all type of action.
 * functions : 
  
  _init()            this method is used to load all libraries and model.
  index()            this method is used to list restaurant listing.
  resturantListingAjax()        this method is used to fetch restaurant list from via ajax.
  delete_resturant()            this method is used to delete restaurant from database.
  massActions()  	    this method is used to delete restaurant in bulk from the database .
  minDist()          this is callback meathod to validate minimum distance.
  maxDist()         this is callback meathod to validate minimum distance.
  information()       this method is used to add restaurant information with all attributes like facility banking price etc.
  upload ()          this method is used to upload gallery image.
  deleteimg()       this method is used to delete gallery image.
  edit_information()     this method is used to update restaurant information based on perticular restaurant id.
  deleteresturantimg()     this method is used to delete restaurant gallery image in the bulk format.
  uploadGaleryImage()  this method is used to upload gallery image zip or normat browse.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Restaurants extends AdminController {
    
    /** @var string|null Should contain a user identification */

    public static $identityfier;

    function __construct() {
        parent::__construct();
        $this->_init();
    }

     /*
     * @method string init()
     * @param N/A
     * @todo Loads all required models(database tables),helpers(views),libraries(third party eg:phpexcel)
     * @return nothing 
     */
    private function _init() {
        if (!$this->ion_auth->logged_in()) {
            // redirect non logged user to  login page
            redirect('auth/login', 'refresh');
        }
        $this->identityfier = $this->session->userdata('session_id') . '_' . time();
        $this->load->helper(array('form', 'html', 'form_field_values', 'file', 'directory'));
        $this->load->library('form_validation');
        $this->load->helper('array_column');
        $this->load->library('Datatables');
        $this->output->set_meta('description', 'Dorak Restaurant');
        $this->output->set_meta('title', 'Dorak Restaurant');
        $this->output->set_title('Dorak Restaurant');
        $this->load->model('restaurants/restaurant_model');
        $this->accessDenidMessage = $this->config->item('access_not_allowed_message');
        $this->contractFileDir = $this->config->item('upload_contract_file_dir');
        $this->marketFileDir = $this->config->item('upload_market_file_dir');
        $this->resturantFileDir = $this->config->item('resturant_gallery_dir');
        $this->restaurant_model->clear_gallery_image_table();
        if (!checkAccess($this->accessLabelId, 'restaurants', 'view')) {
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
    }

     /**
     * string index() used to fetch Restaurant listing and search Restaurant sepecific condition.
     * @todo it will load all the Restaurants listing and search Restaurant by condition.
     * @return Restaurant listing.
     */
    
    public function index() {
        $this->session->unset_userdata('stepsess');
        $this->output->set_meta('title', 'Restaurant Listing');
        $this->output->set_title('Restaurant Listing');
        $search_terms = array();

        if (!empty($_POST)) {
            // store  posted data in session for ajax pagination  and data tables  search results listing
            $hname = removeExtraspace($this->input->post('rest_name'));
            $hcode = removeExtraspace($this->input->post('rest_code'));
            $this->session->set_userdata('searchByRestaurantName', $hname);
            $this->session->set_userdata('searchByRestaurantCode', $hcode);
            $this->session->set_userdata('searchByDorakRating', $this->input->post('dorak_rating'));
            $this->session->set_userdata('searchByPropertyType', $this->input->post('property_type'));
            $this->session->set_userdata('searchByStatus', $this->input->post('status_id'));
            $this->session->set_userdata('searchByPurpose', $this->input->post('purpose'));
            $this->session->set_userdata('searchByChainId', $this->input->post('resturant_chain_id'));
            $this->session->set_userdata('searchByCountry', $this->input->post('country_code'));
            $this->session->set_userdata('searchByCity', $this->input->post('city'));
            $this->session->set_userdata('searchByDistrict', $this->input->post('district'));
            $search_terms['searchByRestaurantName'] = $hname;$search_terms['searchByRestaurantCode'] = $hcode;$search_terms['searchByDorakRating'] = $this->input->post('dorak_rating');$search_terms['searchByPropertyType'] = $this->input->post('property_type');$search_terms['searchByStatus'] = $this->input->post('status_id');$search_terms['searchByPurpose'] = $this->input->post('purpose');$search_terms['searchByChainId'] = $this->input->post('resturant_chain_id');
            $search_terms['searchByCountry'] = $this->input->post('country_code');$search_terms['searchByCity'] = $this->input->post('city');$search_terms['searchByDistrict'] = $this->input->post('district');
        } else {
            $this->session->unset_userdata('searchByRestaurantName');$this->session->unset_userdata('searchByRestaurantCode');$this->session->unset_userdata('searchByDorakRating');
            $this->session->unset_userdata('searchByPropertyType');$this->session->unset_userdata('searchByStatus');
            $this->session->unset_userdata('searchByPurpose');$this->session->unset_userdata('searchByChainId');$this->session->unset_userdata('searchByCountry');
            $this->session->unset_userdata('searchByCity');$this->session->unset_userdata('searchByDistrict');
        }
        $countriesOptions = getCountriesOptions('sel'); /* using helper with select option */
        $dc = $this->config->item('default_country_code');
        $ccode = $this->input->post('country_code') != '' ? $this->input->post('country_code') : '';
        if ($ccode != "") {
            $citiesOptions = getCountyCitiesOptions($ccode, 'sel');
        } else {
            $citiesOptions = array('' => 'Select');
        }
        $scity = $this->input->post('city') != '' ? $this->input->post('city') : '';
        if ($scity != "") {
            $districtOptions = getCityDistrictOptions($scity, 'sel');
        } else {
            $districtOptions = array('' => 'Select');
        }
        $this->data = array(
        'importAction' => site_url('restaurants/import/process'),
        'rest_name' => array('id' => 'rest_name','name' => 'rest_name','maxlength' => '100','size' => '100','value' => $this->input->post('rest_name')),
        'rest_code' => array('id' => 'rest_code','name' => 'rest_code','maxlength' => '100','size' => '100','value' => $this->input->post('rest_code')),
        'district' => array('id' => 'district','tabindex' => 12,'data-validation' => 'custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-80",'data-validation-optional' => 'true','name' => 'district','maxlength' => '100','size' => '100',
        'value' => set_value('district')),'dorak_rating_options' => getDorakRatings(),'status_options' => getRestaurantStatus('Select'),'purpose_options' => getPurposeOptions(),'chain_options' => getRestaurantChainsOptions(),'countriesOptions' => $countriesOptions,'citiesOptions' => $citiesOptions,'districtOptions' => $districtOptions,'property_type_options' => getPropertyTypeOptions(),);
        $this->data['p_country_code'] = $this->input->post('country_code') != '' ? $this->input->post('country_code') : '';
        $this->data['posted_city'] = $this->input->post('city') != '' ? $this->input->post('city') : '';
        $this->data['posted_district'] = $this->input->post('district') != '' ? $this->input->post('district') : '';
        $this->data['serched_chain_id'] = $this->input->post('resturant_chain_id') != '' ? $this->input->post('resturant_chain_id') : '';
        $this->data['serched_dorak_rating'] = $this->input->post('dorak_rating') != '' ? $this->input->post('dorak_rating') : '';
        $this->data['serched_property_type'] = $this->input->post('property_type') != '' ? $this->input->post('property_type') : '';
        $this->data['serched_status'] = $this->input->post('status_id') != '' ? $this->input->post('status_id') : '';
        $this->data['serched_purpose'] = $this->input->post('purpose') != '' ? $this->input->post('purpose') : '';
        $limit = 10;
        $offset = 0;
        $this->data['restaurants'] = $this->restaurant_model->getRestaurantsList($search_terms, $limit, $offset);
        $this->load->view('restaurant_listing', $this->data);
    }

    /**
     * @method string resturantListingAjax()
     * @method use datatable to view the list of restaurants called by ajax	
     * @return datatable library generated list of restaurants.
     */
    function resturantListingAjax() {
        $actionlinks = '';
        if (checkAccess($this->accessLabelId, 'restaurants', 'edit'))
            $actionlinks.='<a class="edit_rec" title="Edit" href="restaurants/edit_information/$1">Edit</a> ';
        if (checkAccess($this->accessLabelId, 'restaurants', 'delete'))
            $actionlinks.='<a title="Delete" class="delete_rec" href="' . base_url() . 'restaurants/delete_resturant/$1" onclick="return confirm(\'Are you sure to delete this restaurant?\')">Delete</a>';
        if (trim($actionlinks) == "") {
            $actionlinks = 'No action permitted';
        }
        $this->datatables->select('
                                            restaurants.rest_id,
                                            restaurants.rest_code,
                                            restaurants.rest_name,	
                                            cn.entity_title as chain,
                                            restaurants.dorak_rating,
                                            restaurants.city,							
                                            countries.country_name,
                                            ea.entity_title as status,
                                            eap.entity_title as property_types,	
                                            eaps.entity_title as purpose
							', False)
				->add_column('Actions', '$1','get_actions_links(restaurants.rest_id,restaurants.rest_name,'.$this->accessLabelId.')')
                //->add_column('Actions', $actionlinks, 'restaurants.rest_id')
                //->edit_column('restaurants.star_rating', '<span class="starRating"><span id="srating$1">$1</span></span>', 'restaurants.star_rating')
                ->edit_column('restaurants.dorak_rating', '<span class="dRating"><span id="drating$1">$1</span></span>', 'restaurants.dorak_rating')
                ->edit_column('property_types', '$1', 'get_assigned_property_types(restaurants.rest_id)')
                ->join('entity_attributes ea', 'restaurants.status = ea.id AND ea.entity_type="ahs"')
                ->join('countries', 'restaurants.country_code = countries.country_code')
                ->join('entity_attributes cn', 'cn.id = restaurants.chain_id AND cn.entity_type="ahc"', 'left')
                ->join('entity_attributes eaps', 'eaps.id = restaurants.purpose', 'left')
                ->join('rest_property_types  hpt', 'restaurants.rest_id = hpt.rest_id', 'left')
                ->join('entity_attributes  eap', 'eap.id = hpt.property_type_id AND eap.entity_type="apt"', 'left')
                ->group_by('restaurants.rest_id');
        if ($this->session->userdata('searchByRestaurantName') && $this->session->userdata('searchByRestaurantName') != "") {
            $this->datatables->like('restaurants.rest_name', $this->session->userdata('searchByRestaurantName'), 'both');
        }
        if ($this->session->userdata('searchByRestaurantCode') && $this->session->userdata('searchByRestaurantCode') != "") {
            $this->datatables->like('restaurants.rest_code', $this->session->userdata('searchByRestaurantCode'), 'both');
        }
        if ($this->session->userdata('searchByCountry') && $this->session->userdata('searchByCountry') != "") {
            $this->datatables->where('restaurants.country_code', $this->session->userdata('searchByCountry'));
        }
        if ($this->session->userdata('searchByCity') && $this->session->userdata('searchByCity') != "") {
            $this->datatables->where('restaurants.city', $this->session->userdata('searchByCity'));
        }
        if ($this->session->userdata('searchByDistrict') && $this->session->userdata('searchByDistrict') != "") {
            $this->datatables->where('restaurants.district', $this->session->userdata('searchByDistrict'));
        }
        if ($this->session->userdata('searchByPurpose') && $this->session->userdata('searchByPurpose') != "") {
            $this->datatables->where('restaurants.purpose', $this->session->userdata('searchByPurpose'));
        }
        if ($this->session->userdata('searchByDorakRating') != "") {
            $this->datatables->where('restaurants.dorak_rating', $this->session->userdata('searchByDorakRating'));
        } 
        if ($this->session->userdata('searchByStatus') && $this->session->userdata('searchByStatus') != "") {
            $this->datatables->where('restaurants.status', $this->session->userdata('searchByStatus'));
        }
        if ($this->session->userdata('searchByChainId') && $this->session->userdata('searchByChainId') != "") {
            $this->datatables->where('restaurants.chain_id', $this->session->userdata('searchByChainId'));
        }
        if ($this->session->userdata('searchByPropertyType') && $this->session->userdata('searchByPropertyType') != "") {
            $this->datatables->where('eap.id', $this->session->userdata('searchByPropertyType'));
        }
        $this->datatables->from('restaurants');
        echo $this->datatables->generate();
        exit();
    }

   /**
     * @method string delete_resturant()
     * @method use datatable to view the list of restaurants called by ajax
    *  @param restaurantId $hid - Restaurant id(primary key) used to delete Restaurant.
     * @return datatable library generated list of Restaurants.
     */
    function delete_resturant($hid = null) {
        if ($hid != "") {
            try {
                $this->restaurant_model->deleteRestaurant($hid);
            } catch (Exception $e) {
                $sdata['message'] = 'Selected Restaurant not deleted!' . $e->getMessage();
                $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'error');
                $this->session->set_userdata($flashdata);
                redirect('/restaurants/', 'refresh');
            }
            $sdata['message'] = 'Selected Restaurant deleted!';
            $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'sucess');
            $this->session->set_userdata($flashdata);
            redirect('/restaurants/', 'refresh');
        }
        $this->index();
    }

    /**
     * @method string massActions()
     * @description delete Restaurants multiple Restaurants by ids
     * @return true|false
     */
    function massActions() {
        if (!empty($_POST)) {
            $sdata = array();
            $resturantIdsToDelete = $this->input->post('id');
            if ($resturantIdsToDelete && count($resturantIdsToDelete) > 0) {
                // delete all the  Restaurants  
                try {
                    $this->restaurant_model->deleteRestaurants($resturantIdsToDelete);
                } catch (Exception $e) {
                    $sdata['message'] = 'Selected Restaurants not deleted!' . $e->getMessage();
                    $flashdata = array(
                        'flashdata' => $sdata['message'],
                        'message_type' => 'error'
                    );
                    $this->session->set_userdata($flashdata);
                    redirect('/restaurants/', 'refresh');
                }
            } else {
                $sdata['message'] = 'Please select records  to delete!';
                $flashdata = array(
                    'flashdata' => $sdata['message'],
                    'message_type' => 'notice'
                );
                $this->session->set_userdata($flashdata);
            }
        }
        redirect('/restaurants');
    }

    /**
     * @method string minDist()
     * @param $input(float) - distance data via post to validate.
     * @method validate minimum distance.
     * @return true|false
     */
    public function minDist($input) {
        # this function validate   decimal  number  for distance min limit
        if ($input != '') {
            $pieces = explode(".", $input);
            if (isset($pieces[0]) && $pieces[0] == '') {
                $this->form_validation->set_message('min_dist', 'The %s field should have at least one digit before decimal');
                return FALSE;
            }
            if (isset($pieces[1]) && $pieces[1] > 99) {
                $this->form_validation->set_message('min_dist', 'The %s field can not accept more then 2 digit after decimial');
                return FALSE;
            }

            if ($input < 0) {
                $this->form_validation->set_message('min_dist', 'The %s field invalid value');
                return FALSE;
            }
        }
        return TRUE;
    }
    /**
     * @method string maxDist()
     * @param $input(float) - distance data via post to validate.
     * @method validate maximum distance.
     * @return true|false
     */
    public function maxDist($input = '') {
        # this function validate   decimanl  number  for distance max limit
        if ($input != '') {
            $pieces = explode(".", $input);
            if (isset($pieces[0]) && $pieces[0] > 999) {
                $this->form_validation->set_message('max_dist', 'The %s field exceed the max limit');
                return FALSE;
            }
            if (isset($pieces[1]) && $pieces[1] > 99) {
                $this->form_validation->set_message('max_dist', 'The %s field can not accept more then 2 digit after decimial');
                return FALSE;
            }
            if ($input < 0) {
                $this->form_validation->set_message('max_dist', 'The %s field invalid value');
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * @method: string information()
     * @todo: Add Restaurant details  in database for all tabs like info,facilities,banking,contract.
     * @return : NULL
     */
    public function information() {
        if (!checkAccess($this->accessLabelId, 'restaurants', 'add')) { // check if user have access to this section
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }

        $data = array('message' => '');
        $this->load->library('upload');
        $this->output->set_meta('description', 'Restaurant Information');
        $this->output->set_meta('title', 'Restaurant Information');
        $this->output->set_title('Restaurant Information');
        $user = $this->ion_auth->user()->row();
        if ($this->session->userdata('stepsess') == '#tab-5') {
            $this->session->set_userdata('stepsess', '#tab-6');
        }
        if (!empty($_POST)) {
            $identity = $this->input->post('identity');

            $step = $this->input->post('step'); /* input for save data on tab basis */
            $this->session->set_userdata('stepsess',  $step); /* session start for tabs */

            ######################### server sides validation rules ################################ 
            
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('rest_name', 'Restaurant name', 'required|min_length[10]|max_length[100]');
            $this->form_validation->set_rules('rest_address', 'Address', 'required|min_length[15]|max_length[150]');
            $this->form_validation->set_rules('post_code', 'Zip Code', 'required|alpha_numeric|min_length[4]|max_length[8]');
            $this->form_validation->set_rules('currency', 'Currency', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('country_code', 'Country', 'required');
            $this->form_validation->set_rules('distance_from_city[1][distance]', 'Distance From City', 'callback_min_dist|callback_max_dist');
            $this->form_validation->set_rules('distance_from_airport[1][distance]', 'Distance From Airport', 'callback_minDist|callback_maxDist');
            $this->form_validation->set_rules('property_type_oth', 'property_type_oth', 'min_length[3]');
            $this->form_validation->set_rules('purpose_options_oth', 'purpose_options_oth', 'min_length[3]');
            $this->form_validation->set_rules('hotel_chain_oth', 'hotel_chain_oth', 'min_length[5]');
            $this->form_validation->set_rules('contact[1][position]', 'Contact position', 'required');
            $this->form_validation->set_rules('contact[1][name]', 'Contact Name', 'required');
            $this->form_validation->set_rules('contact[1][email]', 'Contact Email', 'required|valid_email');
            $this->form_validation->set_rules('contact[1][phone]', 'Contact Phone', 'required');
            
            ###################################End validation rules###########################################
            
            /**********************************If all validation has successed****************/
            if ($this->form_validation->run($this) != FALSE) {
                $purpose = $this->input->post('purpose');
                if ($purpose != '' && $purpose == 'Other') {
                    // insert new purpose
                    $purpose_options_oth = $this->input->post('purpose_options_oth');
                    if ($purpose_options_oth != '') {
                        // check if already in database if not then add
                        if ($pid = $this->restaurant_model->getPurpose($purpose_options_oth)) {
                            $purposeId = $pid;
                        } else { // add data in database 
                            $resturant_purpose = array('title' => $purpose_options_oth);
                            $purposeId = $this->restaurant_model->addPurpose($resturant_purpose);
                        }
                    }
                } else {
                    $purposeId = $purpose;
                }
                // add new restaurant chain if  user choose  other options the specified  value need to store int  restaurant chain list table
                $resturantchain = $this->input->post('chain_id');
                if ($resturantchain != '' && $resturantchain == 'Other') {
                    // insert new purpose
                    $hotel_chain_oth = $this->input->post('hotel_chain_oth');
                    if ($hotel_chain_oth != '') {
                        /* check if already in database if not then add */
                        if ($hcid = $this->restaurant_model->getRestaurantChain($hotel_chain_oth)) {
                            $chainId = $hcid;
                        } else {
                            /* add data in database  */
                            $resturant_chain = array('entity_title' => $hotel_chain_oth, 'entity_type' => $this->config->item('attribute_resturant_chain'));
                            $chainId = $this->restaurant_model->addRestaurantChain($resturant_chain);
                        }
                    }
                } else {
                    $chainId = $resturantchain;
                }
                $contactData = array();  /* # we'll store each contact row in this array */
                $nearestDistanceData = array();  /* we'll store each distance row in this array */
                $resturantBankData = array();  /* we'll store each bank details row in this array */
                $resturantRenovationData = array();  /* we'll store each bank renovation row in this array */
                /* Setup a counter for looping through post arrays */
                $bnk_details = $this->input->post('bank');
                $resturantContactDetails = $this->input->post('contact');
                $aplicable_child_age_group = $this->input->post('aplicable_child_age_group');
                /* Setting values for  Restaurant tabel fields */
                $resturant_data = array('rest_name' => $this->input->post('rest_name'),'rest_address' => $this->input->post('rest_address'),'chain_id' => $chainId,'purpose' => $purposeId,'city' => $this->input->post('city'),'country_code' => $this->input->post('country_code'),'district' => $this->input->post('district'),'post_code' => $this->input->post('post_code'),'currency' => $this->input->post('currency'),'star_rating' => $this->input->post('star_rating'),'dorak_rating' => $this->input->post('dorak_rating'),'commisioned' => $this->input->post('commisioned'),'status' => $this->input->post('status'),'child_age_group_id' => $aplicable_child_age_group,'created_byuser' => $user->id);
                /* check if Restaurant details already added on the basis of name city and postcode */
                if ($this->restaurant_model->getRestaurantDetails($resturant_data['rest_name'], $resturant_data['city'], $resturant_data['post_code'])) {
                    $data['message'] = 'Restaurant is already exists!';
                    $flashdata = array('flashdata' => $data['message'],'message_type' => 'notice');
                    $this->session->set_userdata($flashdata);
                } else {
                    /* if Restaurant details are not already added before then  add  new Restaurant in database,Transfering data to Model */
                    if ($inserted_rest_id = $this->restaurant_model->insertDetails($resturant_data)) {
                        $data['message'] = '<li>Restaurant Information</li>';
                        // sets Restaurant code  as per inserted  id and prefic  defiened in config
                        //Format of the Restaurant Code: DRK-_<Auto generated sequence number>-<First Four Characters of the Restaurant name>-<3 character City Code>-<3 character Country Code>
                        $hCodePrefix = $this->config->item('rest_code_prefix');
                        $first4CharOfName = substr($resturant_data['rest_name'], 0, 4);
                        $first3CharOfCity = substr($resturant_data['city'], 0, 3);
                        $resturantCountryCode = $resturant_data['country_code'];
                        $resturantCityCode = getCityCode($resturant_data['city']); // using helper				
                        $hcode = $hCodePrefix . $inserted_rest_id . "-" . $first4CharOfName;
                        if ($resturantCityCode && $resturantCityCode != "") {
                            $hcode.="-" . $resturantCityCode;
                        } else {
                            $hcode.="-" . $first3CharOfCity;
                        }
                        if ($resturantCountryCode && $resturantCountryCode != "")
                            $hcode.="-" . $resturantCountryCode;
                        $uhData = array('rest_code' => $hcode);
                        $this->restaurant_model->updateRestaurantDetails($inserted_rest_id, $uhData);
                        /* add new restaurant property type  if  user choose  other options */
                        $resturantPtype = $this->input->post('property_type');
                        if ($resturantPtype != '' && count($resturantPtype) > 0) {
                            $addResturantPropertyTypesData = array();
                            foreach ($resturantPtype as $HpType) {
                                if ($HpType != '') {
                                    if ($HpType == 'Other') {
                                        /* insert new purpose */
                                        $property_type_oth = $this->input->post('property_type_oth');
                                        if ($property_type_oth != '') {
                                            /* check if already in database if not then add */
                                            if ($hPid = $this->restaurant_model->getRestaurantPropertyType($property_type_oth)) {
                                                $resturantPtypeId = $hPid;
                                            } else {
                                                /* add data in database */
                                                $resturantPtype = array('entity_title' => $property_type_oth, 'entity_type' => $this->config->item('attribute_property_types'));
                                                $resturantPtypeId = $this->restaurant_model->addRestaurantPropertyType($resturantPtype);
                                            }
                                        }
                                    } else {
                                        $resturantPtypeId = $HpType;
                                    }
                                    $addResturantPropertyTypesData[] = array('rest_id' => $inserted_rest_id,'property_type_id' => $resturantPtypeId);
                                }
                            }
                            if (count($addResturantPropertyTypesData) > 0) {
                                $this->restaurant_model->addResturantPropertyTypes($addResturantPropertyTypesData);
                            }
                        }
                        /* if Resturant Property Types Specified */
                        /* if restaurant added, insert contacts details  of the last added restaurants */
                        if (count($resturantContactDetails) > 0) {
                            foreach ($resturantContactDetails as $resturantContact) {
                                $contactData[] = array('rest_id' => $inserted_rest_id,'position' => $resturantContact['position'],'name' => $resturantContact['name'],'email' => $resturantContact['email'],'phone' => $resturantContact['phone'],'extension' => $resturantContact['extension']);
                            }
							
                            $this->restaurant_model->addResturantContactInfo($contactData);
                        }
                        /* insert distances	from details  of the inserted restaurants */
                        $dst_distance_from_city = $this->input->post('distance_from_city');
                        $dst_distance_from_airport = $this->input->post('distance_from_airport');
                        if (!empty($dst_distance_from_airport) && count($dst_distance_from_airport) > 0) {
                            foreach ($dst_distance_from_airport as $dst_distance_airport) {
                                if ($dst_distance_airport['name'] != "" && $dst_distance_airport['distance'] != "") {
                                    $nearestDistanceData[] = array('rest_id' => $inserted_rest_id,'distance_from' => $dst_distance_airport['name'],'distance' => $dst_distance_airport['distance'],'distance_type' => 2);
                                }
                            }
                        }
                        if (!empty($dst_distance_from_city) && count($dst_distance_from_city) > 0) {
                            foreach ($dst_distance_from_city as $dst_distance_city) {
                                if ($dst_distance_city['name'] != "" && $dst_distance_city['distance'] != "") {
                                    $nearestDistanceData[] = array('rest_id' => $inserted_rest_id,'distance_from' => $dst_distance_city['name'],'distance' => $dst_distance_city['distance'],'distance_type' => 1);
                                }
                            }
                        }
                        if (!empty($nearestDistanceData) && count($nearestDistanceData) > 0) {
                            # add restaurant nearest  distance 				
                            $this->restaurant_model->addResturantDistanceFrom($nearestDistanceData);
                        }
                        # add restaurant banking details if posted , posted data are in multy dimensanol array field name
                        if (!empty($bnk_details) && count($bnk_details) > 0) {
                            foreach ($bnk_details as $bnk) {
                                if ($bnk['account_number'] != "" && $bnk['account_name'] != "" && $bnk['bank_name'] != "" && $bnk['bank_address'] != "") {
                                    $resturantBankData[] = array('rest_id' => $inserted_rest_id,'iban_code' => $bnk['iban_code'],'account_number' => $bnk['account_number'],'account_name' => $bnk['account_name'],'bank_name' => $bnk['bank_name'],'bank_address' => $bnk['bank_address'],'branch_name' => $bnk['branch_name'],'branch_code' => $bnk['branch_code'],'swift_code' => $bnk['swift_code'],'bank_ifsc_code' => $bnk['bank_ifsc_code']);
                                }
                            }

                            if (!empty($resturantBankData) && count($resturantBankData) > 0) {
                                # add restaurant banking details 				
                                if ($this->restaurant_model->addResturantBankingInfo($resturantBankData)) {
                                    $data['message'] = '<li> Restaurant Banking Details</li>';
                                }
                            }
                        }
                        # add restaurant renovation shedule				
                        $RenovationSchedules = $this->input->post('rnv_shedule');
                        if (count($RenovationSchedules) > 0) {
                            foreach ($RenovationSchedules as $RenovationSchedule) {
                                if ($RenovationSchedule['date_from'] != "" && $RenovationSchedule['date_to'] != "" && $RenovationSchedule['renovation_type'] != "" && $RenovationSchedule['area_effected'] != "") {
                                    $resturantRenovationData[] = array('rest_id' => $inserted_rest_id,'date_from' => convert_mysql_format_date($RenovationSchedule['date_from']),'date_to' => convert_mysql_format_date($RenovationSchedule['date_to']),'renovation_type' => $RenovationSchedule['renovation_type'],'area_effected' => $RenovationSchedule['area_effected']);
                                }
                            }

                            if (!empty($resturantRenovationData) && count($resturantRenovationData) > 0) {
                                # add restaurant banking details 				
                                if ($this->restaurant_model->addResturantRenovationSchedule($resturantRenovationData)) {
                                    $data['message'] = '<li> Restaurant Renovation Details</li>';
                                }
                            }
                        }
                        # add restaurant complementary  services
                        $cmplServices = $this->input->post('complimentary');
                        if ($cmplServices != "" && count($cmplServices) > 0) {
                            $resturantCmplServices = array();
                            foreach ($cmplServices as $cmplServiceId) {
                                $resturantCmplServices[] = array('rest_id' => $inserted_rest_id,'cmpl_service_id' => $cmplServiceId);
                            }
                            if ($this->restaurant_model->addResturantComplimentaryServices($resturantCmplServices)) {
                                $data['message'] = '<li>Restaurant Complimentary Details</li>';
                            }
                        }
                        # add restaurant Facilities
                        $facilities = $this->input->post('facilities');
                        if ($facilities != "" && count($facilities) > 0) {
                            $resturantFacilities = array();
                            foreach ($facilities as $facility_id) {
                                $resturantFacilities[] = array('rest_id' => $inserted_rest_id,'facility_id' => $facility_id);
                            }
                            if ($this->restaurant_model->addResturantFacilities($resturantFacilities)) {
                                $data['message'] = '<li>Restaurant Facilities Details</li>';
                            }
                        }
                        # add restaurant complementary room
                        $cmpli_room_night = $this->input->post('cmpli_room_night');
                        $cmpli_date_from = convert_mysql_format_date($this->input->post('cmpli_date_from'));
                        $cmpli_date_to = convert_mysql_format_date($this->input->post('cmpli_date_to'));
                        $cmpli_upgrade = $this->input->post('cmpli_upgrade');
                        $cmpli_exclude_dates = $this->input->post('cmpli_exclude_date');
                        if ($cmpli_room_night != "" && $cmpli_date_from != "" && strtotime($cmpli_date_from) > strtotime("now")) {
                            $complimentary_room_data = array('rest_id' => $inserted_rest_id,'rest_id' => $inserted_rest_id,'room_night' => $cmpli_room_night,'start_date' => $cmpli_date_from,'end_date' => $cmpli_date_to,'upgrade' => $cmpli_upgrade,);
                            ##################insert and get restaurant complementary room id  to add the  multiple Excluded date#####################
                            if ($new_cmpl_room_id = $this->restaurant_model->addResturantComlimentaryRoom($complimentary_room_data)) {
                                $data['message'] = 'Restaurant Complimentary Room Details';
                                $cmpli_excluded_dates = array();
                                if (count($cmpli_exclude_dates) > 0) {
                                    foreach ($cmpli_exclude_dates as $cmpli_exclude_date) {
                                        if ($cmpli_exclude_date['from'] != "" && $cmpli_exclude_date['to'] != "") {
                                            $cmpli_excluded_dates[] = array('cmpl_room_id' => $new_cmpl_room_id,'exclude_date_from' => convert_mysql_format_date($cmpli_exclude_date['from']),'excluded_date_to' => convert_mysql_format_date($cmpli_exclude_date['to']));
                                        }
                                    }
                                    if (!empty($cmpli_excluded_dates) && count($cmpli_excluded_dates) > 0) {
                                        # add complimentary  room excluded dates				
                                        $this->restaurant_model->addResturantComlimentaryRoomExcludedDate($cmpli_excluded_dates);
                                    }
                                }
                            }
                        }
                        ########################################add restaurant complementary room end#######################################	
                        $confrences = $this->input->post('confrence');
                        if (!empty($confrences) && count($confrences) > 0) {
                            $resturantConfrenceData = array();
                            foreach ($confrences as $confrence) {
                                if ($confrence['area'] != "" && $confrence['celling_height'] != "" && $confrence['name'] != "") {
                                    $resturantConfrenceData[] = array('rest_id' => $inserted_rest_id,'confrence_name' => $confrence['name'],'area' => $confrence['area'],'celling_height' => $confrence['celling_height'],'classroom' => $confrence['classroom'],'theatre' => $confrence['theatre'],'banquet' => $confrence['banquet'],'reception' => $confrence['reception'],'conference' => $confrence['conference'],'ushape' => $confrence['ushape'],'hshape' => $confrence['hshape']);
                                }
                            }
                            if (!empty($resturantConfrenceData) && count($resturantConfrenceData) > 0) {
                                # add restaurant confrence details 				
                                $this->restaurant_model->addResturantConfrence($resturantConfrenceData);
                            }
                        }
                        ###########################################add restaurant contract information###################################
                        $contract_start_date = $this->input->post('contract_start_date');
                        $contract_end_date = $this->input->post('contract_end_date');
                        $contract_signed_by = $this->input->post('contract_signed_by');
                        $contract_file_name = '';
                        if ($contract_start_date != "" && $contract_end_date != "") {
                            if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name'])) {
                                $config['upload_path'] = $this->contractFileDir;
                                $config['allowed_types'] = 'pdf|jpg|jpeg';
                                $config['max_size'] = '0'; //2 mb , 0 for unlimited
                                $config['max_width'] = '0'; // unlimited
                                $config['max_height'] = '0';
                                $this->upload->initialize($config);
                                if (!$this->upload->do_upload('contract_file')) {
                                    $error = $this->upload->display_errors();
                                    $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the restaurant details!</li>';
                                } else {
                                    $uploadedFileDetail = $this->upload->data();$contract_file_name = $uploadedFileDetail['file_name'];}
                            }
                            $resturantContractData = array('rest_id' => $inserted_rest_id,'start_date' => convert_mysql_format_date($contract_start_date),'end_date' => convert_mysql_format_date($contract_end_date),'signed_by' => $contract_signed_by,'contract_file' => $contract_file_name);
                            if ($this->restaurant_model->addResturantContract($resturantContractData)) {
                                if ($contract_file_name != "") {
                                    $data['message'] = '<li>Restaurant Contract Details with file ' . $contract_file_name . '</li>';
                                } else {$data['message'] = '<li>Restaurant Contract Details</li>';}
                            }
                        }
                        ###############################add restaurant contract information end########################################
                        
                        ###############################add restaurant cancellation information  start#################################
                        
                        $lowseason_cancelations = $this->input->post('lowseason_canceled'); // multi dimensonal array posted from form
                        $highseason_cancelations = $this->input->post('highseason_canceled'); // multi dimensonal array posted from form
                        $resturantCancellationData = array();
                        if (!empty($lowseason_cancelations) && count($lowseason_cancelations) > 0) {
                            foreach ($lowseason_cancelations as $lowseason_cancel) {
                                if ($lowseason_cancel['before'] != "" && $lowseason_cancel['payment_request'] != "") {
                                    $resturantCancellationData[] = array('rest_id' => $inserted_rest_id,'cancelled_before' => $lowseason_cancel['before'],'payment_request' => $lowseason_cancel['payment_request'],'seasion' => 'low');
                                }
                            }
                        }
                        if (!empty($highseason_cancelations) && count($highseason_cancelations) > 0) {
                            foreach ($highseason_cancelations as $highseason_cancel) {
                                if ($highseason_cancel['before'] != "" && $highseason_cancel['payment_request'] != "") {
                                    $resturantCancellationData[] = array('rest_id' => $inserted_rest_id,'cancelled_before' => $highseason_cancel['before'],'payment_request' => $highseason_cancel['payment_request'],'seasion' => 'high');
                                }
                            }
                        }
                        if (!empty($resturantCancellationData) && count($resturantCancellationData) > 0) {
                            if ($this->restaurant_model->addResturantCancellation($resturantCancellationData)) {
                                $data['message'] = '<li>Restaurant Cancellation Details</li>';
                            }
                        }
                        ######################################add restaurant cancellation information end###################################
                         								
                        ######################################add restaurant payment shedule  option block##################################				
                        $paymentShedules = $this->input->post('payment_plan'); // multi dimensonal array posted from form
                        $resturantPaymentShedulesData = array();
                        if (!empty($paymentShedules) && count($paymentShedules) > 0) {
                            foreach ($paymentShedules as $paymentShedule) {
                                if ($paymentShedule['payment_value'] != "" && $paymentShedule['payment_option_id'] != "") {
                                    $resturantPaymentShedulesData[] = array('rest_id' => $inserted_rest_id,'payment_option_id' => $paymentShedule['payment_option_id'],'payment_value' => $paymentShedule['payment_value'],);
                                }
                            }
                            if (!empty($resturantPaymentShedulesData) && count($resturantPaymentShedulesData) > 0) {
                                if ($this->restaurant_model->addResturantPaymentShedules($resturantPaymentShedulesData)) {
                                    $data['message'] = '<li>Restaurant Payment Details</li>';
                                }
                            }
                        }
                        ####################################add restaurant payment shedule  option end########################################
                         				
                        ####################################add restaurant pricing section####################################################
                        
                        $resturantPricingRecords = $this->input->post('pricing'); // multi dimensonal array posted from form
                        $resturantRoomsPricingData = array(); // for table rest_rooms_pricing
                        if (!empty($resturantPricingRecords) && count($resturantPricingRecords) > 0) {
                            foreach ($resturantPricingRecords as $resturantPricingRecord) {
                                $resturantRroomsPricingDetailsData = array(); // for rest_rooms_pricing_details table
                                $resturantRoomsPricingComplimentaryData = array(); // for table rest_rooms_pricing_complimentary
                                if ($resturantPricingRecord['room_type'] != "" && $resturantPricingRecord['invenory'] != "") {
                                    $resturantRoomsPricingData = array('rest_id' => $inserted_rest_id,'room_type' => $resturantPricingRecord['room_type'],'inclusions' => $resturantPricingRecord['pricing_inclusions'],'curency_code' => $resturantPricingRecord['currency'],'max_adult' => $resturantPricingRecord['max_adult'],'max_child' => $resturantPricingRecord['max_child'],'inventory' => $resturantPricingRecord['invenory'],'period_from' => convert_mysql_format_date($resturantPricingRecord['period_from_date']),'period_to' => convert_mysql_format_date($resturantPricingRecord['period_to_date']),);
                                    $pricingRoomFacilities = $resturantPricingRecord['room_facilities'];
                                    $roomPriceingDetails = $resturantPricingRecord['price_detail'];
                                    if (!empty($resturantRoomsPricingData) && count($resturantRoomsPricingData) > 0) {
                                        $pricing_inserted_id = $this->restaurant_model->addResturantRoomsPricingData($resturantRoomsPricingData); // insert data and get inserted id
                                        if ($pricing_inserted_id) {// if resturant pricing record added in database
                                            // add  room pricing facilities
                                            if (!empty($pricingRoomFacilities) && count($pricingRoomFacilities) > 0) {
                                                foreach ($pricingRoomFacilities as $roomFacility) {
                                                    if ($roomFacility != "") {
                                                        $resturantRoomsPricingComplimentaryData[] = array('pricing_id' => $pricing_inserted_id,'cmpl_service_id' => $roomFacility);
                                                    }
                                                }
                                                if (!empty($resturantRoomsPricingComplimentaryData) && count($resturantRoomsPricingComplimentaryData) > 0) {
                                                    $this->restaurant_model->addResturantRoomPricingComplimentary($resturantRoomsPricingComplimentaryData);
                                                }
                                            }
                                            ##########################room pricing facilities section end.################################
                                            // add  room pricing details
                                            if (!empty($roomPriceingDetails) && count($roomPriceingDetails) > 0) {
                                                foreach ($roomPriceingDetails as $roomPriceingDetail) {
                                                    if ($roomPriceingDetail['market_id'] != "") {
                                                        $resturantRroomsPricingDetailsData[] = array('pricing_id' => $pricing_inserted_id,'market_id' => $roomPriceingDetail['market_id'],'double_price' => $roomPriceingDetail['double_price'],'triple_price' => $roomPriceingDetail['triple_price'],'quad_price' => $roomPriceingDetail['quad_price'],'breakfast_price' => $roomPriceingDetail['breakfast_price'],'half_board_price' => $roomPriceingDetail['half_board_price'],'all_incusive_adult_price' => $roomPriceingDetail['all_incusive'],'extra_adult_price' => $roomPriceingDetail['extra_adult'],'extra_child_price' => $roomPriceingDetail['extra_child'],'extra_bed_price' => $roomPriceingDetail['extra_bed'],);
                                                    }
                                                }
                                                if (!empty($resturantRroomsPricingDetailsData) && count($resturantRroomsPricingDetailsData) > 0) {
                                                    $this->restaurant_model->addRestaurantRroomsPricingDetails($resturantRroomsPricingDetailsData);
                                                    $data['message'] = 'Restaurant Pricing Details ';
                                                }
                                            }
                                            // add  room pricing details end							
                                        }// endif  pricing data added
                                    }
                                }// end of pricing records if exists
                            }// end for each  price records for loop													
                        }
                        
                        #########################################add restaurant pricing section end#####################################
                        $identity = $this->input->post('identity');
                        $dbidentity = $this->restaurant_model->get_dbidentity($identity);
                        if ($dbidentity && $identity == ($dbidentity->identifier)) {
                            $identity_data = array('rest_id' => $inserted_rest_id);
                            $this->restaurant_model->update_images($identity, $identity_data);
                        }
                        if (isset($_FILES['myfile']) && is_uploaded_file($_FILES['myfile']['tmp_name'])) {
                            /*  zip folder code start   */
                            $config['upload_path'] = $this->resturantFileDir;
                            $config['allowed_types'] = 'zip';
                            $config['max_size'] = '0';
                            $this->upload->initialize($config);
                            $this->upload->do_upload('myfile');
                            #Get the uploaded zip file
                            $categories = $this->restaurant_model->gallery_detail();
                            if ($categories && count($categories > 0)) {
                                foreach ($categories as $v) {
                                    $category[$v['id']] = $v['entity_title'];
                                }
                                $zip_array = array('upload_data' => $this->upload->data());
                                //print_r($zip_array); die;
                                $zip_file = $zip_array['upload_data']['full_path'];
                                $pathpart = pathinfo($zip_file);
                                $file_name_with_underscore = $pathpart['filename'];
                                $folder_name = str_replace('_', ' ', $file_name_with_underscore); // folder name contain _ replace it by space
                                $zip = new ZipArchive;
                                #Open the Zip File, and extract it's contents.
                                if ($zip->open($zip_file) === TRUE) {
                                    $zip->extractTo($this->resturantFileDir);
                                    foreach ($category as $key => $v) {
                                        if ($folder_name == $v) {
                                            $images_main = directory_map($this->resturantFileDir . $folder_name);
                                            $counter_main = count($images_main);
                                            if ($counter_main != 1) {
                                                foreach ($images_main as $img_main) {
                                                    $pathpart = pathinfo($img_main);
                                                    $ext = $pathpart['extension'];
                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                                        $dir = $this->resturantFileDir . trim($folder_name) . '/' . $img_main;
                                                        $galimage_data = array('rest_id' => $hid, 'gallery_id' => $key, 'image_name' => $img_main);
                                                        $this->restaurant_model->insert_images($galimage_data);
                                                        rename("$dir", "$this->resturantFileDir$img_main"); // move file to respective directory
                                                    }
                                                }
                                            }
                                        }
                                        $images = directory_map($this->resturantFileDir . $folder_name . '/' . trim($v) . '/');
                                        $counter = count($images);
                                        if ($counter != 1) {
                                            foreach ($images as $img) {
                                                $pathpart = pathinfo($img);
                                                $ext = $pathpart['extension'];
                                                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                                    $dir = $this->resturantFileDir . $folder_name . '/' . trim($v) . '/' . $img;
                                                    $data_image_array = array('rest_id' => $inserted_rest_id, 'gallery_id' => $key, 'image_name' => $img);
                                                    $this->restaurant_model->insert_images($data_image_array);
                                                    rename("$dir", "$this->resturantFileDir$img");
                                                }
                                            }
                                        }
                                    }
                                    $zip->close();
                                    if (file_exists($zip_file)) {
                                        try {
                                            unlink($zip_file);
                                        } catch (Exception $e) {
                                            log_message('gallery zip folder not deleted', $e->getMessage());
                                        }
                                    }
                                    unlink($this->resturantFileDir . $file_name_with_underscore);
                                }
                            }
                        }
                        /*  zip folder code close   */
                        /* clear all post data */
                        //unset($_POST);
                        //unset($_REQUEST);
						$submitcheck = $this->input->post('submit_call');
						 if ($submitcheck != "") {
							$data['message'] = 'Restaurant Details Added Successfully Restaurant Code:- "'.$hcode.'"';
							$flashdata = array('flashdata' => $data['message'],'message_type' => 'sucess');
							$this->session->set_userdata($flashdata);
							redirect('restaurants','refresh');
						}else{
							redirect("restaurants/edit_information/" . $inserted_rest_id);
						}
						
                    } else {
                        $data['message'] = 'Data Not Inserted,Please try again';
                        $flashdata = array('flashdata' => $data['message'],'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
            } else {
                $data['message'] = 'Data Not valid,Please try again';
                $flashdata = array('flashdata' => $data['message'],'message_type' => 'error');
                $this->session->set_userdata($flashdata);
            }
        }
        //get the list of restaurant chain from the form field value helper
        $chain_options = getRestaurantChainsOptions();
        $chain_options['Other'] = 'Other'; // add extra option 
        $propertyTypeOptions = getPropertyTypeOptions('nosel');
        $propertyTypeOptions['Other'] = 'Other';
        $countriesOptions = getCountriesOptions(); /* using helper */
        $pcdoce = $this->input->post('country_code');
        $citiesOptions = getCountyCitiesOptions($pcdoce); /* using helper */
        $roomTypes = getRoomTypes(); /* using helper */
        $roomTypesOptions = array('' => 'Select');
        if ($roomTypes && count($roomTypes) > 0) {
            foreach ($roomTypes as $roomType) {
                $roomTypesOptions[$roomType->id] = $roomType->entity_title;
            }
        }
        //get the list of restaurant position from the form field value helper
        $positions = getPositions();
        $positionOptions = array('' => 'Select');
        if ($positions && count($positions) > 0) {
            foreach ($positions as $position) {
                $positionOptions[$position->id] = $position->entity_title;
            }
        }
        //get the list of restaurant currencies from the form field value helper
        $CurrenciesOptions = getCurrencies(); /* using helper */
        $renovationTypes = getRenovationTypes();
        $renovationTypesOptions = array();
        if ($renovationTypes && count($renovationTypes) > 0) {
            foreach ($renovationTypes as $renovationType) {
                $renovationTypesOptions[$renovationType->id] = $renovationType->renovation_type;
            }
        }
        //get the list of age group for child from the form field value helper
        $ageGroupsOption = getChildAgeOptions(); /* using helper */
        $cancelationPeriods = getCancelationPeriods();
        $cancelationPeriodsOption = array();
        if ($cancelationPeriods && count($cancelationPeriods) > 0) {
            foreach ($cancelationPeriods as $cancelationPeriod) {
                $cancelationPeriodsOption[$cancelationPeriod->id] = $cancelationPeriod->entity_title;
            }
        }
        //get the payment list from the form field value helper
        $applicablePaymentOptions = getPaymentOptions();
        $paymentOptions = array();
        if ($applicablePaymentOptions && count($applicablePaymentOptions) > 0) {
            foreach ($applicablePaymentOptions as $applicablePaymentOption) {
                $paymentOptions[$applicablePaymentOption->id] = $applicablePaymentOption->entity_title;
            }
        }
        //get the list of availabe complementary from the form field value helper
        $roomCmplServices = getAvailabeComplementaryServices();
        $roomCmplServicesOptions = array();
        if ($roomCmplServices && count($roomCmplServices) > 0) {
            foreach ($roomCmplServices as $cmplService) {
                $roomCmplServicesOptions[$cmplService->id] = $cmplService->entity_title;
            }
        }
        //get the list of markets from the form field value helper
        $marketsList = getMarkets();
        $marketOptions = array();
        if ($marketsList && count($marketsList) > 0) {
            foreach ($marketsList as $market) {
                $marketOptions[$market->id] = $market->entity_title;
            }
        }
        $scity = $this->input->post('city') != '' ? $this->input->post('city') : '';
        if ($scity != "") {
            $districtOptions = getCityDistrictOptions($scity, 'sel');
        } else {
            $districtOptions = array('' => 'Select');
        }
          /**************************generate form field for restaurant entry form ***************************************/
            $this->data = array('rest_name' => array('id' => 'rest_name','class' => 'required','tabindex' => 1,'data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "10-100",'data-validation-help' => "Should  be minimum  10  characters ",'name' => 'rest_name','maxlength' => '100','size' => '100','value' => set_value('rest_name')),
            'star_rating_options' => getStarRatings(),
            'dorak_rating_options' => getDorakRatings(),
            'status_options' => getRestaurantStatus(),
            'position_options' => $positionOptions,
            'currency_options' => $CurrenciesOptions,
            'commisioned_options' => array('' => 'Select','1' => 'Yes','0' => 'No'),
            'purpose_options' => getPurposeOptions(),
            'purpose_options_oth' => array('id' => 'purpose_options_oth','class' => 'purpose_options_oth','placeholder' => 'Specify other','data-validation' => 'required,length','data-validation-length' => "4-50",'name' => 'purpose_options_oth','value' => set_value('purpose_options_oth')),
            
            'chain_options' => $chain_options,
            'hotel_chain_oth' => array('id' => 'hotel_chain_oth','class' => 'hotel_chain_oth','maxlength' => "50",'placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",                'data-validation-length' => "3-50",'name' => 'hotel_chain_oth','value' => set_value('hotel_chain_oth')),
            
            'property_type_options' => $propertyTypeOptions,
            'property_type_oth' => array('id' => 'property_type_oth','class' => 'property_type_oth','placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "4-50",'name' => 'property_type_oth','value' => set_value('property_type_oth')),'country_id_options' => $countriesOptions,'city_options' => $citiesOptions,'rest_address' => array('id' => 'rest_address','tabindex' => 14,'data-validation' => 'required,alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "15-150",'data-validation-help' => "Should  be minimum  15  characters ",'name' => 'rest_address','maxlength' => '150','size' => '150','value' => set_value('rest_address')),
            'districtOptions' => $districtOptions,
            
            'post_code' => array('id' => 'post_code','class' => 'required','tabindex' => 18,'data-validation' => 'required,length,alphanumeric','data-validation-length' => "4-8",'name' => 'post_code','maxlength' => '10','size' => '10','value' => set_value('post_code')),
            
            'distance_from_city' => array('name' => 'distance_from_city[1][distance]','data-validation' => 'distance','tabindex' => 20,'data-validation-optional' => 'true','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted ",'placeholder' => 'Distance','value' => set_value('distance_from_city[1][distance]')),
            
            'distance_from_city_name' => array('name' => 'distance_from_city[1][name]','class' => 'g-autofill','tabindex' => 19,'onfocus' => "searchLocation(this)",'id' => "distanc-city",'data-validation' => 'alphanumeric','data-validation-allowing' => "-_@#:,./\() ",'data-validation-optional' => 'true','placeholder' => 'Enter City / Locality Name','value' => set_value('distance_from_city[1][name]')),
            
            'distance_from_airport' => array('name' => 'distance_from_airport[1][distance]','data-validation' => 'distance','tabindex' => 22,'data-validation-optional' => 'true','placeholder' => 'Distance','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted ",'value' => set_value('distance_from_airport[1][distance]')),
            
            'distance_from_airport_name' => array('name' => 'distance_from_airport[1][name]','tabindex' => 21,'data-validation' => 'alphanumeric','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-allowing' => "-_@#:,./\() ",'onfocus' => "searchLocation(this)",'class' => 'g-autofill','data-validation-optional' => 'true','placeholder' => 'Enter Airport Name','value' => set_value('distance_from_airport[1][name]')),
            
            'contact_name' => array('name' => 'contact[1][name]','id' => 'contact_1_name','tabindex' => 10,'class' => 'required','data-validation' => 'custom,required,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "min3",'placeholder' => 'Name','value' => set_value('contact[1][name]')),
            
            'contact_phone' => array('name' => 'contact[1][phone]','tabindex' => 12,'id' => 'contact_1_phone','data-validation' => 'required,phone_number',
                //'data-validation-length'=>"5-20",
                'data-validation-length' => "min10",'maxlength' => '20','placeholder' => 'Mobile','class' => 'required','value' => set_value('contact[1][phone]')),
            
            'contact_email' => array('name' => 'contact[1][email]','tabindex' => 11,'id' => 'contact_1_email','data-validation' => 'required,email','class' => 'required','placeholder' => 'Email','value' => set_value('contact[1][email]')),
            'contact_extension' => array('tabindex' => 13,'name' => 'contact[1][extension]','data-validation' => 'length,number','data-validation-optional' => 'true','data-validation-length' => "2-6",'maxlength' => '6','size' => '6','class' => 'required','placeholder' => 'Ext.','value' => set_value('contact[1][extension]')),
            
            /*********************************form fields for  bank information**************************************/
            'account_number' => array('class' => 'required','data-validation' => 'number,length','data-validation-length' => "5-16",'data-validation-help' => "Minimum  5 digit",'data-validation-optional' => 'true','name' => 'bank[1][account_number]','maxlength' => '16','size' => '20','value' => set_value('bank[1][account_number]')),'account_name' => array('data-validation' => 'custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][account_name]','maxlength' => '100','size' => '100','value' => set_value('bank[1][account_name]')),'bank_name' => array('data-validation' => 'length','data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][bank_name]','maxlength' => '100','size' => '100','value' => set_value('bank[1][bank_name]')),'bank_address' => array('data-validation' => 'alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "10-150",'data-validation-optional' => 'true','name' => 'bank[1][bank_address]','data-validation-help' => "Minimum  length 10  characters ",'maxlength' => '100','size' => '100','value' => set_value('bank[1][bank_address]')),'branch_name' => array('data-validation' => 'length','data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][branch_name]','maxlength' => '100',               'size' => '100','value' => set_value('bank[1][branch_name]')),'bank_ifsc_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",                'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][bank_ifsc_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][bank_ifsc_code]')),
            'iban_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][iban_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][iban_code]')),
            'branch_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][branch_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][branch_code]')),
            'swift_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][swift_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][swift_code]')),
            'roomTypesOptions' => $roomTypesOptions,
            'upgradeOptions' => array('0' => 'No','1' => 'Yes',),
            'ageGroupsOption' => $ageGroupsOption,
            'cmpli_room_night' => array('data-validation' => 'number','data-validation-optional' => 'true','name' => 'cmpli_room_night','value' => set_value('cmpli_room_night')),
            'confrenceUshapeOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'confrenceHsquareOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'cancellation_options' => $cancelationPeriodsOption,
            'payment_options' => $paymentOptions,
            'rnv_shedule_area_effected' => array('name' => 'rnv_shedule[1][area_effected]','id' => 'rnv_shedule[1][area_effected]','value' => set_value('rnv_shedule[1][area_effected]'),'rows' => '1','cols' => '10',),
            'renovation_options' => $renovationTypesOptions,
            'room_cmpl_options' => $roomCmplServicesOptions,
            'pricing_inclusions' => array('name' => 'pricing[1][pricing_inclusions]','id' => 'pricing[1][pricing_inclusions]','value' => set_value('pricing[1][pricing_inclusions]'),'rows' => '5','cols' => '10',),
            'markets_options' => $marketOptions,
        );
            $defMarket = $this->config->item('default_market');
            $this->data['posted_position'] = $this->input->post('position');
            $this->data['posted_star_rating'] = $this->input->post('star_rating');
            $this->data['posted_dorak_rating'] = $this->input->post('dorak_rating');
            $this->data['posted_chain_id'] = $this->input->post('chain_id');
            $this->data['posted_property_type'] = $this->input->post('property_type');
            $this->data['posted_purpose'] = $this->input->post('purpose');
            $this->data['posted_commisioned'] = $this->input->post('commisioned') != '' ? $this->input->post('commisioned') : '';
            $this->data['posted_currency'] = $this->input->post('currency') != '' ? $this->input->post('currency') : 'USD';
            $this->data['posted_status'] = $this->input->post('status') != '' ? $this->input->post('status') : 'Enabled';
            $dc = $this->config->item('default_country_code');
            $defCcode = array($dc);
            $this->data['posted_country_code'] = $this->input->post('country_code') != '' ? $this->input->post('country_code') : $defCcode;
            $this->data['posted_city'] = $this->input->post('city') != '' ? $this->input->post('city') : '';
            $this->data['posted_district'] = $this->input->post('district') != '' ? $this->input->post('district') : '';
            $this->data['posted_room_type'] = $this->input->post('room_type') != '' ? $this->input->post('room_type') : '';
            $this->data['posted_cmpli_room_type'] = $this->input->post('cmpli_room_type') != '' ? $this->input->post('cmpli_room_type') : '';
            $this->data['posted_cmpli_upgrade'] = $this->input->post('cmpli_upgrade') != '' ? $this->input->post('cmpli_upgrade') : '0';
            $this->data['posted_cmpli_room_night'] = $this->input->post('cmpli_room_night') != '' ? $this->input->post('cmpli_room_night') : '';
            $this->data['confrence_conference'] = $this->input->post('confrence_conference');
            $this->data['confrence_reception'] = $this->input->post('confrence_reception');
            $this->data['confrence_banquet'] = $this->input->post('confrence_banquet');
            $this->data['confrence_theatre'] = $this->input->post('confrence_theatre');
            $this->data['confrence_ushape'] = $this->input->post('confrence_ushape') != '' ? $this->input->post('confrence_ushape') : '';
            $this->data['confrence_hsquare'] = $this->input->post('confrence_hsquare') != '' ? $this->input->post('confrence_hsquare') : '';
            $this->data['posted_renovation_type'] = $this->input->post('rnv_shedule[1][renovation_type]') != '' ? $this->input->post('rnv_shedule[1][renovation_type]') : '';
            $this->data['posted_market_id'] = $this->input->post('pricing[1][price_detail][1][market_id]') != '' ? $this->input->post('pricing[1][price_detail][1][market_id]') : $defMarket;
            $this->data['galleries'] = $this->restaurant_model->get_gallary();
            $this->data['identity'] = $this->identityfier;
            $this->load->view('informations_view', $this->data);
    }

    /**
     * @method string upload().
     * @todo upload  gallery image using ajax.  
     * @return success if file successfully uploaded.  
     */
    function upload() {
        if (!empty($_FILES)) {
            $gallary_id = $this->input->post('galeryid');
            $gallerysessid = $this->input->post('gallerysessid');
            $resturantids = $this->input->post('resturantid');
            if (!empty($resturantids)) {
                $resturantid = $resturantids;
            } else {
                $resturantid = null;
            }
            $ds = DIRECTORY_SEPARATOR;
            foreach ($_FILES["file"]["error"] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $tempFile = $_FILES['file']['tmp_name'][$key];
                    $name = $_FILES["file"]["name"][$key];
                    $file_extension = end((explode(".", $name))); # extra () to prevent notice
                    $targetPath = FCPATH . $this->resturantFileDir . $ds;  //4
                    $targetFile = $targetPath . $name;
                    move_uploaded_file($tempFile, $targetFile);
                    $data_galery_det = array('rest_id' => $resturantid, 'gallery_id' => $gallary_id, 'image_name' => $name, 'identifier' => $gallerysessid);
                    $status = $this->restaurant_model->insert_images($data_galery_det);
                }
            }
        }
    }

    /**
     * @method string deleteimg().
     * @todo delete  gallery image using ajax.  
     * @return NULL
     */
    function deleteimg() {
        $imgname = $this->input->post('id');
        $galid = $this->input->post('galid');
        $ds = DIRECTORY_SEPARATOR;
        $targetPath = FCPATH . $this->resturantFileDir . $ds;  //4
        $targetFile = $targetPath . $imgname;
        try {
            unlink($targetFile);
        } catch (Exception $e) {
            log_message('gallery image deletion ', $e->getMessage());
        }
        $this->restaurant_model->delete_images($imgname, $galid);
    }

    /**
     * @method string edit_information()
     * @todo Update restaurant information(all tabs related data) based on perticular restaurant id. 
     * @param $hid(int) restaurant id - it is the primary key of an restaurant.
     * @return success it all data successfully updated else false.
     */
    public function edit_information($hid = null) {
//        ob_start();
//        
//         dump($_POST);die;

        $data = array('message' => '');
        // Check acess the user access	
        if (!checkAccess($this->accessLabelId, 'restaurants', 'edit')) {
            $flashdata = array('flashdata' => $this->accessDenidMessage,'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
        if ($hid == "") {
            $data['message'] = 'Invalid  Access!';
            $flashdata = array('flashdata' => $data['message'],'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect("restaurants"); /* redirect to restaurant listing page */
        }
        $this->load->library('upload');
        $this->output->set_meta('description', 'Restaurant Information');
        $this->output->set_meta('title', 'Restaurant Information');
        $this->output->set_title('Restaurant Information');
        $user = $this->ion_auth->user()->row();
        if ($this->session->userdata('stepsess') == '#tab-5') {
            $this->session->set_userdata('stepsess', '#tab-6');
        }

        if (!empty($_POST)) {
		 //echo ($_POST['submit_call']); die;
			//echo $this->input->get_post('submit_call'); die;
           //dump($_POST);die;
            $identity = $this->input->post('identity');
            $step = $this->input->post('step'); /* input for save data on tab basis */
            $this->session->set_userdata('stepsess', $step); /* session start for tabs */
            # validation rules 
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('rest_address', 'Address', 'required|min_length[15]|max_length[150]');
            $this->form_validation->set_rules('post_code', 'Zip Code', 'required|alpha_numeric|min_length[4]|max_length[8]');
            $this->form_validation->set_rules('currency', 'Currency', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('property_type_oth', 'property_type_oth', 'min_length[3]');
            $this->form_validation->set_rules('purpose_options_oth', 'purpose_options_oth', 'min_length[3]');
            $this->form_validation->set_rules('hotel_chain_oth', 'hotel_chain_oth', 'min_length[5]');
            if ($this->form_validation->run($this) != FALSE) {
                $contactData = array();
                $nearestDistanceData = array();  /* we'll store each distance row in this array */
                $resturantBankData = array();
                $purpose = $this->input->post('purpose');
                if ($purpose != '' && $purpose == 'Other') {
                    # insert new purpose
                    $purpose_options_oth = $this->input->post('purpose_options_oth');
                    if ($purpose_options_oth != '') {
                        # check if already in database if not then add
                        if ($pid = $this->restaurant_model->getPurpose($purpose_options_oth)) {
                            $purposeId = $pid;
                        } else {
                            # add data in database 
                            $resturant_purpose = array('title' => $purpose_options_oth);
                            $purposeId = $this->restaurant_model->addPurpose($resturant_purpose);
                        }
                    }
                } else {
                    $purposeId = $purpose;
                }
                # add new restaurant chain if  user choose  other options the specified  value need to store int  restaurant chain list table
                $resturantchain = $this->input->post('chain_id');
                if ($resturantchain != '' && $resturantchain == 'Other') {
                    # insert new purpose
                    $hotel_chain_oth = $this->input->post('hotel_chain_oth');
                    if ($hotel_chain_oth != '') {
                        /* check if already in database if not then add */
                        if ($hcid = $this->restaurant_model->getRestaurantChain($hotel_chain_oth)) {
                            $chainId = $hcid;
                        } else {
                            /* add data in database  */
                            $resturant_chain = array('entity_title' => $hotel_chain_oth, 'entity_type' => $this->config->item('attribute_resturant_chain'));
                            $chainId = $this->restaurant_model->addRestaurantChain($resturant_chain);
                        }
                    }
                } else {
                    $chainId = $resturantchain;
                }
                $resturant_detail = array('rest_address' => $this->input->post('rest_address'),'post_code' => $this->input->post('post_code'),'star_rating' => $this->input->post('star_rating'),'dorak_rating' => $this->input->post('dorak_rating'),'chain_id' => $chainId,'purpose' => $purposeId,'currency' => $this->input->post('currency'),'district' => $this->input->post('district'),'status' => $this->input->post('status'),);
                $this->restaurant_model->updateRestaurantInformation($hid, $resturant_detail);

                /* update contacts details  of the restaurants */
                $nonDelContactids = array();
                $resturantContactDetails = $this->input->post('contact');
                if (count($resturantContactDetails) > 0) {
                    foreach ($resturantContactDetails as $resturantContact) {
                        if (isset($resturantContact['id']))
                            $hcontactid = $resturantContact['id'];
                        else
                            $hcontactid = '';
                        if ($hcontactid != "")
                            array_push($nonDelContactids, $hcontactid);
                        $contactData = array('rest_id' => $hid,'position' => $resturantContact['position'],'name' => $resturantContact['name'],'email' => $resturantContact['email'],'phone' => $resturantContact['phone'],'extension' => $resturantContact['extension'],);

                        $updatecontactid = $this->restaurant_model->updateResturantContact($contactData, $hcontactid);
                        array_push($nonDelContactids, $updatecontactid);
                    }
                    $this->restaurant_model->deleteResturantContact($nonDelContactids, $hid);
                }
                # add restaurant complementary  services
                $cmplServices = $this->input->post('complimentary');
                if (empty($cmplServices)) {
                    $this->restaurant_model->deleteroomFacility($hid);
                }
                if ($cmplServices != "" && count($cmplServices) > 0) {
                    $resturantCmplServices = array();

                    foreach ($cmplServices as $cmplServiceId) {
                        if (isset($cmplServiceId) && $cmplServiceId != "") {
                            $resturantCmplServices[] = array('rest_id' => $hid,'cmpl_service_id' => $cmplServiceId);
                        }
                    }
                    if ($this->restaurant_model->deleteroomFacility($hid)) {
                        $this->restaurant_model->addResturantComplimentaryServices($resturantCmplServices);
                    }
                }
                # add restaurant Facilities
                $facilities = $this->input->post('facilities');
                if (empty($facilities)) {
                    $this->restaurant_model->deleteresturantFacility($hid);
                }
                if ($facilities != "" && count($facilities) > 0) {
                    $resturantFacilities = array();
                    foreach ($facilities as $facility_id) {

                        if (isset($facility_id) && $facility_id != "") {
                            $resturantFacilities[] = array('rest_id' => $hid,'facility_id' => $facility_id);
                        }
                    }
                    if ($this->restaurant_model->deleteresturantFacility($hid)) {
                        $this->restaurant_model->addResturantFacilities($resturantFacilities);
                    }
                }
                $nonbnkIds = array();
                $bnk_details = $this->input->post('bank');
                if (!empty($bnk_details) && count($bnk_details) > 0) {
                    foreach ($bnk_details as $bnk) {
                        if (isset($bnk['id']))
                            $bankid = $bnk['id'];
                        else
                            $bankid = '';
                        //if($bankid!="")			
                        //array_push($nonbnkIds,$bankid);
                        if ($bnk['account_number'] != "" && $bnk['account_name'] != "" && $bnk['bank_name'] != "" && $bnk['bank_address'] != "") {
                            $resturantBankData = array('rest_id' => $hid,'iban_code' => $bnk['iban_code'],'account_number' => $bnk['account_number'],'account_name' => $bnk['account_name'],'bank_name' => $bnk['bank_name'],'bank_address' => $bnk['bank_address'],'branch_name' => $bnk['branch_name'],'branch_code' => $bnk['branch_code'],'swift_code' => $bnk['swift_code'],'bank_ifsc_code' => $bnk['bank_ifsc_code']);
                            $bankNid = $this->restaurant_model->updateResturantbankaccounts($resturantBankData, $bankid);
                            array_push($nonbnkIds, $bankNid);
                        } else {
                            $this->restaurant_model->deleteResturantbankaccount($bankid, $hid); // delete empty  account details
                        }
                    }
                    // delete non existing data
                    $this->restaurant_model->deleteResturantbankaccounts($nonbnkIds, $hid);
                } else {
                    $this->restaurant_model->deleteResturantbankaccounts(false, $hid);
                }
                /* distance by city name */
                $noncityIds = array();
                $distance_from_city_name = $this->input->post('distance_from_city');
                if (!empty($distance_from_city_name)) {
                    foreach ($distance_from_city_name as $cityname) {
                        if (isset($cityname['id']))
                            $cityid = $cityname['id'];
                        else
                            $cityid = '';
                        if ($cityid != "")
                            array_push($noncityIds, $cityid);
                        if (isset($cityname['name']) && $cityname['name'] != "" && isset($cityname['distance']) && $cityname['distance'] != "") {
                            $citynamedata = array('rest_id' => $hid,'distance_from' => $cityname['name'],'distance_type' => '1','distance' => $cityname['distance']);
                            $cityupdateid = $this->restaurant_model->updateResturantcity($citynamedata, $cityid);
                            array_push($noncityIds, $cityupdateid);
                        }
                    }
                    $this->restaurant_model->deleteResturantcity($noncityIds, $hid);
                }

                /* distance by city name close here */

                /* distance by airport name */
                $nonairIds = array();
                $distance_from_airport_name = $this->input->post('distance_from_airport');
                if (!empty($distance_from_airport_name)) {
                    foreach ($distance_from_airport_name as $airname) {
                        if (isset($airname['id']))
                            $airid = $airname['id'];
                        else
                            $airid = '';
                        if ($airid != "")
                            array_push($nonairIds, $airid);
                        if (isset($airname['name']) && $airname['name'] != "" && isset($airname['distance']) && $airname['distance'] != "") {
                            $airnamedata = array('rest_id' => $hid,'distance_from' => $airname['name'],'distance_type' => '2','distance' => $airname['distance']);
                            $airupdateid = $this->restaurant_model->updatedistanceForAirport($airnamedata, $airid);
                            array_push($nonairIds, $airupdateid);
                        }
                    }
                    $this->restaurant_model->deletedistanceForAirport($nonairIds, $hid);
                }
                /* distance by airport name close here */
                /* add new restaurant property type  if  user choose  other options */
                $resturantPtype = $this->input->post('property_type');
                if (empty($resturantPtype)) {
                    $this->restaurant_model->deleteResturantpropertytype($hid);
                }
                if ($resturantPtype != '' && count($resturantPtype) > 0) {
                    $addResturantPropertyTypesData = array();
                    foreach ($resturantPtype as $HpType) {
                        if ($HpType != '') {
                            if ($HpType == 'Other') {
                                /* insert new purpose */
                                $property_type_oth = $this->input->post('property_type_oth');
                                if ($property_type_oth != '') {
                                    /* check if already in database if not then add */
                                    if ($hPid = $this->restaurant_model->getRestaurantPropertyType($property_type_oth)) {
                                        $resturantPtypeId = $hPid;
                                    } else {
                                        /* add data in database */
                                        $resturantPtype = array('entity_title' => $property_type_oth, 'entity_type' => $this->config->item('attribute_property_types'));
                                        $resturantPtypeId = $this->restaurant_model->addRestaurantPropertyType($resturantPtype);
                                    }
                                }
                            } else {
                                $resturantPtypeId = $HpType;
                            }
                            $addResturantPropertyTypesData[] = array('rest_id' => $hid,'property_type_id' => $resturantPtypeId);
                        }
                    }
                    if (count($addResturantPropertyTypesData) > 0) {
                        if ($this->restaurant_model->deleteResturantpropertytype($hid)) {
                            $this->restaurant_model->addResturantPropertyTypes($addResturantPropertyTypesData);
                        }
                    }
                }
                /* add property close here */
                # add restaurant renovation shedule	
                $nonDelRnvIds = array();
                $renovationSchedules = $this->input->post('rnv_shedule');
                if (count($renovationSchedules) > 0) {
                    foreach ($renovationSchedules as $renovationSchedule) {
                        if (isset($renovationSchedule['id']))
                            $scheduleId = $renovationSchedule['id'];
                        else
                            $scheduleId = '';
                        if ($scheduleId != "")
                            array_push($nonDelRnvIds, $scheduleId);
                        if ($renovationSchedule['date_from'] != "" && $renovationSchedule['date_to'] != "" && $renovationSchedule['renovation_type'] != "" && $renovationSchedule['area_effected'] != "") {
                            $resturantRenovationData = array('rest_id' => $hid,'date_from' => convert_mysql_format_date($renovationSchedule['date_from']),'date_to' => convert_mysql_format_date($renovationSchedule['date_to']),'renovation_type' => $renovationSchedule['renovation_type'],'area_effected' => $renovationSchedule['area_effected']);
                            $idu = $this->restaurant_model->updateResturantRenovationSchedule($resturantRenovationData, $scheduleId);
                            array_push($nonDelRnvIds, $idu);
                        }
                    }
                    // delete non existing data
                    $this->restaurant_model->deleteResturantRenovationSchedule($nonDelRnvIds, $hid);
                }
                # add restaurant complementary room
                $cmpli_room_night = $this->input->post('cmpli_room_night');
                $cmpli_room_id = $this->input->post('cmpli_room_id');
                $cmpli_date_from = convert_mysql_format_date($this->input->post('cmpli_date_from'));
                $cmpli_date_to = convert_mysql_format_date($this->input->post('cmpli_date_to'));
                $cmpli_upgrade = $this->input->post('cmpli_upgrade');
                $cmpli_exclude_dates = $this->input->post('cmpli_exclude_date');
                if ($cmpli_room_night != "" && $cmpli_date_from != "") {
                    $complimentary_room_data = array('rest_id' => $hid,'room_night' => $cmpli_room_night,'start_date' => $cmpli_date_from,'end_date' => $cmpli_date_to,'upgrade' => $cmpli_upgrade,
                    );

                    #update   details
                    if ($cmpli_room_id = $this->restaurant_model->updateResturantComlimentaryRoom($complimentary_room_data, $hid, $cmpli_room_id)) {
                        $nonDelCmpExIds = array();
                        if (count($cmpli_exclude_dates) > 0) {
                            foreach ($cmpli_exclude_dates as $cmpli_exclude_date) {
                                if (isset($cmpli_exclude_date['id']))
                                    $cmExId = $cmpli_exclude_date['id'];
                                else
                                    $cmExId = '';
                                if ($cmExId != "")
                                    array_push($nonDelCmpExIds, $cmExId);
                                if ($cmpli_exclude_date['from'] != "" && $cmpli_exclude_date['to'] != "") {
                                    $cmpli_excluded_dates = array('cmpl_room_id' => $cmpli_room_id,'exclude_date_from' => convert_mysql_format_date($cmpli_exclude_date['from']),'excluded_date_to' => convert_mysql_format_date($cmpli_exclude_date['to'])
                                    );
                                    $ncmExId = $this->restaurant_model->updateResturantComlimentaryRoomExcludedDate($cmpli_excluded_dates, $cmExId);
                                    // function return new added id so we do not have to deltete that one
                                    array_push($nonDelCmpExIds, $ncmExId);
                                }
                            }
                            $this->restaurant_model->deleteResturantComlimentaryRoomExcludedDate($nonDelCmpExIds, $cmpli_room_id);
                        }
                    }
                }

                # update restaurant payment shedule  option				
                $paymentShedules = $this->input->post('payment_plan'); // multi dimensonal array posted from form
                if (!empty($paymentShedules) && count($paymentShedules) > 0) {
                    $nonDelPaymentIds = array();
                    foreach ($paymentShedules as $paymentShedule) {
                        if (isset($paymentShedule['id']))
                            $pmExId = $paymentShedule['id'];
                        else
                            $pmExId = '';
                        if ($pmExId != "")
                            array_push($nonDelPaymentIds, $pmExId);
                        if ($paymentShedule['payment_value'] != "" && $paymentShedule['payment_option_id'] != "") {
                            $resturantPaymentShedulesData = array('rest_id' => $hid,'payment_option_id' => $paymentShedule['payment_option_id'],'payment_value' => $paymentShedule['payment_value'],
                            );
                            $npmExId = $this->restaurant_model->updateResturantPaymentShedules($resturantPaymentShedulesData, $pmExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelPaymentIds, $npmExId);
                        }
                    }
                    $this->restaurant_model->deleteResturantPaymentShedules($nonDelPaymentIds, $hid);
                }
                # add restaurant payment shedule  option end 
                #add restaurant cancellation information  
                $lowseason_cancelations = $this->input->post('lowseason_canceled'); // multi dimensonal array posted from form
                $highseason_cancelations = $this->input->post('highseason_canceled'); // multi dimensonal array posted from form
                $nonDelCancellationIds = array();
                if (!empty($lowseason_cancelations) && count($lowseason_cancelations) > 0) {
                    foreach ($lowseason_cancelations as $lowseason_cancel) {
                        if (isset($lowseason_cancel['id']))
                            $cancellationExId = $lowseason_cancel['id'];
                        else
                            $cancellationExId = '';
                        if ($cancellationExId != "")
                            array_push($nonDelCancellationIds, $cancellationExId);
                        if ($lowseason_cancel['before'] != "" && $lowseason_cancel['payment_request'] != "") {
                            $resturantCancellationData = array('rest_id' => $hid,'cancelled_before' => $lowseason_cancel['before'],'payment_request' => $lowseason_cancel['payment_request'],'seasion' => 'low'
                            );
                            $nCanclExId = $this->restaurant_model->updateResturantCancellation($resturantCancellationData, $cancellationExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelCancellationIds, $nCanclExId);
                        }
                    }
                }
                if (!empty($highseason_cancelations) && count($highseason_cancelations) > 0) {
                    foreach ($highseason_cancelations as $highseason_cancel) {
                        if (isset($highseason_cancel['id']))
                            $cancellationExId = $highseason_cancel['id'];
                        else
                            $cancellationExId = '';
                        if ($cancellationExId != "")
                            array_push($nonDelCancellationIds, $cancellationExId);

                        if ($highseason_cancel['before'] != "" && $highseason_cancel['payment_request'] != "") {
                            $resturantCancellationData = array('rest_id' => $hid,'cancelled_before' => $highseason_cancel['before'],'payment_request' => $highseason_cancel['payment_request'],'seasion' => 'high'
                            );
                            $nCanclExId = $this->restaurant_model->updateResturantCancellation($resturantCancellationData, $cancellationExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelCancellationIds, $nCanclExId);
                        }
                    }
                }
                $this->restaurant_model->deleteResturantCancellation($nonDelCancellationIds, $hid);
                #add restaurant cancellation information end 
                $aplicable_child_age_group = $this->input->post('aplicable_child_age_group');
                if ($aplicable_child_age_group != "") {
                    $uchData = array('child_age_group_id' => $aplicable_child_age_group);
                    $this->restaurant_model->updateRestaurantDetails($hid, $uchData);
                }
                #add restaurant contract information
                $contract_start_date = $this->input->post('contract_start_date');
                $contract_end_date = $this->input->post('contract_end_date');
                $contract_signed_by = $this->input->post('contract_signed_by');
                $contract_file_name = '';
				
                if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name']) && $contract_end_date == "" && $contract_signed_by == "") {
					//dump($_FILES); die;
                    $data['message'] = '<li> Restaurant Contract Details Not Added,Restaurant Contract Details Fields are missing! </li>';
                } elseif ($contract_start_date != "" && $contract_end_date != "" && $contract_signed_by != "") {
					
                   if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name'])) { 
						
                        $config['upload_path'] = $this->contractFileDir;
                        $config['allowed_types'] = 'pdf|jpg|jpeg';
                        $config['max_size'] = '0'; //2 mb , 0 for unlimited
                        $config['max_width'] = '0'; // unlimited
                        $config['max_height'] = '0';
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('contract_file')) {
                            $error = $this->upload->display_errors();
                            $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the restaurant details!</li>';
                        } else {
                            $uploadedFileDetail = $this->upload->data();
                            $contract_file_name = $uploadedFileDetail['file_name'];
                        }
                    }
                    $resturantContractData = array('rest_id' => $hid,'start_date' => convert_mysql_format_date($contract_start_date),'end_date' => convert_mysql_format_date($contract_end_date),'signed_by' => $contract_signed_by,'contract_file' => $contract_file_name
                    );
                    if ($this->restaurant_model->addResturantContract($resturantContractData)) {
                        if ($contract_file_name != "") {
                            $data['message'] = '<li>Restaurant Contract Details Added</li>';
                        } else {
                            $data['message'] = '<li>Restaurant Contract Details</li> ';
                        }
                    }
                }
                
                 #add menu market information
                $menu_marketDetails = array();
                $menu_marketDetails = $this->input->post('menu_market');
                $menu_file_name = '';
//                dump($menu_marketDetails);die;
                if (count($menu_marketDetails) > 0) {
                    foreach ($menu_marketDetails as $key => $menu_marketDetail) {
                        
                            if (isset($_FILES['market_files']['name']) && is_uploaded_file($_FILES['market_files']['tmp_name'])) {
//                                dump($_FILES['market_files']['name']);die;
                                $config['upload_path'] = $this->marketFileDir;
                                $config['allowed_types'] = 'pdf|jpg|jpeg';
                                $config['max_size'] = '0'; //2 mb , 0 for unlimited
                                $config['max_width'] = '0'; // unlimited
                                $config['max_height'] = '0';
                                $this->upload->initialize($config);
//                                dump($this->upload->do_upload('market_files'));die;
                                if (!$this->upload->do_upload('market_files')) {
                                    $error = $this->upload->display_errors();
                                    $data['message'] = '<li>Notice.Market files not uploaded because ' . $error . ' , please upload it again, by editing the restaurant details!</li>';
                                } else {
                                    $uploadedFileDetail = $this->upload->data();
                                    $menu_file_name = $uploadedFileDetail['file_name'];
                                }
                            }
//                            echo "jeevan";die;
                            $menuMarketData = array('market_id' => $menu_marketDetail['market_name'],'rest_id' => $hid, 'files' => $menu_file_name);
                            if ($this->restaurant_model->addMarketFiles($menuMarketData)) {
                                if ($menu_file_name != "") {
                                    $data['message'] = '<li>Restaurant Market Details Added</li>';
                                } else {
                                    $data['message'] = '<li>Restaurant Market Details</li> ';
                                }
                            }
                    }
//                    die;
                }


                # edit restaurant pricing section					
                $resturantPricingRecords = $this->input->post('pricing'); // multi dimensonal array posted from form
                $resturantRoomsPricingData = array(); // for table rest_rooms_pricing
                if (!empty($resturantPricingRecords) && count($resturantPricingRecords) > 0) {
                    $nonDelPricingRecordIds = array();
                    foreach ($resturantPricingRecords as $resturantPricingRecord) {
                        //echo $resturantPricingRecord['pricing_id']."<br>";
                        // prepare  records  which will not deleted other record whicg are not submitted will be deleted
                        if (isset($resturantPricingRecord['pricing_id']))
                            $prExId = $resturantPricingRecord['pricing_id'];
                        else
                            $prExId = '';

                        $resturantRroomsPricingDetailsData = array(); // for rest_rooms_pricing_details table
                        $resturantRoomsPricingComplimentaryData = array(); // for table rest_rooms_pricing_complimentary

                        if (isset($resturantPricingRecord['room_type']) && $resturantPricingRecord['room_type'] != "" && isset($resturantPricingRecord['invenory']) && $resturantPricingRecord['invenory'] != "") {
                            $resturantRoomsPricingData = array('rest_id' => $hid,'room_type' => $resturantPricingRecord['room_type'],'inclusions' => $resturantPricingRecord['pricing_inclusions'],'curency_code' => $resturantPricingRecord['currency'],'max_adult' => $resturantPricingRecord['max_adult'],'max_child' => $resturantPricingRecord['max_child'],'inventory' => $resturantPricingRecord['invenory'],'period_from' => convert_mysql_format_date($resturantPricingRecord['period_from_date']),'period_to' => convert_mysql_format_date($resturantPricingRecord['period_to_date']),
                            );
                            $pricing_inserted_id = $this->restaurant_model->updateResturantRoomsPricingData($resturantRoomsPricingData, $prExId);
                            if ($pricing_inserted_id) {
                                array_push($nonDelPricingRecordIds, $pricing_inserted_id); // add id to non delete list			
                                // if restaurant pricing record added in database // add  room pricing facilities
                                if (isset($resturantPricingRecord['room_facilities'])) {
                                    $pricingRoomFacilities = $resturantPricingRecord['room_facilities'];
                                    if (!empty($pricingRoomFacilities) && count($pricingRoomFacilities) > 0) {
                                        foreach ($pricingRoomFacilities as $roomFacility) {
                                            if ($roomFacility != "") {
                                                $resturantRoomsPricingComplimentaryData[] = array('pricing_id' => $pricing_inserted_id,'cmpl_service_id' => $roomFacility
                                                );
                                            }
                                        }
                                        if (!empty($resturantRoomsPricingComplimentaryData) && count($resturantRoomsPricingComplimentaryData) > 0) {
                                            $this->restaurant_model->addResturantRoomPricingComplimentary($resturantRoomsPricingComplimentaryData, $pricing_inserted_id);
                                        }
                                    }
                                }// add  room pricing facilities end
                                // add  room pricing details
                                if (isset($resturantPricingRecord['price_detail'])) {
                                    $roomPriceingDetails = $resturantPricingRecord['price_detail'];
                                    if ($pricing_inserted_id == '')
                                        $pricing_inserted_id = $prExId;
                                    if (!empty($roomPriceingDetails) && count($roomPriceingDetails) > 0) {
                                        $nonDelPricingDetailRecordIds = array();
                                        // store  ids which need to keep after update
                                        foreach ($roomPriceingDetails as $roomPriceingDetail) {
                                            if (isset($roomPriceingDetail['pricing_detId']))
                                                $prDetExId = $roomPriceingDetail['pricing_detId'];
                                            else
                                                $prDetExId = '';
                                            if ($prDetExId != "")
                                                array_push($nonDelPricingDetailRecordIds, $prDetExId);
                                            if ($roomPriceingDetail['market_id'] != "") {
                                                $resturantRroomsPricingDetailsData = array('pricing_id' => $pricing_inserted_id,'market_id' => $roomPriceingDetail['market_id'],'double_price' => $roomPriceingDetail['double_price'],'triple_price' => $roomPriceingDetail['triple_price'],'quad_price' => $roomPriceingDetail['quad_price'],'breakfast_price' => $roomPriceingDetail['breakfast_price'],'half_board_price' => $roomPriceingDetail['half_board_price'],'all_incusive_adult_price' => $roomPriceingDetail['all_incusive'],'extra_adult_price' => $roomPriceingDetail['extra_adult'],'extra_child_price' => $roomPriceingDetail['extra_child'],'extra_bed_price' => $roomPriceingDetail['extra_bed'],
                                                );
                                                $npricingDetId = $this->restaurant_model->updateRestaurantRroomsPricingDetails($resturantRroomsPricingDetailsData, $prDetExId, $pricing_inserted_id);
                                                array_push($nonDelPricingDetailRecordIds, $npricingDetId);
                                            }
                                        }
                                        // delete non  existing  pricing details
                                        if (count($nonDelPricingDetailRecordIds) > 0)
                                            $this->restaurant_model->deleteResturantRestaurantRroomsPricingDetails($nonDelPricingDetailRecordIds, $pricing_inserted_id);
                                    }// end of pricing records if exists	
                                }
                            }
                            // add  room pricing details end
                        }// end if of if   pricing data added							
                        // delete other not existing procing  of the restaurant
                    }// end of pricing records for	

                    if (count($nonDelPricingRecordIds) > 0)
                        $this->restaurant_model->deleteResturantRoomsPricingData($nonDelPricingRecordIds, $hid);
                }// end of pricing records if exists	
                # add restaurant pricing section end
                $identity = $this->input->post('identity');
                $dbidentity = $this->restaurant_model->get_dbidentity($identity);
                if ($dbidentity && $identity == ($dbidentity->identifier)) {
                    $data_identity = array('rest_id' => $hid);
                    $this->restaurant_model->update_images($identity, $data_identity);
                }
                if (isset($_FILES['myfile']) && is_uploaded_file($_FILES['myfile']['tmp_name'])) {
				$this->uploadGaleryImage($hid);
				}
                /*  zip folder code close   */
				 $data['rest_code']= $this->restaurant_model->getRestaurantbyid($hid);
				 $resturantcode = $data['rest_code']->rest_code;
			if(isset($_POST['submit_call'])=='save'){
                $data['message'] = '<li>Restaurant information updated for  Restaurant Code:-"'.$resturantcode.'"</li>';
                $flashdata = array(
                    'flashdata' => $data['message'],'message_type' => 'sucess'
                );
                $this->session->set_userdata($flashdata);
				
					redirect('restaurants','refresh');
				}
            } else {
                $data['message'] = 'Data Not valid,Please try again';
                $flashdata = array('flashdata' => $data['message'],'message_type' => 'error'
                );
                $this->session->set_userdata($flashdata);
            }
        }
        $chain_options = getRestaurantChainsOptions();
        $chain_options['Other'] = 'Other'; // add extra option 
        $propertyTypeOptions = getPropertyTypeOptions('nosel');
        $propertyTypeOptions['Other'] = 'Other';
        $countriesOptions = getCountriesOptions();  /* using helper to get available Countries Options for select dropdown */
//        dump($countriesOptions);die;
        $pcdoce = $this->input->post('country_code');
        $citiesOptions = getCountyCitiesOptions($pcdoce); /* using helper */
        $roomTypes = getRoomTypes(); /* using helper to get available Room Types for select dropdown */
        $roomTypesOptions = array('' => 'Select');
        if ($roomTypes && count($roomTypes) > 0) {
            foreach ($roomTypes as $roomType) {
                $roomTypesOptions[$roomType->id] = $roomType->entity_title;
            }
        }
        $positions = getPositions();
        $positionOptions = array('' => 'Select');
        if ($positions && count($positions) > 0) {
            foreach ($positions as $position) {
                $positionOptions[$position->id] = $position->entity_title;
            }
        }
        $CurrenciesOptions = getCurrencies(); /* using helper to get available Currencies for select dropdown */
        $renovationTypes = getRenovationTypes(); /* using helper to get available Renovation Types for select dropdown */
        $renovationTypesOptions = array();
        if ($renovationTypes && count($renovationTypes) > 0) {
            foreach ($renovationTypes as $renovationType) {
                $renovationTypesOptions[$renovationType->id] = $renovationType->renovation_type;
            }
        }
        $ageGroupsOption = getChildAgeOptions(); /* using helper */
        $cancelationPeriods = getCancelationPeriods();
        $cancelationPeriodsOption = array();
        if ($cancelationPeriods && count($cancelationPeriods) > 0) {
            foreach ($cancelationPeriods as $cancelationPeriod) {
                $cancelationPeriodsOption[$cancelationPeriod->id] = $cancelationPeriod->entity_title;
            }
        }
        $applicablePaymentOptions = getPaymentOptions();
        $paymentOptions = array();
        if ($applicablePaymentOptions && count($applicablePaymentOptions) > 0) {
            foreach ($applicablePaymentOptions as $applicablePaymentOption) {
                $paymentOptions[$applicablePaymentOption->id] = $applicablePaymentOption->entity_title;
            }
        }
        $roomCmplServices = getAvailabeComplementaryServices();
        $roomCmplServicesOptions = array();
        if ($roomCmplServices && count($roomCmplServices) > 0) {
            foreach ($roomCmplServices as $cmplService) {
                $roomCmplServicesOptions[$cmplService->id] = $cmplService->entity_title;
            }
        }
        $marketsList = getMarkets();
        $marketOptions = array('' => 'Select');
        if ($marketsList && count($marketsList) > 0) {
            foreach ($marketsList as $market) {
                $marketOptions[$market->id] = $market->entity_title;
            }
        }
        $scity = $this->input->post('city') != '' ? $this->input->post('city') : '';
        if ($scity != "") {
            $districtOptions = getCityDistrictOptions($scity, 'sel');
        } else {
            $districtOptions = array('' => 'Select');
        }
        $resturantContactInfo = $this->restaurant_model->getResturantContactInfo($hid);
        $resturantContractInfo = $this->restaurant_model->getResturantContract($hid);
//        dump($resturantContractInfo);
        $hotalComlimentaryRoom = $this->restaurant_model->getResturantComlimentaryRoom($hid);
        $hotalComlimentaryRoomExcludedDate = $this->restaurant_model->getResturantComlimentaryRoomExcludedDateByRestaurant($hid);
        $hotalRenovationSchedule = $this->restaurant_model->getResturantRenovationSchedule($hid);
        $hotalPaymentPlans = $this->restaurant_model->getResturantPaymentShedules($hid);
        $hotalCancellation = $this->restaurant_model->getResturantCancellation($hid);
        $lowseason = 0;
        $high_session = 0;
        if(!empty($hotalCancellation)){
        foreach($hotalCancellation as $resturant_session){
            if($resturant_session->seasion == 'low'){
                $lowseason++;
            }elseif($resturant_session->seasion == 'high'){
                $high_session++;
            }
        }
        }
//        echo $high_session;die;
        $cmpli_room_night = ($hotalComlimentaryRoom) ? $hotalComlimentaryRoom->room_night : '';
        /* generate form field for restaurant entry form */
        $this->data = array('rest_name' => array('id' => 'rest_name2','readonly' => 'readonly','tabindex' => 1,'name' => 'rest_name2','maxlength' => '100','size' => '100'),
            'status_options' => getRestaurantStatus(),
            'position_options' => $positionOptions,
            'currency_options' => $CurrenciesOptions,
            'commisioned_options' => array('' => 'Select','1' => 'Yes','0' => 'No'),
            'purpose_options' => getPurposeOptions(),
            'purpose_options_oth' => array('id' => 'purpose_options_oth','class' => 'purpose_options_oth','placeholder' => 'Specify other','data-validation' => 'required,length','data-validation-length' => "4-50",'name' => 'purpose_options_oth'),
            
            'chain_options' => $chain_options,
            'hotel_chain_oth' => array('id' => 'hotel_chain_oth','class' => 'hotel_chain_oth','maxlength' => "50",'placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-50",'name' => 'hotel_chain_oth'),'property_type_options' => $propertyTypeOptions,'property_type_oth' => array('id' => 'property_type_oth','class' => 'property_type_oth','placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "4-50",'name' => 'property_type_oth'),
            
            'star_rating_options' => getStarRatings(),
            'dorak_rating_options' => getDorakRatings(),
            'country_id_options' => $countriesOptions,
            'rest_address' => array('id' => 'rest_address','tabindex' => 14,'data-validation' => 'required,alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "15-150",'data-validation-help' => "Should  be minimum  15  characters ",'name' => 'rest_address','maxlength' => '150','size' => '150'),
            
            'districtOptions' => $districtOptions,
            'post_code' => array('id' => 'post_code','class' => 'required','tabindex' => 18,'data-validation' => 'required,length,alphanumeric','data-validation-length' => "4-8",'name' => 'post_code','maxlength' => '10','size' => '10'),
            'distance_from_city' => array('name' => 'distance_from_city[1][distance]','data-validation' => 'distance','tabindex' => 20,'data-validation-optional' => 'true','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted ",'placeholder' => 'Distance'),
            'distance_from_city_name' => array('name' => 'distance_from_city[1][name]','class' => 'g-autofill','tabindex' => 19,'onfocus' => "searchLocation(this)",'id' => "distanc-city",'data-validation' => 'alphanumeric','data-validation-allowing' => "-_@#:,./\() ",'data-validation-optional' => 'true','placeholder' => 'Enter City / Locality Name'),
            'distance_from_airport' => array('name' => 'distance_from_airport[1][distance]','data-validation' => 'distance','tabindex' => 22,'data-validation-optional' => 'true','placeholder' => 'Distance','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted "),
            'distance_from_airport_name' => array('name' => 'distance_from_airport[1][name]','tabindex' => 21,'data-validation' => 'alphanumeric','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-allowing' => "-_@#:,./\() ",'onfocus' => "searchLocation(this)",'class' => 'g-autofill','data-validation-optional' => 'true','placeholder' => 'Enter Airport Name'),
            'contact_name' => array('name' => 'contact[1][name]','id' => 'contact_1_name','tabindex' => 10,'class' => 'required','data-validation' => 'custom,required,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "min3",'placeholder' => 'Name',
            ),
            'contact_phone' => array('name' => 'contact[1][phone]','tabindex' => 12,'id' => 'contact_1_phone','data-validation' => 'required,phone_number,length','data-validation-length' => "min10",'maxlength' => '20','placeholder' => 'Mobile','class' => 'required',),
            'contact_email' => array('name' => 'contact[1][email]','tabindex' => 11,'id' => 'contact_1_email','data-validation' => 'required,email','class' => 'required','placeholder' => 'Email'),
            'contact_extension' => array('tabindex' => 13,'name' => 'contact[1][extension]','data-validation' => 'length,number','data-validation-optional' => 'true','data-validation-length' => "2-6",'maxlength' => '6','size' => '6','class' => 'required','placeholder' => 'Ext.'),
            'account_number' => array('class' => 'required','data-validation' => 'number,length','data-validation-length' => "5-16",'data-validation-help' => "Minimum  5 digit",'data-validation-optional' => 'true','name' => 'bank[1][account_number]','maxlength' => '16','size' => '20'),
            'account_name' => array('data-validation' => 'custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][account_name]','maxlength' => '100','size' => '100',),
            'bank_name' => array('data-validation' => 'length','data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][bank_name]','maxlength' => '100','size' => '100',),
            'bank_address' => array('data-validation' => 'alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "10-150",'data-validation-optional' => 'true','name' => 'bank[1][bank_address]','data-validation-help' => "Minimum  length 10  characters ",'maxlength' => '100','size' => '100'),
            'branch_name' => array('data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters",'data-validation-optional' => 'true','name' => 'bank[1][branch_name]','maxlength' => '100','size' => '100'),
            'bank_ifsc_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][bank_ifsc_code]','maxlength' => '30','size' => '30'),
            'iban_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][iban_code]','maxlength' => '30','size' => '30'),
            'branch_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][branch_code]','maxlength' => '30','size' => '30'),
            'swift_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][swift_code]','maxlength' => '30','size' => '30'),
            'roomTypesOptions' => $roomTypesOptions,
            'upgradeOptions' => array('0' => 'No', '1' => 'Yes'),
            'ageGroupsOption' => $ageGroupsOption,
            'cmpli_room_night' => array('data-validation' => 'number','data-validation-optional' => 'true','name' => 'cmpli_room_night','value' => $cmpli_room_night),
            'confrenceUshapeOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'confrenceHsquareOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'cancellation_options' => $cancelationPeriodsOption,
            'payment_options' => $paymentOptions,
            'rnv_shedule_area_effected' => array('name' => 'rnv_shedule[1][area_effected]','id' => 'rnv_shedule[1][area_effected]','value' => set_value('rnv_shedule[1][area_effected]'),'rows' => '1','cols' => '10'),
            'renovation_options' => $renovationTypesOptions,
            'room_cmpl_options' => $roomCmplServicesOptions,
            'pricing_inclusions' => array('name' => 'pricing[1][pricing_inclusions]','id' => 'pricing[1][pricing_inclusions]','value' => set_value('pricing[1][pricing_inclusions]'),'rows' => '5','cols' => '10'),
            'markets_options' => $marketOptions);
        $this->data['galleries'] = $this->restaurant_model->get_gallary();
        $this->data['hotalCancellation'] = $hotalCancellation;$this->data['lowsession_count'] = $lowseason;$this->data['highsession_count'] = $high_session;$this->data['hotalPaymentPlans'] = $hotalPaymentPlans;$this->data['comlimentaryRoom'] = $hotalComlimentaryRoom;$this->data['comlimentaryRoomExcludedDate'] = $hotalComlimentaryRoomExcludedDate;$this->data['hotalRenovationSchedule'] = $hotalRenovationSchedule;$this->data['resturantPricing'] = $this->restaurant_model->getResturantRoomsPricingData($hid);
        $this->data['resturantContactInfo'] = $resturantContactInfo;$this->data['resturantContractInfo'] = $resturantContractInfo;
        $this->data['identity'] = $this->identityfier;$this->data['resturantinfo'] = $this->restaurant_model->getRestaurantbyid($hid);
        $this->data['propertyids'] = $this->restaurant_model->getpropertyid($hid);$this->data['contactinfo'] = $this->restaurant_model->getcontactinfobyid($hid);
        $this->data['citydis'] = $this->restaurant_model->getdistanceForCity($hid);$this->data['airportdis'] = $this->restaurant_model->getdistanceForAirport($hid);
        $this->data['resturantfacilities'] = $this->restaurant_model->resturantFacility($hid);$this->data['roomfacility'] = $this->restaurant_model->roomFacility($hid);
        $this->data['resturantbankaccounts'] = $this->restaurant_model->resturantBankAccount($hid);$this->data['galimages'] = $this->restaurant_model->get_gallaryimg($hid);
		$this->data['commonfiles'] = $this->restaurant_model->get_common_menu_files($hid);
        $this->data['resturantid'] = $hid;
        $cityi = $this->data['resturantinfo']->city;
        $districtOptions = getCityDistrictOptions($cityi, 'sel');
        $this->data['districtOptions'] = $districtOptions;
        $this->load->view('edit_information_view', $this->data);
    }
    /**
     * @method string deleteresturantimg()
     * @todo delete restaurant gallery image via ajax.
     * @return : it will return deleted id from the database.
     */
    function deleteresturantimg() {
        $imgid = $this->input->post('imgid');
        $resturantgalid = $this->input->post('galid');
        $resturantid = $this->input->post('hid');
        $imageid = $this->restaurant_model->deleteresturantgalleryimg($imgid, $resturantgalid, $resturantid);
        echo $imageid;
    }
    
    /**
     * @method string uploadGaleryImage()
     * @param $hid(int) : - it is the primary key of restaurant table.
     * @todo upload gallery images based on restaurants.
     * @return : it will return true if data successfully deleted else false.
     */
	function uploadGaleryImage($hid)
	{
		 if (isset($_FILES['myfile']) && is_uploaded_file($_FILES['myfile']['tmp_name'])) {
                    /*  zip folder code start   */
                    $config['upload_path'] = $this->resturantFileDir;
                    $config['allowed_types'] = 'zip';
                    $config['max_size'] = '0';
                    $this->upload->initialize($config);
                    $this->upload->do_upload('myfile');
                    #Get the uploaded zip file
                    $categories = $this->restaurant_model->gallery_detail();
                    if ($categories && count($categories > 0)) {
                        foreach ($categories as $v) {
                            $category[$v['id']] = $v['entity_title'];
                        }
                        $zip_array = array('upload_data' => $this->upload->data());
                        $zip_file = $zip_array['upload_data']['full_path'];
                        $pathpart = pathinfo($zip_file);
                        $file_name_with_underscore = $pathpart['filename'];
                        $folder_name = str_replace('_', ' ', $file_name_with_underscore);
                        $zip = new ZipArchive;
                        #Open the Zip File, and extract it's contents.
                        if ($zip->open($zip_file) === TRUE) {
                            $zip->extractTo($this->resturantFileDir);
                            foreach ($category as $key => $v) {
                                if ($folder_name == $v) {
                                    $images_main = directory_map($this->resturantFileDir . $folder_name);
                                    $counter_main = count($images_main);
                                    if ($counter_main != 1) {
                                        foreach ($images_main as $img_main) {
                                            $pathpart = pathinfo($img_main);
                                            $ext = $pathpart['extension'];
                                            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                                $dir = $this->resturantFileDir . trim($folder_name) . '/' . $img_main;
                                                $data_gallery_images = array('rest_id' => $hid, 'gallery_id' => $key, 'image_name' => $img_main);
                                                $this->restaurant_model->insert_images($data_gallery_images);
                                                rename("$dir", "$this->resturantFileDir$img_main");
                                            }
                                        }
                                    }
                                }
                                $images = directory_map($this->resturantFileDir . $folder_name . '/' . trim($v) . '/');
                                $counter = count($images);
                                if ($counter != 1) {
                                    foreach ($images as $img) {
                                        $pathpart = pathinfo($img);
                                        $ext = $pathpart['extension'];
                                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                            $dir = $this->resturantFileDir . $folder_name . '/' . trim($v) . '/' . $img;
                                            $data_galery_image = array('rest_id' => $hid, 'gallery_id' => $key, 'image_name' => $img);
                                            $this->restaurant_model->insert_images($data_galery_image);
                                            rename("$dir", "$this->resturantFileDir$img");
                                        }
                                    }
                                }
                            }
                            $zip->close();
                            if (file_exists($zip_file)) {
                                unlink($zip_file);
                            }
                        }
                    }
                }
				return true;
	}
}