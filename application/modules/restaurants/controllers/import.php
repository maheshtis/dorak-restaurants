<?php

/*
 * @data 15-mar-2016
 * @author tis
 * @functions:
  importPriceing():import restaurant room details and pricing for markets, on the basis of restaurant code
  process():import restaurant all tabs information together or individually as  added in excel file
  restaurantPurposeId():use to add if purpose not exists in database and return id / get purpose id  if already in database

  restaurantChainId():use to add if chain not exists in database and return id / get id  if already in database
  contactPositionId():use to add if contact Position not exists in database and return id / get id  if already in database

  getCmplServiceId():use to add if Cmplimentary Service not exists in database and return id / get id  if already in database

  getFacilityId():use to add if Facility not exists in database and return id / get id  if already in database
  getAgegroupId():use to add if Age group not exists in database and return id / get id  if already in database
  getRoomTypeId():use to add if room type not exists in database and return id / get id  if already in database
  getPaymentOpionId():use to add if payment opion not exists in database and return id / get id  if already in database

  getRenovationTypeId():use to add if payment opion not exists in database and return id / get id  if already in database

  getCancelBeforeOpionId(): use to add if cancel before opion not exists in database and return id / get id  if already in database

  getMarketId(): use to add if Market not exists in database and return id / get id  if already in database
  getPropertyType() :use to add if property type not exists in database and return id / get id  if already in database

  chkCountryCity() : use to add if country city not exists in database and return id / get id  if already in database

  chkCityDistrict() : use to add if country district not exists in database and return district / get district if already in database

  chkCountryCode() :use to add if country code not exists in database and return code / get code  if already in database

  generateHotelcode():  use to generate restaurant code
  export_restaurant() : used for export restaurants into excel file using php excel library
  writeHoteldata() :Write data into excel file in the provided array

 * @description This model  is for  add/ edit delete restaurants setting related stuff.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import extends AdminController {

    function __construct() {
        parent::__construct();

        $this->_init();
    }

    private function _init() {
        if (!$this->ion_auth->logged_in()) {
            // redirect none logged user the login page
            redirect('auth/login', 'refresh');
        }
        if (!checkAccess($this->accessLabelId, 'restaurants', 'add')) {
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }

        $this->load->helper(array('form', 'html', 'import'));
        $this->load->library('form_validation');
        $this->output->set_meta('description', 'Restaurant information');
        $this->output->set_meta('title', 'Restaurant information');
        $this->output->set_title('Restaurant informations');
        $this->tmpFileDir = $this->config->item('upload_temp_dir');
        $this->load->model('restaurants/import_model');
    }

    public function index() {
        $this->data = array(
            'importAction' => site_url('restaurants/import/process'),
        );
        $this->load->view('import', $this->data);
    }

    /* Import pricing */

    /**
     * @method string importPriceing()
     * @todo import restaurant room details and pricing for markets, on the basis of restaurant code	
     */
    public function importPriceing() {
        $data = array('message' => '');
        $user = $this->ion_auth->user()->row();
        if ($this->form_validation->run($this) != FALSE) {
            $sdata['message'] = 'Please select excel file to import';
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('/restaurants');
        } else {
            $arr_data = array();
            // config upload
            $config['upload_path'] = $this->tmpFileDir;
            $config['allowed_types'] = 'xlsx|csv|xls|xls';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('restaurants_pricingfile')) {
                $sdata['message'] = $this->upload->display_errors();
                $flashdata = array(
                    'flashdata' => $sdata['message'],
                    'message_type' => 'notice'
                );
                $this->session->set_userdata($flashdata);
                redirect('/restaurants');
            } else {
                $upload_data = $this->upload->data();
                $restaurants_pricingfile = $upload_data['full_path'];
                chmod($restaurants_pricingfile, 0777);
                $this->load->library('excel');
                //read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($restaurants_pricingfile);
                try {
                    $fileType = PHPExcel_IOFactory::identify($restaurants_pricingfile);
                    $objReader = PHPExcel_IOFactory::createReader($fileType);
                    $objPHPExcel = $objReader->load($restaurants_pricingfile);
                    $sheets = [];
                    foreach ($objPHPExcel->getAllSheets() as $sheet) {
                        $sheets[$sheet->getTitle()] = $sheet->toArray();
                    }
                } catch (Exception $e) {

                    log_message('importPriceing : ', $e->getMessage());
                }
                $pricingRecordsArray = array();
                $header = array();
                $srh = array("  ", " ", "_(", ")", "(");
                $repl = array("_", "_", "_", "", "_");
                $objWorksheet = $objPHPExcel->getActiveSheet();
                // format  each row  with column as key of array 
                foreach ($objWorksheet->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                    //    even if a cell value is not set.
                    // By default, only cells that have a value 
                    //    set will be iterated.
                    foreach ($cellIterator as $cell) {
                        $rowNumber = $cell->getRow();
                        $cell_value = $cell->getValue();
                        $column = $cell->getColumn();
                        if ($rowNumber == 1) {
                            $header[$rowNumber][$column] = str_replace($srh, $repl, strtolower(trim($cell_value)));
                        } else if ($rowNumber > 1) {
                            $pricingRecordsArray[$rowNumber][$header[1][$column]] = $cell_value;
                        }
                    }
                }
                if (count($pricingRecordsArray) > 0) {
                    foreach ($pricingRecordsArray as $pricingRec) {
                        $restaurantInfoArray = array(); // array to  insert restaurant  information
                        if (isset($pricingRec['rest_code']) && $pricingRec['rest_code'] != "") {
                            // check if restaurant details exists
                            $restaurantId = $this->import_model->getRestaurantDetailsBycode($pricingRec['rest_code']);
                            if ($restaurantId) {
                                $hid = $restaurantId;
                                // restaurant pricing section	
                                if (isset($pricingRec['room_type']) && $pricingRec['room_type'] != '' && isset($pricingRec['currency']) && $pricingRec['currency'] != '' && isset($pricingRec['inventory']) && $pricingRec['inventory'] != '') {
                                    $room_type = $this->getRoomTypeId($pricingRec['room_type']);
                                    $max_adult = '';
                                    $complimentary = '';
                                    $max_child = '';
                                    $period_from = '';
                                    $period_to = '';
                                    $complimentary = '';
                                    $inclusions = '';
                                    if (isset($pricingRec['max_adult']) && $pricingRec['max_adult'] != "") {
                                        $max_adult = $pricingRec['max_adult'];
                                    }
                                    if (isset($pricingRec['complimentary']) && $pricingRec['complimentary'] != "") {
                                        $complimentary = $pricingRec['complimentary'];
                                    }
                                    if (isset($pricingRec['max_child']) && $pricingRec['max_child'] != "") {
                                        $max_child = $pricingRec['max_child'];
                                    }
                                    if (isset($pricingRec['inclusions']) && $pricingRec['inclusions'] != "") {
                                        $inclusions = $pricingRec['inclusions'];
                                    }
                                    if (isset($pricingRec['period_from']) && $pricingRec['period_from'] != "") {
                                        $period_from = formatDateToMysql($pricingRec['period_from']);
                                    }
                                    if (isset($pricingRec['period_to']) && $pricingRec['period_to'] != "") {
                                        $period_to = formatDateToMysql($pricingRec['period_to']);
                                    }
                                    // check  if record already   exists  for  room type and  period 
                                    // then update the dada
                                    $restaurant_pricing_id = $this->import_model->getRestaurantRoomsPricingData($hid, $room_type, $pricingRec['currency'], $period_from, $period_to);
                                    if (!$restaurant_pricing_id) {
                                        // if not exists  then add
                                        $restaurantRoomTypesDetailsData = array(
                                            'rest_id' => $hid,
                                            'room_type' => $room_type,
                                            'inclusions' => $inclusions,
                                            'curency_code' => $pricingRec['currency'],
                                            'max_adult' => $max_adult,
                                            'max_child' => $max_child,
                                            'inventory' => $pricingRec['inventory'],
                                            'period_from' => $period_from,
                                            'period_to' => $period_to,
                                        );
                                        // add  data in database table  and get  id
                                        $pricing_id = $this->import_model->importRestaurantRoomsPricingData($restaurantRoomTypesDetailsData);
                                    } else {
                                        $pricing_id = $restaurant_pricing_id;
                                        // edit  pricing details
                                        $restaurantRoomTypesDetailsData = array(
                                            'inclusions' => $inclusions,
                                            'max_adult' => $max_adult,
                                            'max_child' => $max_child,
                                            'inventory' => $pricingRec['inventory'],
                                        );
                                        $this->import_model->editRestaurantRoomsPricingData($hid, $pricing_id, $restaurantRoomTypesDetailsData);
                                    }
                                    if ($pricing_id) {
                                        $data['message'] = 'Room Type Detail information imported. <br>';
                                    }
                                    $roomComplementaryArray = explode(',', $complimentary); //coma seprated
                                    if (count($roomComplementaryArray) > 0) {
                                        $restaurantRoomsPricingComplimentaryData = array();
                                        foreach ($roomComplementaryArray as $roomComplementary) {
                                            if (trim($roomComplementary) != "") {
                                                $rcmpl_service_id = $this->getCmplServiceId(trim($roomComplementary));
                                                $restaurantRoomsPricingComplimentaryData[] = array(
                                                    'pricing_id' => $pricing_id,
                                                    'cmpl_service_id' => $rcmpl_service_id
                                                );
                                            }
                                        }
                                        if (!empty($restaurantRoomsPricingComplimentaryData) && count($restaurantRoomsPricingComplimentaryData) > 0) {
                                            $this->import_model->deleteRoomPricingComplimentary($pricing_id);
                                            $this->import_model->addRestaurantRoomPricingComplimentary($restaurantRoomsPricingComplimentaryData);
                                        }
                                    }
                                }
                                if ($pricing_id && $pricing_id > 0 && isset($pricingRec['market']) && $pricingRec['market'] != "") {
                                    $market_id = $this->getMarketId($pricingRec['market']);
                                    $double_price = '';
                                    $triple_price = '';
                                    $quad_price = '';
                                    $breakfast_price = '';
                                    $half_board_price = '';
                                    $price_all_inclusive = '';
                                    $extra_adult_price = '';
                                    $extra_child_price = '';
                                    $extra_bed_price = '';
                                    if (isset($pricingRec['double_price']) && $pricingRec['double_price'] != "") {
                                        $double_price = $pricingRec['double_price'];
                                    }
                                    if (isset($pricingRec['triple_price']) && $pricingRec['triple_price'] != "") {
                                        $triple_price = $pricingRec['triple_price'];
                                    }
                                    if (isset($pricingRec['quad_price']) && $pricingRec['quad_price'] != "") {
                                        $quad_price = $pricingRec['quad_price'];
                                    }
                                    if (isset($pricingRec['breakfast_price']) && $pricingRec['breakfast_price'] != "") {
                                        $breakfast_price = $pricingRec['breakfast_price'];
                                    }
                                    if (isset($pricingRec['half_board_price']) && $pricingRec['half_board_price'] != "") {
                                        $half_board_price = $pricingRec['half_board_price'];
                                    }
                                    if (isset($pricingRec['price_all_inclusive']) && $pricingRec['price_all_inclusive'] != "") {
                                        $price_all_inclusive = $pricingRec['price_all_inclusive'];
                                    }
                                    if (isset($pricingRec['extra_adult_price']) && $pricingRec['extra_adult_price'] != "") {
                                        $extra_adult_price = $pricingRec['extra_adult_price'];
                                    }
                                    if (isset($pricingRec['extra_child_price']) && $pricingRec['extra_child_price'] != "") {
                                        $extra_child_price = $pricingRec['extra_child_price'];
                                    }
                                    if (isset($pricingRec['extra_bed_price']) && $pricingRec['extra_bed_price'] != "") {
                                        $extra_bed_price = $pricingRec['extra_bed_price'];
                                    }
                                    $restaurantRoomTypesRateData = array(
                                        'pricing_id' => $pricing_id,
                                        'market_id' => $market_id,
                                        'double_price' => $double_price,
                                        'triple_price' => $triple_price,
                                        'quad_price' => $quad_price,
                                        'breakfast_price' => $breakfast_price,
                                        'half_board_price' => $half_board_price,
                                        'all_incusive_adult_price' => $price_all_inclusive,
                                        'extra_adult_price' => $extra_adult_price,
                                        'extra_child_price' => $extra_child_price,
                                        'extra_bed_price' => $extra_bed_price,
                                    );
                                    $this->import_model->updateHotelRroomsPricingDetails($restaurantRoomTypesRateData, $pricing_id, $market_id);
                                }
                                // restaurant pricing section end
                                $sdata['message'] = 'Restaurant pricing imported';
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'sucess'
                                );
                                $this->session->set_userdata($flashdata);
                            } else {
                                // restaurant not exists;
                                $sdata['message'] = $pricingRec['rest_code'] . ' Not Exists.';
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'error'
                                );
                                $this->session->set_userdata($flashdata);
                            }
                        }
                    }
                }
            }
        }

        if (file_exists($restaurants_pricingfile)) {
            try {
                unlink($restaurants_pricingfile);
            } catch (Exception $e) {
                log_message('pricing file delete: ', $e->getMessage());
            }
        }
        redirect('/restaurants');
    }

    ## Import pricing  end ####	
    ## import restaurant details ####		
    /**
     * @method string process()
     * @todo import restaurant all tabs information together or individually as  added in excel file
     */

    public function process() {
        $data = array('message' => '');
        $user = $this->ion_auth->user()->row();
        if ($this->form_validation->run($this) != FALSE) {
            $sdata['message'] = 'Please select excel file to import';
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('/restaurants');
        } else {
            $arr_data = array();
            // config upload
            $config['upload_path'] = $this->tmpFileDir;
            $config['allowed_types'] = 'xlsx|csv|xls|xls';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('restaurants_file')) {
                $sdata['message'] = $this->upload->display_errors();
                $flashdata = array(
                    'flashdata' => $sdata['message'],
                    'message_type' => 'notice'
                );
                $this->session->set_userdata($flashdata);
                redirect('/restaurants');
            } else {
                $upload_data = $this->upload->data();
                $restaurants_file = $upload_data['full_path'];
                try {
                    chmod($restaurants_file, 0777);
                } catch (Exception $e) {
                    log_message('file permession: ', $e->getMessage());
                }
                $this->load->library('excel');
                //read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($restaurants_file);
                try {
                    $fileType = PHPExcel_IOFactory::identify($restaurants_file);
                    $objReader = PHPExcel_IOFactory::createReader($fileType);
                    $objPHPExcel = $objReader->load($restaurants_file);
                    $sheets = [];
                    foreach ($objPHPExcel->getAllSheets() as $sheet) {
                        $sheets[$sheet->getTitle()] = $sheet->toArray();
                    }
                } catch (Exception $e) {
                    die($e->getMessage());
                }
                $arrangedRecordsArray = array();
                $header = array();
                $srh = array("  ", " ", "_(", ")", "(");
                $repl = array("_", "_", "_", "", "_");
                $objWorksheet = $objPHPExcel->getActiveSheet();
                // format  each row  with column as key of array 
                foreach ($objWorksheet->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                    //    even if a cell value is not set.
                    // By default, only cells that have a value 
                    //    set will be iterated.
                    foreach ($cellIterator as $cell) {
                        $rowNumber = $cell->getRow();
                        $cell_value = $cell->getValue();
                        $column = $cell->getColumn();
                        if ($rowNumber == 2) {
                            $header[$rowNumber][$column] = str_replace($srh, $repl, strtolower(trim($cell_value)));
                        } else if ($rowNumber > 2) {
                            $arrangedRecordsArray[$rowNumber][$header[2][$column]] = $cell_value;
                        }
                    }
                }
                $contactData = array();
                $nearestDistanceData = array();
                $restaurantFacilityData = array();
                $roomFacilityComplimentaryData = array();
                $bankingDetailsData = array();
                $contractInfoData = array();
                $contractComplimentryRoomData = array();
                $contractComplimentryRoomExData = array();
                $contractRenovationData = array();
                $contractPaymentPlanData = array();
                $contractCancellationInfoData = array();
                $restaurantRoomTypesDetailsData = array();
                $restaurantRoomTypesRateData = array();
                $cmpl_room_id = false;
                $restaurant_room_pricing_id = false;
                $hid = false;
                $exhid = false;
                $restaurantCode = false;
                $isrestaurantInfoAdded = false;
                $restaurantCounter = -1; // counter  counting extra 1 so insilize by -1
                //echo '<pre>'; print_r($arrangedRecordsArray); die;
                foreach ($arrangedRecordsArray as $rec) {
                    $restaurantInfoArray = array(); // array to  insert restaurants  information
                    if (isset($rec['restaurant_name']) && $rec['restaurant_name'] != "" && $rec['city'] != "" && $rec['post_code'] != "" && $rec['address'] != "") {
                        // check if restaurants details already exists
                        $restaurantId = $this->import_model->getRestaurantDetails($rec['restaurant_name'], $rec['city'], $rec['post_code']);
                        if ($restaurantId) {
                            $hid = $restaurantId;
                            $restaurantCounter++;
                        } else {

                            // add resta details in db
                            $restaurantInfoArray['rest_name'] = $rec['restaurant_name'];
                          if (isset($rec['rest_code']) && $rec['rest_code'] != "")
                                $restaurantInfoArray['rest_code'] = $rec['rest_code'];
                            if (isset($rec['restaurant_name']) && $rec['restaurant_name'] != "")
                                $restaurantInfoArray['rest_address'] = $rec['address'];
                            if (isset($rec['restaurant_chain']) and $rec['restaurant_chain'] != "")
                                $restaurantInfoArray['chain_id'] = $this->restaurantChainId($rec['restaurant_chain']);
                            if (isset($rec['restaurant_type']) and $rec['restaurant_type'] != "")
                                $restaurantInfoArray['purpose'] = $this->restaurantPurposeId($rec['restaurant_type']);
                            if (isset($rec['country_code']) and $rec['country_code'] != "")
                                $restaurantInfoArray['country_code'] = $this->chkCountryCode($rec['country_code']);
                            if (isset($rec['city']) and $rec['city'] != "")
                                $restaurantInfoArray['city'] = $this->chkCountryCity($rec['city'], $rec['country_code']);
                            if (isset($rec['district']) and $rec['district'] != "")
                                $restaurantInfoArray['district'] = $this->chkCityDistrict($rec['district'], $rec['country_code'], $rec['city']);
                            if (isset($rec['post_code']) and $rec['post_code'] != "")
                                $restaurantInfoArray['post_code'] = $rec['post_code'];
                            if (isset($rec['currency_code']) and $rec['currency_code'] != "")
                                $restaurantInfoArray['currency'] = $rec['currency_code'];
                         
                            if (isset($rec['dorak_rating']) and $rec['dorak_rating'] != "")
                                $restaurantInfoArray['dorak_rating'] = $rec['dorak_rating'];
                            //$restaurantInfoArray['commisioned']=$rec['rest_name'];
                            if (isset($rec['status']) and $rec['status'] != "")
                                $restaurantInfoArray['status'] = getHotelStatusId($rec['status']); // using helper
                            $restaurantInfoArray['created_byuser'] = $user->id;
                            // add restaurant details
                            if (!empty($restaurantInfoArray) && count($restaurantInfoArray) > 0) {
								
								
                                if ($hid = $this->import_model->insertDetails($restaurantInfoArray)) {
                                    // if restaurant code not provided the  or empty the add code for the restaurant
                                    if (!isset($rec['rest_code']) || $rec['rest_code'] == "")
                                        $restaurantCode = $this->generateHotelcode($hid, $restaurantInfoArray);
                                }
                                $restaurantCounter++;
                                $isrestaurantInfoAdded = true;
								//echo '<pre>'; print_r($hid); die;
                            }
                        }
                    }
                    elseif (isset($rec['rest_code']) && $rec['rest_code'] != "") {
                        $hid = $this->import_model->getRestaurantDetailsBycode($rec['rest_code']);
                    }
                    // prepare  other  details of restaurant to add
                    if (!$hid && $hid != "") {
						//echo $hid; die;
                        $data['message'] .= 'Restaurants required  details are missing! Please Download sample file! and add content accordingly<br>';
                        $flashdata = array(
                            'flashdata' => $data['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                        redirect('/restaurants');
                        exit();
                    }
                    if ($hid && $hid != "") {
                        $age_group = '';
                        $isrestaurantInfoAdded = true;
                        if (isset($rec['age_group']) && $rec['age_group'] != "") {
                            $age_group = $this->getAgegroupId($rec['age_group']);
                            if ($age_group != "") {
                                $uchData = array('child_age_group_id' => $age_group);
                                $this->import_model->updateRestaurantDetails($hid, $uchData);
                            }
                        }
                        #add restaurant cuisine types
                        if (isset($rec['cuisine']) and $rec['cuisine'] != "") {
                            $restaurantPropertyTypeArray = explode(',', $rec['cuisine']); //coma seprated
                            if (count($restaurantPropertyTypeArray) > 0) {
                                $RestaurantCuisineTypeData = array();
                                foreach ($restaurantPropertyTypeArray as $restaurantPropertyType) {
                                    if (trim($restaurantPropertyType) != "") {
                                        $propertyType_id = $this->getPropertyType($restaurantPropertyType);
                                        $RestaurantCuisineTypeData[] = array(
                                            'rest_id' => $hid,
                                            'property_type_id' => $propertyType_id
                                        );
                                    }
                                }
                                if (!empty($RestaurantCuisineTypeData) && count($RestaurantCuisineTypeData) > 0) {
                                    $this->import_model->addRestaurantPropertyTypes($RestaurantCuisineTypeData);
                                }
                            }
                        }
                        #property type end
                        // check if  contact details 
                        if (isset($rec['designation']) && $rec['name'] != "" && $rec['email'] != "" && $rec['phone'] != "") {
                            if (isset($rec['extension']) && $rec['extension'] != "") {
                                $contactextension = $rec['extension'];
                            } else {
                                $contactextension = '';
                            }
                            $contactData[] = array(
                                'rest_id' => $hid,
                                'position' => $this->contactPositionId($rec['designation']),
                                'name' => $rec['name'],
                                'email' => $rec['email'],
                                'phone' => $rec['phone'],
                                'extension' => $contactextension
                            );
                        }
                        $this->import_model->deleteRestaurantContactInfo($hid);
                        //echo '<pre>'; print_r($contactData); die;
                        if (isset($rec['city_name']) && isset($rec['city_distance']) && $rec['city_distance'] != "" && $rec['city_name'] != "") {
                            $nearestDistanceData[] = array(
                                'rest_id' => $hid,
                                'distance_from' => $rec['city_name'],
                                'distance' => $rec['city_distance'],
                                'distance_type' => 1 // 1 for city
                            );
                        }
                        if (isset($rec['airport_name']) && isset($rec['airport_distance']) && $rec['airport_name'] != "" && $rec['airport_distance'] != "") {
                            $nearestDistanceData[] = array(
                                'rest_id' => $hid,
                                'distance_from' => $rec['airport_name'],
                                'distance' => $rec['airport_distance'],
                                'distance_type' => 2 // 2 for airport
                            );
                        }
                        $this->import_model->deleteRestaurantDistanceFrom($hid);
                        if (isset($rec['room_facility']) && $rec['room_facility'] != "") {
                            $roomFacilityComplimentaryData[] = array(
                                'rest_id' => $hid,
                                'cmpl_service_id' => $this->getCmplServiceId($rec['room_facility'])
                            );
                        }
                        $this->import_model->deleteComplimentaryServices($hid);
                        if (isset($rec['restaurant_facility']) && $rec['restaurant_facility'] != "") {
                            $restaurantFacilityData[] = array(
                                'rest_id' => $hid,
                                'facility_id' => $this->getFacilityId($rec['restaurant_facility'])
                            );
                        }
                        $this->import_model->deleteRestaurantFacilities($hid);
                        if (isset($rec['account_number']) && isset($rec['account_name']) && isset($rec['bank_name']) && isset($rec['bank_address']) && $rec['account_number'] != "" && $rec['account_name'] != "" && $rec['bank_name'] != "" && $rec['bank_address'] != "") {
                            $iban_code = '';
                            $ifsc_code = '';
                            $branch_name = '';
                            $branch_code = '';
                            $swift_code = '';
                            if (isset($rec['iban_code']) && $rec['iban_code'] != "") {
                                $iban_code = $rec['iban_code'];
                            }
                            if (isset($rec['branch_name']) && $rec['branch_name'] != "") {
                                $branch_name = $rec['branch_name'];
                            }
                            if (isset($rec['bank_branch_code']) && $rec['bank_branch_code'] != "") {
                                $branch_code = $rec['bank_branch_code'];
                            }
                            if (isset($rec['ifsc_code']) && $rec['ifsc_code'] != "") {
                                $ifsc_code = $rec['ifsc_code'];
                            }
                            if (isset($rec['swift_code']) && $rec['swift_code'] != "") {
                                $swift_code = $rec['swift_code'];
                            }
                            $bankingDetailsData[] = array(
                                'rest_id' => $hid,
                                'iban_code' => $iban_code,
                                'account_number' => $rec['account_number'],
                                'account_name' => $rec['account_name'],
                                'bank_name' => $rec['bank_name'],
                                'bank_address' => $rec['bank_address'],
                                'branch_name' => $branch_name,
                                'branch_code' => $branch_code,
                                'swift_code' => $swift_code,
                                'bank_ifsc_code' => $ifsc_code
                            );
                        }
                        $this->import_model->deleteRestaurantBankingInfo($hid);
                        // contract  imformation section
                        if (isset($rec['contract_start_date']) && isset($rec['contract_end_date']) && isset($rec['contract_signed_by']) && $rec['contract_start_date'] != "" && $rec['contract_end_date'] != "" && $rec['contract_signed_by'] != "") {
                            $contract_start_date = formatDateToMysql($rec['contract_start_date']); //use helper
                            $contract_end_date = formatDateToMysql($rec['contract_end_date']); //use helper
                            $contract_file_name = '';
                            if (isset($rec['contract_file_name']) && $rec['contract_file_name'] != "") {
                                $contract_file_name = $rec['contract_file_name'];
                            }
                            $contractInfoData[] = array(
                                'rest_id' => $hid,
                                'start_date' => $contract_start_date,
                                'end_date' => $contract_end_date,
                                'signed_by' => $rec['contract_signed_by'],
                                'contract_file' => $contract_file_name
                            );
                        }
                        // contract  imformation section end
                        // complimentary section	
                        if (isset($rec['package']) && $rec['package'] != "" && isset($rec['period_from_date']) && $rec['period_from_date'] != "" && isset($rec['period_to_date']) && $rec['period_to_date'] != "") {
                            $compliRoomNight = '';
                            $compliPeriodFromDate = '';
                            $compliPeriodToDate = '';
                            $compliUpgradable = '0'; // 0 mfor no		 
                            if (isset($rec['package']) && $rec['package'] != "") {
                                $compliRoomNight = $rec['package'];
                            }
                            if (isset($rec['period_from_date']) && $rec['period_from_date'] != "") {
                                $compliPeriodFromDate = formatDateToMysql($rec['period_from_date']);
                            }
                            if (isset($rec['period_to_date']) && $rec['period_to_date'] != "") {
                                $compliPeriodToDate = formatDateToMysql($rec['period_to_date']);
                            }
                            if (isset($rec['upgradable']) && $rec['upgradable'] != "") {
                                if (strtolower($rec['upgradable']) == 'yes')
                                    $compliUpgradable = 1;
                                else
                                    $compliUpgradable = 0;
                            }
                            $contractComplimentryRoomData = array(
                                'rest_id' => $hid,
                                'room_night' => $compliRoomNight,
                                'start_date' => $compliPeriodFromDate,
                                'end_date' => $compliPeriodToDate,
                                'upgrade' => $compliUpgradable,
                            );
                            // remove old ComlimentaryRoom details 
                            $this->import_model->deleteRestaurantComlimentaryRoom($hid);
                            // add  data in database table  and get  id
                            $cmpl_room_id = $this->import_model->importRestaurantComlimentaryRoom($contractComplimentryRoomData);
                            if ($cmpl_room_id && $rec['rest_name'] != "") {
                                $data['message'] .= $rec['rest_name'] . ' : Complimentary Room information imported. <br>';
                            }
                        }
                        if ($cmpl_room_id && $cmpl_room_id > 0 && isset($rec['excluded_from_date']) && $rec['excluded_from_date'] != "" && isset($rec['excluded_to_date']) && $rec['excluded_to_date'] != "") {
                            $excludedFromDate = formatDateToMysql($rec['excluded_from_date']);
                            $excludedToDate = formatDateToMysql($rec['excluded_to_date']);
                            $contractComplimentryRoomExData[] = array(
                                'cmpl_room_id' => $cmpl_room_id,
                                'exclude_date_from' => $excludedFromDate,
                                'excluded_date_to' => $excludedToDate
                            );
                        }
                        // complimentary section end
                        // renovation section
                        if (isset($rec['renovation_from']) && isset($rec['renovation_to']) && isset($rec['renovation_type']) && isset($rec['areas_effected']) && $rec['renovation_from'] != "" && $rec['renovation_to'] != "" && $rec['renovation_type'] != "" && $rec['areas_effected'] != "") {
                            $renovation_from = formatDateToMysql($rec['renovation_from']); //use helper
                            $renovation_to = formatDateToMysql($rec['renovation_to']); //use helper
                            $contractRenovationData[] = array(
                                'rest_id' => $hid,
                                'date_from' => $renovation_from,
                                'date_to' => $renovation_to,
                                'renovation_type' => $this->getRenovationTypeId($rec['renovation_type']),
                                'area_effected' => $rec['areas_effected']
                            );
                        }
                        $this->import_model->deleteRestaurantRenovationSchedule($hid);
                        // renovation section end
                        // payment plan section
                        if (isset($rec['plan']) && isset($rec['payment_plan_value']) && $rec['plan'] != "" && $rec['payment_plan_value'] != "") {
                            $contractPaymentPlanData[] = array(
                                'rest_id' => $hid,
                                'payment_option_id' => $this->getPaymentOpionId($rec['plan']),
                                'payment_value' => $rec['payment_plan_value'],
                            );
                        }
                        $this->import_model->deleteRestaurantPaymentShedules($hid);
                        // payment plan section end
                        if (isset($rec['low_season_cancel_before']) && isset($rec['low_season_cancel_before_value']) && $rec['low_season_cancel_before'] != "" && $rec['low_season_cancel_before_value'] != "") {
                            $contractCancellationInfoData[] = array(
                                'rest_id' => $hid,
                                'cancelled_before' => $this->getCancelBeforeOpionId($rec['low_season_cancel_before']),
                                'payment_request' => $rec['low_season_cancel_before_value'],
                                'seasion' => 'low'
                            );
                            $this->import_model->deleteRestaurantCancellation($hid, 'low');
                        }

                        if (isset($rec['high_season_cancel_before']) && isset($rec['high_season_cancel_before_value']) && $rec['high_season_cancel_before'] != "" && $rec['high_season_cancel_before_value'] != "") {
                            $contractCancellationInfoData[] = array(
                                'rest_id' => $hid,
                                'cancelled_before' => $this->getCancelBeforeOpionId($rec['high_season_cancel_before']),
                                'payment_request' => $rec['high_season_cancel_before_value'],
                                'seasion' => 'high'
                            );
                            $this->import_model->deleteRestaurantCancellation($hid, 'high');
                        }

                        // restaurant pricing section

                       /*  if (isset($rec['room_type']) && $rec['room_type'] != '' && isset($rec['currency']) && $rec['currency'] != '' && isset($rec['inventory']) && $rec['inventory'] != '') {
                            //$this->import_model->deleteRoompricing($hid);
                            $room_type = $this->getRoomTypeId($rec['room_type']);
                            $max_adult = '';
                            $complimentary = '';
                            $max_child = '';
                            $period_from = '';
                            $period_to = '';
                            $complimentary = '';
                            $inclusions = '';
                            if (isset($rec['max_adult']) && $rec['max_adult'] != "") {
                                $max_adult = $rec['max_adult'];
                            }
                            if (isset($rec['complimentary']) && $rec['complimentary'] != "") {
                                $complimentary = $rec['complimentary'];
                            }
                            if (isset($rec['max_child']) && $rec['max_child'] != "") {
                                $max_child = $rec['max_child'];
                            }
                            if (isset($rec['inclusions']) && $rec['inclusions'] != "") {
                                $inclusions = $rec['inclusions'];
                            }
                            if (isset($rec['period_from']) && $rec['period_from'] != "") {
                                $period_from = formatDateToMysql($rec['period_from']);
                            }
                            if (isset($rec['period_to']) && $rec['period_to'] != "") {
                                $period_to = formatDateToMysql($rec['period_to']);
                            }
                            $this->import_model->deleteRoompricing($hid, $room_type);
                            $restaurantRoomTypesDetailsData = array(
                                'rest_id' => $hid,
                                'room_type' => $room_type,
                                'inclusions' => $inclusions,
                                'curency_code' => $rec['currency'],
                                'max_adult' => $max_adult,
                                'max_child' => $max_child,
                                'inventory' => $rec['inventory'],
                                'period_from' => $period_from,
                                'period_to' => $period_to,
                            );
                            // add  data in database table  and get  id

                            $restaurant_room_pricing_id = $this->import_model->importRestaurantRoomsPricingData($restaurantRoomTypesDetailsData);
                            if ($restaurant_room_pricing_id && $rec['rest_name'] != "") {
                                $data['message'] .= $rec['rest_name'] . ' : Room Type Detail information imported. <br>';
                            }
                            $roomComplementaryArray = explode(',', $complimentary); //coma seprated
                            if (count($roomComplementaryArray) > 0) {
                                $restaurantRoomsPricingComplimentaryData = array();
                                foreach ($roomComplementaryArray as $roomComplementary) {
                                    if (trim($roomComplementary) != "") {
                                        $rcmpl_service_id = $this->getCmplServiceId(trim($roomComplementary));
                                        $restaurantRoomsPricingComplimentaryData[] = array(
                                            'pricing_id' => $restaurant_room_pricing_id,
                                            'cmpl_service_id' => $rcmpl_service_id
                                        );
                                    }
                                }
                                if (!empty($restaurantRoomsPricingComplimentaryData) && count($restaurantRoomsPricingComplimentaryData) > 0) {
                                    $this->import_model->deleteRoomPricingComplimentary($restaurant_room_pricing_id);
                                    $this->import_model->addRestaurantRoomPricingComplimentary($restaurantRoomsPricingComplimentaryData);
                                }
                            }
                        } */
                      /*   if ($restaurant_room_pricing_id && $restaurant_room_pricing_id > 0 && isset($rec['market']) && $rec['market'] != "") {
                            $market_id = $this->getMarketId($rec['market']);
                            $double_price = '';
                            $triple_price = '';
                            $quad_price = '';
                            $breakfast_price = '';
                            $half_board_price = '';
                            $price_all_inclusive = '';
                            $extra_adult_price = '';
                            $extra_child_price = '';
                            $extra_bed_price = '';
                            if (isset($rec['double_price']) && $rec['double_price'] != "") {
                                $double_price = $rec['double_price'];
                            }
                            if (isset($rec['triple_price']) && $rec['triple_price'] != "") {
                                $triple_price = $rec['triple_price'];
                            }
                            if (isset($rec['quad_price']) && $rec['quad_price'] != "") {
                                $quad_price = $rec['quad_price'];
                            }
                            if (isset($rec['breakfast_price']) && $rec['breakfast_price'] != "") {
                                $breakfast_price = $rec['breakfast_price'];
                            }
                            if (isset($rec['half_board_price']) && $rec['half_board_price'] != "") {
                                $half_board_price = $rec['half_board_price'];
                            }
                            if (isset($rec['price_all_inclusive']) && $rec['price_all_inclusive'] != "") {
                                $price_all_inclusive = $rec['price_all_inclusive'];
                            }
                            if (isset($rec['extra_adult_price']) && $rec['extra_adult_price'] != "") {
                                $extra_adult_price = $rec['extra_adult_price'];
                            }
                            if (isset($rec['extra_child_price']) && $rec['extra_child_price'] != "") {
                                $extra_child_price = $rec['extra_child_price'];
                            }
                            if (isset($rec['extra_bed_price']) && $rec['extra_bed_price'] != "") {
                                $extra_bed_price = $rec['extra_bed_price'];
                            }
                            $restaurantRoomTypesRateData[] = array(
                                'pricing_id' => $restaurant_room_pricing_id,
                                'market_id' => $market_id,
                                'double_price' => $double_price,
                                'triple_price' => $triple_price,
                                'quad_price' => $quad_price,
                                'breakfast_price' => $breakfast_price,
                                'half_board_price' => $half_board_price,
                                'all_incusive_adult_price' => $price_all_inclusive,
                                'extra_adult_price' => $extra_adult_price,
                                'extra_child_price' => $extra_child_price,
                                'extra_bed_price' => $extra_bed_price,
                            );
                        } */
                        // restaurant pricing section end
                    } else {

                        if (isset($rec['rest_name']) && isset($rec['city'])) {
                            $data['message'] .= 'Restaurants ' . $rec['rest_name'] . ' in city ' . $rec['city'] . ' required  details are missing!<br>';
                        } else {
                            $data['message'] .= 'Restaurants  required  details are missing or improper format!<br>';
                        }
                        $flashdata = array(
                            'flashdata' => $data['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                    $exhid = $hid;
                }
                // for new restaurant record the  restaurant details are changed  so we have to insert the  already  added in array value in database
                //echo '<pre>'; print_r($contactData);die;
                if (!empty($contactData) && count($contactData) > 0) {
                    // delete old record from database table and add new one			
                    $this->import_model->importRestaurantContactInfo($contactData);
                }

                if (!empty($nearestDistanceData) && count($nearestDistanceData) > 0) {
                    // delete old record from database table and add new one
                    //$this->import_model->deleteRestaurantDistanceFrom($hid);				
                    $this->import_model->importRestaurantDistanceFrom($nearestDistanceData);
                }
                if (!empty($restaurantFacilityData) && count($restaurantFacilityData) > 0) {
                    //$this->import_model->deleteRestaurantFacilities($hid);					
                    $this->import_model->importRestaurantFacilities($restaurantFacilityData);
                }
                if (!empty($roomFacilityComplimentaryData) && count($roomFacilityComplimentaryData) > 0) {
                    //$this->import_model->deleteComplimentaryServices($hid);		
                    $this->import_model->importComplimentaryServices($roomFacilityComplimentaryData);
                }
                if (!empty($bankingDetailsData) && count($bankingDetailsData) > 0) {
                    //$this->import_model->deleteRestaurantBankingInfo($hid);				
                    $this->import_model->importRestaurantBankingInfo($bankingDetailsData);
                }
                if (!empty($contractInfoData) && count($contractInfoData) > 0) {
                    $this->import_model->importRestaurantContract($contractInfoData);
                }
                if (!empty($contractComplimentryRoomExData) && count($contractComplimentryRoomExData) > 0) {
                    $this->import_model->importRestaurantComlimentaryRoomExcludedDate($contractComplimentryRoomExData);
                }
                if (!empty($contractRenovationData) && count($contractRenovationData) > 0) {
                    //$this->import_model->deleteRestaurantRenovationSchedule($hid);		
                    $this->import_model->importRestaurantRenovationSchedule($contractRenovationData);
                }
                if (!empty($contractPaymentPlanData) && count($contractPaymentPlanData) > 0) {
                    //$this->import_model->deleteRestaurantPaymentShedules($hid);		
                    $this->import_model->importRestaurantPaymentShedules($contractPaymentPlanData);
                }
                if (!empty($contractCancellationInfoData) && count($contractCancellationInfoData) > 0) {
                    $this->import_model->importRestaurantCancellation($contractCancellationInfoData);
                }
                if (!empty($restaurantRoomTypesRateData) && count($restaurantRoomTypesRateData) > 0) {
                    $this->import_model->importRestaurantRroomsPricingDetails($restaurantRoomTypesRateData);
                }
                if ($isrestaurantInfoAdded) {
                    $restaurantCounter++;
                    $data['message'] = 'Totel ' . $restaurantCounter . ' Restaurant(s) Information Imported.Please upload contract manually for each restaurant.<br>';
                    $flashdata = array(
                        'flashdata' => $data['message'],
                        'message_type' => 'notice'
                    );
                    $this->session->set_userdata($flashdata);
                } else {
                    $restaurantCounter++;
                    $data['message'] = 'No  Restaurant  Information Imported. <br>';
                    $flashdata = array(
                        'flashdata' => $data['message'],
                        'message_type' => 'notice'
                    );
                    $this->session->set_userdata($flashdata);
                }
                if (file_exists($restaurants_file)) {
                    try {
                        unlink($restaurants_file);
                    } catch (Exception $e) {
                        log_message('restaurant details import: ', $e->getMessage());
                    }
                }
            }
            redirect('/restaurants');
        }
    }

## other functions ####

    /**
     * @method string restaurantPurposeId()
     * @param string $purpose_title
     * @todo use to add if purpose not exists in database and return id / get purpose id  if already in database
     * @return purpose Id.	
     */
    private function restaurantPurposeId($purpose_title) {
        $purpose_title = removeExtraspace($purpose_title);
        if ($pid = $this->import_model->getPurpose($purpose_title)) {
            $purposeId = $pid;
        } else {
            # add data in database 
            $restaurant_purpose = array('entity_title' => $purpose_title, 'entity_type' => $this->config->item('attribute_resturant_purpose'));
            $purposeId = $this->import_model->addPurpose($restaurant_purpose);
        }
        return $purposeId;
    }

    /**
     * @method string restaurantChainId()
     * @method string $chain_title hold restaurant chain titile
     * @method use to add if chain not exists in database and return id / get id  if already in database
     * @return Chain Id.	
     */
    private function restaurantChainId($chain_title) {
        $chain_title = removeExtraspace($chain_title);
        /* check if already in database if not then add */
        if ($hcid = $this->import_model->getRestaurantChain($chain_title)) {
            $chainId = $hcid;
        } else {
            /* add data in database  */
            $restaurant_chain = array('entity_title' => $chain_title, 'entity_type' => $this->config->item('attribute_resturant_chain'));
            $chainId = $this->import_model->addRestaurantChain($restaurant_chain);
        }
        return $chainId;
    }

    /**
     * @method string contactPositionId()
     * @method string $title hold position title
     * @method use to add if contact Position not exists in database and return id / get id  if already in database
     * @return contact Position Id.	
     */
    private function contactPositionId($title) {
        $title = removeExtraspace($title);
        /* check if already in database if not then add */
        if ($hcid = $this->import_model->getPositions($title)) {
            $cId = $hcid;
        } else {
            /* add data in database  */
            $position_det = array('entity_title' => $title, 'entity_type' => $this->config->item('attribute_resturant_positions'));
            $cId = $this->import_model->addPositions($position_det);
        }
        return $cId;
    }

    /**
     * @method string getCmplServiceId()
     * @param string $title hold complementry service title
     * @todo use to add if Cmplimentary Service not exists in database and return id / get id  if already in database
     * @return   Service Id.	
     */
    private function getCmplServiceId($title) {
        $title = removeExtraspace($title);
        /* check if already in database if not then add */
        if ($hcSid = $this->import_model->getComplimentaryService($title)) {
            $csId = $hcSid;
        } else {
            /* add data in database  */
            $service_name_det = array('entity_title' => $title, 'entity_type' => $this->config->item('attribute_complementy_service'));
            $csId = $this->import_model->addComplimentaryService($service_name_det);
        }
        return $csId;
    }

    /**
     * @method string getCmplServiceId()
     * @param string $title hold facility title
     * @method use to add if Facility not exists in database and return id / get id  if already in database
     * @return   Facility Id.	
     */
    private function getFacilityId($title) {
        $title = removeExtraspace($title);
        /* check if already in database if not then add */
        if ($hfid = $this->import_model->getFacility($title)) {
            $fId = $hfid;
        } else {
            /* add data in database  */
            $facility_det = array('entity_title' => $title, 'entity_type' => $this->config->item('attribute_facilities'));
            $fId = $this->import_model->addFacility($facility_det);
        }
        return $fId;
    }

    /**
     * @method string getAgegroupId()
     * @param string $agegroup hold age group title
     * @todo use to add if Age group not exists in database and return id / get id  if already in database
     * @return   group Id.	
     */
    private function getAgegroupId($agegroup) {
        $agegroup = removeExtraspace($agegroup);
        /* check if already in database if not then add */
        if ($afid = $this->import_model->getAgeGroup($agegroup)) {
            $fId = $afid;
        } else {
            /* add data in database  */
            $agegroup_det = array('entity_title' => $agegroup, $this->config->item('attribute_child_group'));
            $fId = $this->import_model->addAgeGroup($agegroup_det);
        }
        return $fId;
    }

    /**
     * @method string getRoomTypeId()
     * @param string $getRoomType hold room type title
     * @todo use to add if room type not exists in database and return id / get id  if already in database
     * @return   Room Type Id.	
     */
    private function getRoomTypeId($getRoomType) {
        $getRoomType = removeExtraspace($getRoomType);
        /* check if already in database if not then add */
        if ($afid = $this->import_model->getRoomTypeId($getRoomType)) {
            $fId = $afid;
        } else {
            /* add data in database  */
            $getRoomType_det = array('entity_title' => $getRoomType, 'entity_type' => $this->config->item('attribute_room_types'));
            $fId = $this->import_model->addRoomType($getRoomType_det);
        }
        return $fId;
    }

    /**
     * @method string getPaymentOpionId()
     * @param string $patymentplan hold payment plan title
     * @todo use to add if payment opion not exists in database and return id / get id  if already in database
     * @return  Payment Opion Id.	
     */
    private function getPaymentOpionId($patymentplan) {
        $patymentplan = removeExtraspace($patymentplan);
        /* check if already in database if not then add */
        if ($afid = $this->import_model->getPaymentPlan($patymentplan)) {
            $fId = $afid;
        } else {
            /* add data in database  */
            $patymentplan_det = array('entity_title' => $patymentplan, 'entity_type' => $this->config->item('attribute_duration_payment'));
            $fId = $this->import_model->addPaymentPlan($patymentplan_det);
        }
        return $fId;
    }

    /**
     * @method string getPaymentOpionId()
     * @param string $renovationName hold renovation name
     * @method use to add if payment opion not exists in database and return id / get id  if already in database
     * @return  Payment Opion Id.	
     */
    private function getRenovationTypeId($renovationName) {
        $renovationName = removeExtraspace($renovationName);
        /* check if already in database if not then add */
        if ($renoID = $this->import_model->getRenovationName($renovationName)) {
            $fId = $renoID;
        } else {
            /* add data in database  */
            $patymentplan_det = array('renovation_type' => $renovationName);
            $fId = $this->import_model->addRenovationName($patymentplan_det);
        }
        return $fId;
    }

    /**
     * @method string getCancelBeforeOpionId()
     * @param string $optitle hold cancellation title
     * @todo use to add if cancel before opion not exists in database and return id / get id  if already in database
     * @return  cancel before opion Id.	
     */
    private function getCancelBeforeOpionId($optitle) {
        $optitle = removeExtraspace($optitle);
        /* check if already in database if not then add */
        if ($afid = $this->import_model->getCancelBefore($optitle)) {
            $fId = $afid;
        } else {
            /* add data in database  */
            $optitle_det = array('entity_title' => $optitle, 'entity_type' => $this->config->item('attribute_duration_cancellation'));
            $fId = $this->import_model->addCancelBefore($optitle_det);
        }
        return $fId;
    }

    /**
     * @method string getMarketId()
     * @param string $mtitle hold market title
     * @todo use to add if Market not exists in database and return id / get id  if already in database
     * @return  Market Id .	
     */
    private function getMarketId($mtitle) {
        $mtitle = removeExtraspace($mtitle);
        /* check if already in database if not then add */
        if ($afid = $this->import_model->getMarket($mtitle)) {
            $fId = $afid;
        } else {
            /* add data in database  */
            $m_det = array('entity_title' => $mtitle, 'entity_type' => $this->config->item('attribute_market'));
            $fId = $this->import_model->addMarket($m_det);
        }
        return $fId;
    }

    /**
     * @method string getPropertyType()
     * @param string $ptitle hold property title
     * @method use to add if property type not exists in database and return id / get id  if already in database
     * @return  Property Type ID.	
     */
    private function getPropertyType($ptitle) {
        $mtitle = removeExtraspace($ptitle);
        /* check if already in database if not then add */
        if ($afid = $this->import_model->getPropertyTypeId($ptitle)) {
            $fId = $afid;
        } else {
            /* add data in database  */
            $m_det = array('entity_title' => $mtitle, 'entity_type' => $this->config->item('attribute_property_types'));
            $fId = $this->import_model->addPropertyType($m_det);
        }
        return $fId;
    }

    /**
     * @method string chkCountryCity()
     * @param string $city hold city name
     * @param string $country_code hold country code
     * @todo use to add if country city not exists in database and return id / get id  if already in database
     * @return  city  code.	
     */
    private function chkCountryCity($city, $country_code = '') {
        if (!empty($country_code) && !empty($city)) {
            $country_code = removeExtraspace($country_code);
            $city = removeExtraspace($city);
            $country_id = $this->import_model->getCountryId($country_code);
            /* check if already in database if not then add */
            if (!$this->import_model->getCityId($city, $country_code)) {
                /* add data in database  */
                $city_det = array('country_id' => $country_id, 'country_code' => $country_code, 'city_code' => $city, 'city_name' => $city);
                $this->import_model->addCity($city_det);
            }
            return $city;
        }
        return;
    }

    /**
     * @method string chkCityDistrict()
     * @param string $dname hold district name
     * @param string $country_code hold country code
     * @param string $city hold city name
     * @todo use to add if country district not exists in database and return district / get district if already in database
     * @return  district.	
     */
    private function chkCityDistrict($dname, $country_code = '', $city = '') {
        if (!empty($dname) && !empty($country_code) && !empty($city)) {
            $dname = removeExtraspace($dname);
            $city = removeExtraspace($city);
            $country_code = removeExtraspace($country_code);
            $country_id = $this->import_model->getCountryId($country_code);
            $cityid = $this->import_model->getCityId($city, $country_code);
            /* check if already in database if not then add */
            if (!$this->import_model->getDistrictId($dname, $cityid)) {
                /* add data in database  */
                $districtDet = array('country_id' => $country_id, 'city_id' => $cityid, 'city_name' => $city, 'district_name' => $dname);
                $this->import_model->addDistrict($districtDet);
            }
            return $dname;
        }
    }

    /**
     * @method string chkCountryCode()
     * @param string $countrycode hold country code
     * @todo use to add if country code not exists in database and return code / get code  if already in database
     * @return  city  code.	
     */
    private function chkCountryCode($countrycode) {
        $countrycode = removeExtraspace($countrycode);
        /* check if already in database if not then add */
        if ($dcode = $this->import_model->getCountryCode($countrycode)) {
            $countrycode = $dcode;
        } else {
            /* add data in database  */
            $country_det = array('country_code' => $countrycode, 'country_name' => $countrycode);
            $this->import_model->addCountry($country_det);
        }
        return $countrycode;
    }

    /**
     * @method string generateHotelcode()
     * @param int $inserted_rest_id hold restaurant id
     * @param array() $restaurant_data hold data in the form of an array
     * @method use to generate restaurant code
     * @return   code.	
     */
    private function generateHotelcode($inserted_rest_id, $restaurant_data) {
        $hCodePrefix = $this->config->item('rest_code_prefix');
        $first4CharOfName = substr($restaurant_data['rest_name'], 0, 4);
        $first3CharOfCity = substr($restaurant_data['city'], 0, 3);
        $restaurantCountryCode = $restaurant_data['country_code'];
        $restaurantCityCode = getCityCode($restaurant_data['city']); // using helper				
        $hcode = $hCodePrefix . $inserted_rest_id . "-" . $first4CharOfName;
        if ($restaurantCityCode && $restaurantCityCode != "") {
            $hcode.="-" . $restaurantCityCode;
        } else {
            $hcode.="-" . $first3CharOfCity;
        }
        if ($restaurantCountryCode && $restaurantCountryCode != "")
            $hcode.="-" . $restaurantCountryCode;
        $uhData = array(
            'rest_code' => $hcode);
        $this->import_model->updateRestaurantDetails($inserted_rest_id, $uhData);
        return $hcode;
    }

    /**
     * @method string export_restaurants()
     * @todo used for export restaurants into excel file using php excel library
     * @return boolean.	
     */
    public function export_restaurant() {
        $this->load->model('restaurants/restaurant_model');
        $restaurant_data = $this->restaurant_model->getHotelsList();

        if (!$restaurant_data) {
            // restaurant data not exists;
            $sdata['message'] = 'Data Not Exists.';
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'error'
            );
            $this->session->set_userdata($flashdata);
            redirect('/restaurants', 'refresh');
        } else {
            $this->load->library('excel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
            $objPHPExcel->setActiveSheetIndex(0);

            //Custom Excel header
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '#0C27F2'),
                    'size' => 20,
                    'name' => 'Verdana'
            ));

//        $to = 'J1';
//        $from = 'N1';
//        $cell_value = 'Dorake Holdings';
//        $this->merge_custom_cell($to,$from,$cell_value,$styleArray,$objPHPExcel);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:N1');
            $objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Dorake Holding');
            $objPHPExcel->getActiveSheet()->getStyle('J1:N1')->applyFromArray($styleArray);


            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:N2');
            $objPHPExcel->getActiveSheet()->getCell('A2')->setValue('Restaurant Information');
            $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "F28A8C"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('O2:S2');
            $objPHPExcel->getActiveSheet()->getCell('O2')->setValue('Contact Details');
            $objPHPExcel->getActiveSheet()->getStyle("O2:S2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "90C3D4"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('T2:U2');
            $objPHPExcel->getActiveSheet()->getCell('T2')->setValue('Distnace');
            $objPHPExcel->getActiveSheet()->getStyle("T2:U2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "A1D490"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('V2:W2');
            $objPHPExcel->getActiveSheet()->getCell('V2')->setValue('Airport Dest');
            $objPHPExcel->getActiveSheet()->getStyle("V2:W2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "C390D4"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('X2:AA2');
            $objPHPExcel->getActiveSheet()->getCell('X2')->setValue('Contract Info');
            $objPHPExcel->getActiveSheet()->getStyle("X2:AA2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "D4A190"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB2:AE2');
            $objPHPExcel->getActiveSheet()->getCell('AB2')->setValue('Renovation Schedule');
            $objPHPExcel->getActiveSheet()->getStyle("AB2:AE2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "D4A190"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF2:AN2');
            $objPHPExcel->getActiveSheet()->getCell('AF2')->setValue('Banking Details');
            $objPHPExcel->getActiveSheet()->getStyle("AF2:AN2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "C390D4"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('AO2:AR2');
            $objPHPExcel->getActiveSheet()->getCell('AO2')->setValue('Cancellation');
            $objPHPExcel->getActiveSheet()->getStyle("AO2:AR2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "F28A8C"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('AS2:AX2');
            $objPHPExcel->getActiveSheet()->getCell('AS2')->setValue('Complimentry Room');
            $objPHPExcel->getActiveSheet()->getStyle("AS2:AX2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "C390D4"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('AY2:AZ2');
            $objPHPExcel->getActiveSheet()->getCell('AY2')->setValue('Payment Plan');
            $objPHPExcel->getActiveSheet()->getStyle("AY2:AZ2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "D4A190"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->getActiveSheet()->getCell('BA2')->setValue('Restaurant Facilities');
            $objPHPExcel->getActiveSheet()->getStyle("BA2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "C390D4"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->getActiveSheet()->getCell('BB2')->setValue('Room Facilities');
            $objPHPExcel->getActiveSheet()->getStyle("BB2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "D4A190"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('BC2:BT2');
            $objPHPExcel->getActiveSheet()->getCell('BC2')->setValue('Room Type Detail');
            $objPHPExcel->getActiveSheet()->getStyle("BC2:BT2")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "F28A8C"
                ),
                'font' => array(
                    'size' => 12,
                    'name' => 'Verdana'
                )
            ));


            // Field names in the first row
            $fields = $this->db->list_fields('restaurants');
            unset($fields[0]);
            unset($fields[17]);
            unset($fields[14]);
            unset($fields[15]);
            unset($fields[18]);

            $objPHPExcel->getActiveSheet()->getCell('O3')->setValue('Designation');
            $objPHPExcel->getActiveSheet()->getCell('P3')->setValue('Name');
            $objPHPExcel->getActiveSheet()->getCell('Q3')->setValue('Email');
            $objPHPExcel->getActiveSheet()->getCell('R3')->setValue('Phone');
            $objPHPExcel->getActiveSheet()->getCell('S3')->setValue('Extention');

            $objPHPExcel->getActiveSheet()->getCell('T3')->setValue('City Name');
            $objPHPExcel->getActiveSheet()->getCell('U3')->setValue('City Distance');

            $objPHPExcel->getActiveSheet()->getCell('V3')->setValue('Airpot Name');
            $objPHPExcel->getActiveSheet()->getCell('W3')->setValue('Airport Destination');

            $objPHPExcel->getActiveSheet()->getCell('X3')->setValue('Contact Start Date');
            $objPHPExcel->getActiveSheet()->getCell('Y3')->setValue('Contact End Date');
            $objPHPExcel->getActiveSheet()->getCell('Z3')->setValue('Contact Signed Date');
            $objPHPExcel->getActiveSheet()->getCell('AA3')->setValue('Contact File Name');

            $objPHPExcel->getActiveSheet()->getCell('AB3')->setValue('renovation from');
            $objPHPExcel->getActiveSheet()->getCell('AC3')->setValue('renovation to');
            $objPHPExcel->getActiveSheet()->getCell('AD3')->setValue('renovation type');
            $objPHPExcel->getActiveSheet()->getCell('AE3')->setValue('Areas Effected');

            $objPHPExcel->getActiveSheet()->getCell('AF3')->setValue('Iban Code');
            $objPHPExcel->getActiveSheet()->getCell('AG3')->setValue('Account Number');
            $objPHPExcel->getActiveSheet()->getCell('AH3')->setValue('Account Name ');
            $objPHPExcel->getActiveSheet()->getCell('AI3')->setValue('Bank Name');
            $objPHPExcel->getActiveSheet()->getCell('AJ3')->setValue('Bank Address');
            $objPHPExcel->getActiveSheet()->getCell('AK3')->setValue('Branch Name');
            $objPHPExcel->getActiveSheet()->getCell('AL3')->setValue('Bank Branch Code');
            $objPHPExcel->getActiveSheet()->getCell('AM3')->setValue('IFSC Code');
            $objPHPExcel->getActiveSheet()->getCell('AN3')->setValue('SWIFT Code');

            $objPHPExcel->getActiveSheet()->getCell('AO3')->setValue('low season cancel before');
            $objPHPExcel->getActiveSheet()->getCell('AP3')->setValue('Value');
            $objPHPExcel->getActiveSheet()->getCell('AQ3')->setValue('high season cancel before');
            $objPHPExcel->getActiveSheet()->getCell('AR3')->setValue('Value');

            $objPHPExcel->getActiveSheet()->getCell('AS3')->setValue('room night');
            $objPHPExcel->getActiveSheet()->getCell('AT3')->setValue('period from date');
            $objPHPExcel->getActiveSheet()->getCell('AU3')->setValue('period to date');
            $objPHPExcel->getActiveSheet()->getCell('AV3')->setValue('Upgradable');
            $objPHPExcel->getActiveSheet()->getCell('AW3')->setValue('Excluded from Date ');
            $objPHPExcel->getActiveSheet()->getCell('AX3')->setValue('Excluded To Date');

            $objPHPExcel->getActiveSheet()->getCell('AY3')->setValue('Plan');
            $objPHPExcel->getActiveSheet()->getCell('AZ3')->setValue('Plan Value');
            $objPHPExcel->getActiveSheet()->getCell('BB3')->setValue('Hotel Facilities');
            $objPHPExcel->getActiveSheet()->getCell('BC3')->setValue('Room Facilities');

            $objPHPExcel->getActiveSheet()->getCell('BC3')->setValue('inclusions');
            $objPHPExcel->getActiveSheet()->getCell('BD3')->setValue('double price');
            $objPHPExcel->getActiveSheet()->getCell('BE3')->setValue('triple price');
            $objPHPExcel->getActiveSheet()->getCell('B3')->setValue('quad price');
            $objPHPExcel->getActiveSheet()->getCell('BG3')->setValue('breakfast price');
            $objPHPExcel->getActiveSheet()->getCell('BH3')->setValue('half board price');
            $objPHPExcel->getActiveSheet()->getCell('BI3')->setValue('all adult price');
            $objPHPExcel->getActiveSheet()->getCell('BJ3')->setValue('extra adult price');
            $objPHPExcel->getActiveSheet()->getCell('BK3')->setValue('extra child price');
            $objPHPExcel->getActiveSheet()->getCell('BL3')->setValue('extra bed price');
            $objPHPExcel->getActiveSheet()->getCell('BM3')->setValue('max adult');
            $objPHPExcel->getActiveSheet()->getCell('BN3')->setValue('max child');
            $objPHPExcel->getActiveSheet()->getCell('BO3')->setValue('period from');
            $objPHPExcel->getActiveSheet()->getCell('BP3')->setValue('period to');
            $objPHPExcel->getActiveSheet()->getCell('BQ3')->setValue('curency code');
            $objPHPExcel->getActiveSheet()->getCell('BR3')->setValue('Room Type');
            $objPHPExcel->getActiveSheet()->getCell('BS3')->setValue('Market');
            $objPHPExcel->getActiveSheet()->getCell('BT3')->setValue('inventory');

            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 3, $field);
                $col++;
            }

            // Custom coloums array to export the data into table field coloums

            $contact_data = array();
            $contact_fields = $this->db->list_fields('rest_contact_details');
            $contact_fields[2] = 'title';
            unset($contact_fields[0]);
            unset($contact_fields[1]);

            $restaurant_distance = array();
            $restaurant_distance_coloum = $this->db->list_fields('rest_distance');
            unset($restaurant_distance_coloum[0]);
            unset($restaurant_distance_coloum[1]);
            unset($restaurant_distance_coloum[4]);

            $restaurant_contract_info = array();
            $restaurant_contract_coloum = $this->db->list_fields('rest_contract_info');
            unset($restaurant_contract_coloum[0]);
            unset($restaurant_contract_coloum[1]);
            unset($restaurant_contract_coloum[6]);
            unset($restaurant_contract_coloum[7]);

            $restaurant_renovation_info = array();
            $restaurant_renovation_coloum = $this->db->list_fields('rest_renovation_schedule');
            unset($restaurant_renovation_coloum[0]);
            unset($restaurant_renovation_coloum[1]);
            unset($restaurant_renovation_coloum[6]);

            $restaurant_bank_info = array();
            $restaurant_bank_coloum = $this->db->list_fields('rest_bank_accounts');
            unset($restaurant_bank_coloum[0]);
            unset($restaurant_bank_coloum[1]);
            unset($restaurant_bank_coloum[11]);
            unset($restaurant_bank_coloum[12]);


            $restaurant_cancellation_info = array();
            $restaurant_cancellation_coloum = $this->db->list_fields('rest_cancellation');
            unset($restaurant_cancellation_coloum[0]);
            unset($restaurant_cancellation_coloum[1]);

            $restaurant_complimentry_info = array();
            $restaurant_complimentry_coloum = $this->db->list_fields('rest_complimentary_room');
            $restaurant_complimentry_coloum[7] = 'exclude_date_from';
            $restaurant_complimentry_coloum[8] = 'excluded_date_to';
            unset($restaurant_complimentry_coloum[0]);
            unset($restaurant_complimentry_coloum[1]);
            unset($restaurant_complimentry_coloum[3]);

            $restaurant_payment_info = array();
            $restaurant_payment_coloum = $this->db->list_fields('rest_payment_shedules');
            $restaurant_payment_coloum[2] = 'plan';
            unset($restaurant_payment_coloum[0]);
            unset($restaurant_payment_coloum[1]);

            $restaurant_facilities_info = array();
            $restaurant_facilities_coloum[1] = 'facilities';

            $room_facilities_info = array();
            $room_facilities_coloum[1] = 'room_facilities';

            $room_type_info = array();
            $room_price_coloum = $this->db->list_fields('restaurant_rooms_pricing');
            $room_price_detail_coloum = $this->db->list_fields('restaurant_rooms_pricing_details');
            $room_type_coloum = array_merge($room_price_detail_coloum, $room_price_detail_coloum);
            $room_type_coloum[2] = 'inclusions';
            $room_type_coloum[26] = 'curency_code';
            $room_type_coloum[27] = 'title';
            $room_type_coloum[28] = 'market';
            $room_type_coloum[29] = 'inventory';
            $room_type_coloum[21] = 'max_adult';
            $room_type_coloum[22] = 'max_child';
            $room_type_coloum[23] = 'period_from';
            $room_type_coloum[24] = 'period_to';
            unset($room_type_coloum[0]);
            unset($room_type_coloum[1]);
            unset($room_type_coloum[13]);
            unset($room_type_coloum[12]);
            unset($room_type_coloum[14]);
            unset($room_type_coloum[15]);
            unset($room_type_coloum[16]);
            unset($room_type_coloum[17]);
            unset($room_type_coloum[18]);
            unset($room_type_coloum[19]);
            unset($room_type_coloum[20]);
            unset($room_type_coloum[25]);

            $returnArray = array();
            $returnArray['next_counter'] = 0;
            $returnArray['coloum_count'] = 0;
            $row = 4;
            foreach ($restaurant_data as $data) {
                $col = 0;
                foreach ($fields as $field) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, isset($data->$field) ? $data->$field : null);
                    $col++;
                }

                $restaurant_col = count($fields);
                $intial_contracts_counter = count($fields);
                $query = $this->db->query('select * from rest_contact_details inner join positions on rest_contact_details.position = positions.id where rest_id = "' . $data->rest_id . '"');
                $contact_data = $query->result();
                if (!empty($contact_data)) {
                    foreach ($contact_data as $contact_datas) {
                        foreach ($contact_fields as $contact_field) {
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($restaurant_col, $row, isset($contact_datas->$contact_field) ? $contact_datas->$contact_field : null);
                            $restaurant_col++;
                        }
                        $restaurant_col = $intial_contracts_counter;
                        $row++;
                    }
                }
                //Restaurant Distance array into excel format
                $next_tab_counter = $restaurant_col + count($contact_fields);
                $intial_distance_counter = $next_tab_counter;
                $query = $this->db->query('select distance_from,distance,distance_type from rest_distance where distance_type = 1 AND rest_id = "' . $data->rest_id . '"');
                $restaurant_distance = $query->result();
                if (!empty($restaurant_distance)) {
                    foreach ($restaurant_distance as $restaurant_distances) {
                        foreach ($restaurant_distance_coloum as $restaurant_distance_coloums) {
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($next_tab_counter, $row, isset($restaurant_distances->$restaurant_distance_coloums) ? $restaurant_distances->$restaurant_distance_coloums : null);
                            $next_tab_counter++;
                        }
                        $next_tab_counter = $intial_distance_counter;
                        $row++;
                    }
                }
                //end restaurant distance array format
                //Restaurant Distance array into excel format
                $next_airport_count = $next_tab_counter + count($restaurant_distance_coloum);
                $intial_airport_counter = $next_airport_count;
                $query = $this->db->query('select distance_from,distance,distance_type from rest_distance where distance_type = 2 AND rest_id = "' . $data->rest_id . '"');
                $restaurant_airport_distance = $query->result();
                if (!empty($restaurant_airport_distance)) {
                    foreach ($restaurant_airport_distance as $restaurant_airport_distances) {
                        foreach ($restaurant_distance_coloum as $restaurant_distance_coloums) {
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($next_airport_count, $row, isset($restaurant_airport_distances->$restaurant_distance_coloums) ? $restaurant_airport_distances->$restaurant_distance_coloums : null);
                            $next_airport_count++;
                        }
                        $next_airport_count = $intial_airport_counter;
                        $row++;
                    }
                }
                //end restaurant distance array format
                //restaurant contract info array format
                $next_contract_info = $next_airport_count + count($restaurant_distance_coloum);
                $intial_counter_info_counter = $next_contract_info;
                $query = $this->db->query('select start_date,end_date,signed_by,contract_file from rest_contract_info where rest_id = "' . $data->rest_id . '"');
                $restaurant_contract_info = $query->result();

                if (!empty($restaurant_contract_info)) {
//                dump(count($restaurant_contract_info));
                    foreach ($restaurant_contract_info as $restaurant_contract_infos) {
                        foreach ($restaurant_contract_coloum as $restaurant_contract_coloums) {
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($next_contract_info, $row, isset($restaurant_contract_infos->$restaurant_contract_coloums) ? $restaurant_contract_infos->$restaurant_contract_coloums : null);
                            $next_contract_info++;
                        }
                        $next_contract_info = $intial_counter_info_counter;
                        $row++;
                    }
                }
                //end restaurant distance array format
                //restaurant renovation shedule array format
                $next_restaurant_renovation_info = $next_contract_info + count($restaurant_contract_coloum);
                $intial_restaurant_renovation_counter = $next_restaurant_renovation_info;
                $query = $this->db->query('select rest_id,date_from,date_to,renovation_type,area_effected from rest_renovation_schedule where rest_id = "' . $data->rest_id . '"');
                $restaurant_renovation_info = $query->result();


                $returnArray = $this->writeHoteldata($restaurant_renovation_info, $restaurant_renovation_coloum, $next_restaurant_renovation_info, $intial_restaurant_renovation_counter, $objPHPExcel, $row);
//            dump($returnArray);die;
                //restaurant banking detail array format
                $next_banking_info = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_restaurant_bank_counter = $next_banking_info;
                $query = $this->db->query('select * from rest_bank_accounts where rest_id = "' . $data->rest_id . '"');
                $restaurant_bank_info = $query->result();

                $returnArray = $this->writeHoteldata($restaurant_bank_info, $restaurant_bank_coloum, $next_banking_info, $intial_restaurant_bank_counter, $objPHPExcel, $row);
//           
                //restaurant cancellation array format
                $next_cancellation_info = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_cancellation_counter = $next_cancellation_info;
                $query = $this->db->query('select * from rest_cancellation where rest_id = "' . $data->rest_id . '"');
                $restaurant_cancellation_info = $query->result();

                $returnArray = $this->writeHoteldata($restaurant_cancellation_info, $restaurant_cancellation_coloum, $next_cancellation_info, $intial_cancellation_counter, $objPHPExcel, $row);

                //restaurant complimentry array format
                $next_complimentry_info = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_complimentry_counter = $next_complimentry_info;
                $query = $this->db->query('select room_night,DATE(start_date) as start_date,DATE(end_date) as end_date,upgrade,DATE(exclude_date_from) as exclude_date_from ,DATE(excluded_date_to) as excluded_date_to from rest_complimentary_room inner join rest_cmplimntry_room_excluded_dates on rest_complimentary_room.cmpl_room_id = rest_cmplimntry_room_excluded_dates.cmpl_room_id  where rest_id = "' . $data->rest_id . '"');
                $restaurant_complimentry_info = $query->result();
                $returnArray = $this->writeHoteldata($restaurant_complimentry_info, $restaurant_complimentry_coloum, $next_complimentry_info, $intial_complimentry_counter, $objPHPExcel, $row);

                //restaurant payment plan array format
                $next_payment_info = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_payment_counter = $next_payment_info;
                $query = $this->db->query('select payment_value,option_title as plan from rest_payment_shedules inner join payment_options on rest_payment_shedules.payment_option_id = payment_options.id where rest_id = "' . $data->rest_id . '"');
                $restaurant_payment_info = $query->result();
                $returnArray = $this->writeHoteldata($restaurant_payment_info, $restaurant_payment_coloum, $next_payment_info, $intial_payment_counter, $objPHPExcel, $row);

                //restaurant facilities array format
                $next_facilities_info = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_facilities_counter = $next_facilities_info;
                $query = $this->db->query('select facility_title as facilities  from rest_facilities inner join facilities on rest_facilities.facility_id = facilities.facility_id where rest_id = "' . $data->rest_id . '"');
                $restaurant_facilities_info = $query->result();
                $returnArray = $this->writeHoteldata($restaurant_facilities_info, $restaurant_facilities_coloum, $next_facilities_info, $intial_facilities_counter, $objPHPExcel, $row);

                //restaurant room facilities array format
                $room_facilities_count = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_room_facilities_counter = $room_facilities_count;
                $query = $this->db->query('select service_name as room_facilities from rest_complimentary_services inner join complimentary_services on rest_complimentary_services.cmpl_service_id = complimentary_services.cmpl_service_id  where rest_id = "' . $data->rest_id . '"');
                $room_facilities_info = $query->result();
                $returnArray = $this->writeHoteldata($room_facilities_info, $room_facilities_coloum, $room_facilities_count, $intial_room_facilities_counter, $objPHPExcel, $row);

                //restaurant Room Type array format
                $room_type_count = (!empty($returnArray['next_counter']) ? $returnArray['next_counter'] : null) + (!empty($returnArray['coloum_count']) ? $returnArray['coloum_count'] : null);
                $intial_room_type_counter = $room_type_count;
                $room_type_info = $this->import_model->get_room_types($data->rest_id);
                $returnArray = $this->writeHoteldata($room_type_info, $room_type_coloum, $room_type_count, $intial_room_type_counter, $objPHPExcel, $row);

                //end Room Type array format
                $row++;
            }

            $objPHPExcel->setActiveSheetIndex(0);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // Sending headers to force the user to download the file
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Restaurants' . date('dMy') . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');

            $sdata['message'] = 'Restaurant Exported Successfully';
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'sucess'
            );
            $this->session->set_userdata($flashdata);
            redirect('/restaurants', 'refresh');
        }
    }

    /**
     * @method writeHoteldata()
     * @todo Write data into excel file in the provided array
     * @param mixed[] $items Array,$coloum Array, $counter,$obj,structure to write data.
     * @return int[] $return_array Returns the nextcounter and initial counter of elements.
     */
    function writeHoteldata($data_array = array(), $coloum_array = array(), $next_counter = null, $initial_counter = null, $obj = null, $row = null) {
        $return_array = array();
        if (!empty($data_array)) {
            foreach ($data_array as $data_arrays) {
                foreach ($coloum_array as $coloum_arrays) {
                    $obj->getActiveSheet()->setCellValueByColumnAndRow($next_counter, $row, isset($data_arrays->$coloum_arrays) ? $data_arrays->$coloum_arrays : null);
                    $next_counter++;
                }
                $next_counter = $initial_counter;
                $row++;
            }
            $return_array['next_counter'] = $next_counter;
            $return_array['coloum_count'] = count($coloum_array);
            return $return_array;
        }
        return $return_array;
    }

}
