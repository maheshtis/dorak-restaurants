<?php

/**
 * @author tis
 * @author M
 * @functions:
  edit_information() method used to update restaurant information using ajax request
 * @description this controller  is for  add/ edit delete restaurant  related  information
 * @uses AdminController::Common base controller to perform all type of action.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajaxform extends AdminController {

    /** @var string|null Should contain a user identification */
    public static $identityfier;

    function __construct() {
        parent::__construct();
        $this->_init();
    }

    /**
     * @method _init() method used initialise associative helpers('form','html','form_field_values','file','directory','array_column')and library('form_validation','datatables') and restaurant_model
     */
    private function _init() {
        if (!$this->ion_auth->logged_in()) {
            // redirect non logged user to  login page
            redirect('auth/login', 'refresh');
        }
        $this->identityfier = $this->session->userdata('session_id') . '_' . time();
        $this->load->helper(array('form', 'html', 'form_field_values', 'file', 'directory'));
        $this->load->library('form_validation');
        $this->load->helper('array_column');
        $this->load->library('Datatables');
        $this->output->set_meta('description', 'Dorak Restaurant');
        $this->output->set_meta('title', 'Dorak Restaurant');
        $this->output->set_title('Dorak Restaurant');
        $this->load->model('restaurants/restaurant_model');
        $this->accessDenidMessage = $this->config->item('access_not_allowed_message');
        $this->contractFileDir = $this->config->item('upload_contract_file_dir');
        $this->marketFileDir = $this->config->item('upload_market_file_dir');
        $this->resturantFileDir = $this->config->item('resturant_gallery_dir');
        $this->resturantCommonfileDir = $this->config->item('common_menu_dir');
        $this->restaurant_model->clear_gallery_image_table();
        if (!checkAccess($this->accessLabelId, 'restaurants', 'view')) {
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
    }

    /**
     * @method edit_information() method used to update restaurant information using ajax request
     * @param int $hid restaurant id
     * @todo this method used for update restaurant information
     * @return json value|false
     */
    public function edit_information($hid = null) {
//        dump($_FILES);
        $data = array('message' => '');
        // this  function edit restaurant information	
        if (!checkAccess($this->accessLabelId, 'restaurants', 'edit')) {
            $flashdata = array('flashdata' => $this->accessDenidMessage, 'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
        if ($hid == "") {
            $data['message'] = 'Invalid  Access!';
            $flashdata = array('flashdata' => $data['message'], 'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect("restaurants"); /* redirect to restaurant listing page */
        }
        $this->load->library('upload');
        $this->output->set_meta('description', 'Restaurant Information');
        $this->output->set_meta('title', 'Restaurant Information');
        $this->output->set_title('Restaurant Information');
        $user = $this->ion_auth->user()->row();
        if ($this->session->userdata('stepsess') == '#tab-5') {
            $this->session->set_userdata('stepsess', '#tab-6');
        }

        if (!empty($_POST)) {
            $identity = $this->input->post('identity');
            $step = $this->input->post('step'); /* input for save data on tab basis */
            $this->session->set_userdata('stepsess', $step); /* session start for tabs */
            # validation rules 

            $contactData = array();
            $nearestDistanceData = array();  /* we'll store each distance row in this array */
            $restaurantBankData = array();
            $purpose = $this->input->post('purpose');
            if ($purpose != '' && $purpose == 'Other') {
                # insert new purpose
                $purpose_options_oth = $this->input->post('purpose_options_oth');
                if ($purpose_options_oth != '') {
                    # check if already in database if not then add
                    if ($pid = $this->restaurant_model->getPurpose($purpose_options_oth)) {
                        $purposeId = $pid;
                    } else {
                        # add data in database 
                        $restaurant_purpose = array('title' => $purpose_options_oth);
                        $purposeId = $this->restaurant_model->addPurpose($restaurant_purpose);
                    }
                }
            } else {
                $purposeId = $purpose;
            }
            # add new restaurant chain if  user choose  other options the specified  value need to store int  restaurant chain list table
            $restaurantchain = $this->input->post('chain_id');
            if ($restaurantchain != '' && $restaurantchain == 'Other') {
                # insert new purpose
                $restaurant_chain_oth = $this->input->post('hotel_chain_oth');
                if ($restaurant_chain_oth != '') {
                    /* check if already in database if not then add */
                    if ($hcid = $this->restaurant_model->getResturantChain($restaurant_chain_oth)) {
                        $chainId = $hcid;
                    } else {
                        /* add data in database  */
                        $restaurant_chain = array('entity_title' => $restaurant_chain_oth, 'entity_type' => $this->config->item('attribute_hotel_chain'));
                        $chainId = $this->restaurant_model->addResturantChain($restaurant_chain);
                    }
                }
            } else {
                $chainId = $restaurantchain;
            }
            $restaurant_detail = array('rest_address' => $this->input->post('rest_address'), 'post_code' => $this->input->post('post_code'), 'star_rating' => $this->input->post('star_rating'),
                'dorak_rating' => $this->input->post('dorak_rating'), 'chain_id' => $chainId, 'purpose' => $purposeId, 'currency' => $this->input->post('currency'), 'district' => $this->input->post('district'), 'status' => $this->input->post('status'),
            );
            $this->restaurant_model->updateRestaurantInformation($hid, $restaurant_detail);

            /* update contacts details  of the restaurants */
            $nonDelContactids = array();
            $restaurantContactDetails = $this->input->post('contact');
            if (count($restaurantContactDetails) > 0) {
                foreach ($restaurantContactDetails as $restaurantContact) {
                    if (isset($restaurantContact['id']))
                        $hcontactid = $restaurantContact['id'];
                    else
                        $hcontactid = '';
                    if ($hcontactid != "")
                        array_push($nonDelContactids, $hcontactid);
                    $contactData = array('rest_id' => $hid, 'position' => $restaurantContact['position'], 'name' => $restaurantContact['name'], 'email' => $restaurantContact['email'], 'phone' => $restaurantContact['phone'],
                        'extension' => $restaurantContact['extension']);
                    $updatecontactid = $this->restaurant_model->updateResturantContact($contactData, $hcontactid);
                    array_push($nonDelContactids, $updatecontactid);
                }
                $this->restaurant_model->deleteResturantContact($nonDelContactids, $hid);
            }
            # add restaurant complementary  services
            $cmplServices = $this->input->post('complimentary');
            if (empty($cmplServices)) {
                $this->restaurant_model->deleteroomFacility($hid);
            }
            if ($cmplServices != "" && count($cmplServices) > 0) {
                $restaurantCmplServices = array();

                foreach ($cmplServices as $cmplServiceId) {
                    if (isset($cmplServiceId) && $cmplServiceId != "") {
                        $restaurantCmplServices[] = array('rest_id' => $hid, 'cmpl_service_id' => $cmplServiceId);
                    }
                }
                if ($this->restaurant_model->deleteroomFacility($hid)) {
                    $this->restaurant_model->addResturantComplimentaryServices($restaurantCmplServices);
                }
            }
            # add restaurant Facilities
            $facilities = $this->input->post('facilities');
            if (empty($facilities)) {
                $this->restaurant_model->deletehotelFacility($hid);
            }
            if ($facilities != "" && count($facilities) > 0) {
                $restaurantFacilities = array();
                foreach ($facilities as $facility_id) {

                    if (isset($facility_id) && $facility_id != "") {
                        $restaurantFacilities[] = array('rest_id' => $hid, 'facility_id' => $facility_id
                        );
                    }
                }
                if ($this->restaurant_model->deleteresturantFacility($hid)) {
                    $this->restaurant_model->addResturantFacilities($restaurantFacilities);
                }
            }
            $nonbnkIds = array();
            $bnk_details = $this->input->post('bank');
            if (!empty($bnk_details) && count($bnk_details) > 0) {
                foreach ($bnk_details as $bnk) {
                    if (isset($bnk['id'])) {
                        $bankid = $bnk['id'];
                    } else {
                        $bankid = '';
                    }
                    if ($bnk['account_number'] != "" && $bnk['account_name'] != "" && $bnk['bank_name'] != "" && $bnk['bank_address'] != "") {
                        $restaurantBankData = array('rest_id' => $hid, 'iban_code' => $bnk['iban_code'],
                            'account_number' => $bnk['account_number'], 'account_name' => $bnk['account_name'], 'bank_name' => $bnk['bank_name'], 'bank_address' => $bnk['bank_address'], 'branch_name' => $bnk['branch_name'], 'branch_code' => $bnk['branch_code'], 'swift_code' => $bnk['swift_code'], 'bank_ifsc_code' => $bnk['bank_ifsc_code']);
                        $bankNid = $this->restaurant_model->updateResturantbankaccounts($restaurantBankData, $bankid);
                        array_push($nonbnkIds, $bankNid);
                    } else {
                        $this->restaurant_model->deleteResturantbankaccount($bankid, $hid); // delete empty  account details
                    }
                }
                // delete non existing data
                $this->restaurant_model->deleteResturantbankaccounts($nonbnkIds, $hid);
            } else {
                $this->restaurant_model->deleteResturantbankaccounts(false, $hid);
            }
            /* distance by city name */
            $noncityIds = array();
            $distance_from_city_name = $this->input->post('distance_from_city');
            if (!empty($distance_from_city_name)) {
                foreach ($distance_from_city_name as $cityname) {
                    if (isset($cityname['id'])) {
                        $cityid = $cityname['id'];
                    } else {
                        $cityid = '';
                    }
                    if ($cityid != "")
                        array_push($noncityIds, $cityid);
                    if (isset($cityname['name']) && $cityname['name'] != "" && isset($cityname['distance']) && $cityname['distance'] != "") {
                        $citynamedata = array('rest_id' => $hid, 'distance_from' => $cityname['name'], 'distance_type' => '1', 'distance' => $cityname['distance']);
                        $cityupdateid = $this->restaurant_model->updateResturantcity($citynamedata, $cityid);
                        array_push($noncityIds, $cityupdateid);
                    }
                }
                $this->restaurant_model->deleteResturantcity($noncityIds, $hid);
            }

            /* distance by city name close here */

            /* distance by airport name */
            $nonairIds = array();
            $distance_from_airport_name = $this->input->post('distance_from_airport');
            if (!empty($distance_from_airport_name)) {
                foreach ($distance_from_airport_name as $airname) {
                    if (isset($airname['id'])) {
                        $airid = $airname['id'];
                    } else {
                        $airid = '';
                    }
                    if ($airid != "")
                        array_push($nonairIds, $airid);
                    if (isset($airname['name']) && $airname['name'] != "" && isset($airname['distance']) && $airname['distance'] != "") {
                        $airnamedata = array('rest_id' => $hid, 'distance_from' => $airname['name'], 'distance_type' => '2', 'distance' => $airname['distance']);
                        $airupdateid = $this->restaurant_model->updatedistanceForAirport($airnamedata, $airid);
                        array_push($nonairIds, $airupdateid);
                    }
                }
                $this->restaurant_model->deletedistanceForAirport($nonairIds, $hid);
            }
            /* distance by airport name close here */
            /* add new restaurant property type  if  user choose  other options */
            $restaurantPtype = $this->input->post('property_type');
            if (empty($restaurantPtype)) {
                $this->restaurant_model->deleteResturantpropertytype($hid);
            }
            if ($restaurantPtype != '' && count($restaurantPtype) > 0) {
                $addResturantPropertyTypesData = array();
                foreach ($restaurantPtype as $HpType) {
                    if ($HpType != '') {
                        if ($HpType == 'Other') {
                            /* insert new purpose */
                            $property_type_oth = $this->input->post('property_type_oth');
                            if ($property_type_oth != '') {
                                /* check if already in database if not then add */
                                if ($hPid = $this->restaurant_model->getResturantPropertyType($property_type_oth)) {
                                    $restaurantPtypeId = $hPid;
                                } else {
                                    /* add data in database */
                                    $restaurantPtype = array('entity_title' => $property_type_oth, 'entity_type' => $this->config->item('attribute_property_types'));
                                    $restaurantPtypeId = $this->restaurant_model->addResturantPropertyType($restaurantPtype);
                                }
                            }
                        } else {
                            $restaurantPtypeId = $HpType;
                        }

                        $addResturantPropertyTypesData[] = array('rest_id' => $hid, 'property_type_id' => $restaurantPtypeId);
                    }
                }
                if (count($addResturantPropertyTypesData) > 0) {
                    if ($this->restaurant_model->deleteResturantpropertytype($hid)) {
                        $this->restaurant_model->addResturantPropertyTypes($addResturantPropertyTypesData);
                    }
                }
            }
            /* add property close here */
            # add restaurant renovation shedule	
            $nonDelRnvIds = array();
            $renovationSchedules = $this->input->post('rnv_shedule');
            if (count($renovationSchedules) > 0) {
                foreach ($renovationSchedules as $renovationSchedule) {
                    if (isset($renovationSchedule['id']))
                        $scheduleId = $renovationSchedule['id'];
                    else
                        $scheduleId = '';
                    if ($scheduleId != "")
                        array_push($nonDelRnvIds, $scheduleId);
                    if ($renovationSchedule['date_from'] != "" && $renovationSchedule['date_to'] != "" && $renovationSchedule['renovation_type'] != "" && $renovationSchedule['area_effected'] != "") {
                        $restaurantRenovationData = array('rest_id' => $hid, 'date_from' => convert_mysql_format_date($renovationSchedule['date_from']), 'date_to' => convert_mysql_format_date($renovationSchedule['date_to']), 'renovation_type' => $renovationSchedule['renovation_type'], 'area_effected' => $renovationSchedule['area_effected']);
                        $idu = $this->restaurant_model->updateResturantRenovationSchedule($restaurantRenovationData, $scheduleId);
                        array_push($nonDelRnvIds, $idu);
                    }
                }
                // delete non existing data
                $this->restaurant_model->deleteResturantRenovationSchedule($nonDelRnvIds, $hid);
            }
            # add restaurant complementary room
            $cmpli_room_night = $this->input->post('cmpli_room_night');
            $cmpli_room_id = $this->input->post('cmpli_room_id');
            $cmpli_date_from = convert_mysql_format_date($this->input->post('cmpli_date_from'));
            $cmpli_date_to = convert_mysql_format_date($this->input->post('cmpli_date_to'));
            $cmpli_upgrade = $this->input->post('cmpli_upgrade');
            $cmpli_exclude_dates = $this->input->post('cmpli_exclude_date');
            if ($cmpli_room_night != "" && $cmpli_date_from != "") {
                $complimentary_room_data = array('rest_id' => $hid, 'room_night' => $cmpli_room_night, 'start_date' => $cmpli_date_from, 'end_date' => $cmpli_date_to, 'upgrade' => $cmpli_upgrade);

                #update   details
                if ($cmpli_room_id = $this->restaurant_model->updateResturantComlimentaryRoom($complimentary_room_data, $hid, $cmpli_room_id)) {
                    $nonDelCmpExIds = array();
                    if (count($cmpli_exclude_dates) > 0) {
                        foreach ($cmpli_exclude_dates as $cmpli_exclude_date) {
                            if (isset($cmpli_exclude_date['id']))
                                $cmExId = $cmpli_exclude_date['id'];
                            else
                                $cmExId = '';
                            if ($cmExId != "")
                                array_push($nonDelCmpExIds, $cmExId);
                            if ($cmpli_exclude_date['from'] != "" && $cmpli_exclude_date['to'] != "") {
                                $cmpli_excluded_dates = array('cmpl_room_id' => $cmpli_room_id, 'exclude_date_from' => convert_mysql_format_date($cmpli_exclude_date['from']), 'excluded_date_to' => convert_mysql_format_date($cmpli_exclude_date['to']));
                                $ncmExId = $this->restaurant_model->updateResturantComlimentaryRoomExcludedDate($cmpli_excluded_dates, $cmExId);
                                // function return new added id so we do not have to deltete that one
                                array_push($nonDelCmpExIds, $ncmExId);
                            }
                        }
                        $this->restaurant_model->deleteResturantComlimentaryRoomExcludedDate($nonDelCmpExIds, $cmpli_room_id);
                    }
                }
            }

            # update restaurant payment shedule  option				
            $paymentShedules = $this->input->post('payment_plan'); // multi dimensonal array posted from form
            if (!empty($paymentShedules) && count($paymentShedules) > 0) {
                $nonDelPaymentIds = array();
                foreach ($paymentShedules as $paymentShedule) {
                    if (isset($paymentShedule['id']))
                        $pmExId = $paymentShedule['id'];
                    else
                        $pmExId = '';
                    if ($pmExId != "")
                        array_push($nonDelPaymentIds, $pmExId);
                    if ($paymentShedule['payment_value'] != "" && $paymentShedule['payment_option_id'] != "") {
                        $restaurantPaymentShedulesData = array('rest_id' => $hid, 'payment_option_id' => $paymentShedule['payment_option_id'], 'payment_value' => $paymentShedule['payment_value'],
                        );
                        $npmExId = $this->restaurant_model->updateResturantPaymentShedules($restaurantPaymentShedulesData, $pmExId);
                        // function return new added id so we do not have to deltete that one
                        array_push($nonDelPaymentIds, $npmExId);
                    }
                }

                $this->restaurant_model->deleteResturantPaymentShedules($nonDelPaymentIds, $hid);
            }
            # add restaurant payment shedule  option end 
            #add restaurant cancellation information  
            $lowseason_cancelations = $this->input->post('lowseason_canceled'); // multi dimensonal array posted from form
            $highseason_cancelations = $this->input->post('highseason_canceled'); // multi dimensonal array posted from form
            $nonDelCancellationIds = array();
            if (!empty($lowseason_cancelations) && count($lowseason_cancelations) > 0) {
                foreach ($lowseason_cancelations as $lowseason_cancel) {
                    if (isset($lowseason_cancel['id']))
                        $cancellationExId = $lowseason_cancel['id'];
                    else
                        $cancellationExId = '';
                    if ($cancellationExId != "")
                        array_push($nonDelCancellationIds, $cancellationExId);
                    if ($lowseason_cancel['before'] != "" && $lowseason_cancel['payment_request'] != "") {
                        $restaurantCancellationData = array('rest_id' => $hid, 'cancelled_before' => $lowseason_cancel['before'], 'payment_request' => $lowseason_cancel['payment_request'], 'seasion' => 'low'
                        );
                        $nCanclExId = $this->restaurant_model->updateResturantCancellation($restaurantCancellationData, $cancellationExId);
                        // function return new added id so we do not have to deltete that one
                        array_push($nonDelCancellationIds, $nCanclExId);
                    }
                }
            }
            if (!empty($highseason_cancelations) && count($highseason_cancelations) > 0) {
                foreach ($highseason_cancelations as $highseason_cancel) {
                    if (isset($highseason_cancel['id']))
                        $cancellationExId = $highseason_cancel['id'];
                    else
                        $cancellationExId = '';
                    if ($cancellationExId != "")
                        array_push($nonDelCancellationIds, $cancellationExId);

                    if ($highseason_cancel['before'] != "" && $highseason_cancel['payment_request'] != "") {
                        $restaurantCancellationData = array('rest_id' => $hid, 'cancelled_before' => $highseason_cancel['before'], 'payment_request' => $highseason_cancel['payment_request'], 'seasion' => 'high');
                        $nCanclExId = $this->restaurant_model->updateResturantCancellation($restaurantCancellationData, $cancellationExId);
                        // function return new added id so we do not have to deltete that one
                        array_push($nonDelCancellationIds, $nCanclExId);
                    }
                }
            }
            $this->restaurant_model->deleteResturantCancellation($nonDelCancellationIds, $hid);
            #add restaurant cancellation information end 
            $aplicable_child_age_group = $this->input->post('aplicable_child_age_group');
            if ($aplicable_child_age_group != "") {
                $uchData = array('child_age_group_id' => $aplicable_child_age_group);
                $this->restaurant_model->updateResturantDetails($hid, $uchData);
            }
            #add restaurant contract information
            $contract_start_date = $this->input->post('contract_start_date');
            $contract_end_date = $this->input->post('contract_end_date');
            $contract_signed_by = $this->input->post('contract_signed_by');
            $contract_file_name = '';
            $responseArray = array();
            if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name']) && $contract_end_date == "" && $contract_signed_by == "") {
                $data['message'] = '<li> Restaurant Contract Details Not Added,Restaurant Contract Details Fields are missing! </li>';
            } elseif ($contract_start_date != "" && $contract_end_date != "" && $contract_signed_by != "") {
                if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name'])) {

                    $config['upload_path'] = $this->contractFileDir;
                    $config['allowed_types'] = 'pdf|jpg|jpeg';
                    $config['max_size'] = '0'; //2 mb , 0 for unlimited
                    $config['max_width'] = '0'; // unlimited
                    $config['max_height'] = '0';
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('contract_file')) {
                        $error = $this->upload->display_errors();
                        $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the restaurant details!</li>';
                    } else {
                        $uploadedFileDetail = $this->upload->data();
                        $contract_file_name = $uploadedFileDetail['file_name'];
                    }
                }
                $restaurantContractData = array('rest_id' => $hid, 'start_date' => convert_mysql_format_date($contract_start_date), 'end_date' => convert_mysql_format_date($contract_end_date), 'signed_by' => $contract_signed_by, 'contract_file' => $contract_file_name);
                if ($this->restaurant_model->addResturantContract($restaurantContractData)) {
                    if ($contract_file_name != "") {
                        $data['message'] = '<li>Restaurant Contract Details Added</li>';
                    } else {
                        $data['message'] = '<li>Restaurant Contract Details</li> ';
                    }
                }

                $explode = explode('.', $contract_file_name);
                $file_extention = !empty($explode[1]) ? $explode[1] : '';
                $name_with_break = strlen($contract_file_name) > 16 ? substr($contract_file_name, 0, 20) . "..." . $file_extention : $contract_file_name;

                $responseArray['contract_file'] = $name_with_break;
                $responseArray['contract_name'] = $contract_file_name;
                $responseArray['start_date'] = $contract_start_date;
                $responseArray['end_date'] = $contract_end_date;
                $responseArray['signed_by'] = $contract_signed_by;
                $responseArray['success'] = 'true';

                echo json_encode($responseArray);
                die;
            }


            # edit restaurant pricing section					
            $restaurantPricingRecords = $this->input->post('pricing'); // multi dimensonal array posted from form
            $restaurantRoomsPricingData = array(); // for table rest_rooms_pricing
            if (!empty($restaurantPricingRecords) && count($restaurantPricingRecords) > 0) {
                $nonDelPricingRecordIds = array();
                foreach ($restaurantPricingRecords as $restaurantPricingRecord) {
                    //echo $restaurantPricingRecord['pricing_id']."<br>";
                    // prepare  records  which will not deleted other record whicg are not submitted will be deleted
                    if (isset($restaurantPricingRecord['pricing_id']))
                        $prExId = $restaurantPricingRecord['pricing_id'];
                    else
                        $prExId = '';

                    $restaurantRroomsPricingDetailsData = array(); // for rest_rooms_pricing_details table
                    $restaurantRoomsPricingComplimentaryData = array(); // for table rest_rooms_pricing_complimentary

                    if (isset($restaurantPricingRecord['room_type']) && $restaurantPricingRecord['room_type'] != "" && isset($restaurantPricingRecord['invenory']) && $restaurantPricingRecord['invenory'] != "") {
                        $restaurantRoomsPricingData = array('rest_id' => $hid, 'room_type' => $restaurantPricingRecord['room_type'], 'inclusions' => $restaurantPricingRecord['pricing_inclusions'], 'curency_code' => $restaurantPricingRecord['currency'], 'max_adult' => $restaurantPricingRecord['max_adult'], 'max_child' => $restaurantPricingRecord['max_child'], 'inventory' => $restaurantPricingRecord['invenory'], 'period_from' => convert_mysql_format_date($restaurantPricingRecord['period_from_date']), 'period_to' => convert_mysql_format_date($restaurantPricingRecord['period_to_date']));
                        $pricing_inserted_id = $this->restaurant_model->updateResturantRoomsPricingData($restaurantRoomsPricingData, $prExId);
                        if ($pricing_inserted_id) {
                            array_push($nonDelPricingRecordIds, $pricing_inserted_id); // add id to non delete list			
                            // if restaurant pricing record added in database // add  room pricing facilities
                            if (isset($restaurantPricingRecord['room_facilities'])) {
                                $pricingRoomFacilities = $restaurantPricingRecord['room_facilities'];
                                if (!empty($pricingRoomFacilities) && count($pricingRoomFacilities) > 0) {
                                    foreach ($pricingRoomFacilities as $roomFacility) {
                                        if ($roomFacility != "") {
                                            $restaurantRoomsPricingComplimentaryData[] = array('pricing_id' => $pricing_inserted_id, 'cmpl_service_id' => $roomFacility);
                                        }
                                    }
                                    if (!empty($restaurantRoomsPricingComplimentaryData) && count($restaurantRoomsPricingComplimentaryData) > 0) {
                                        $this->restaurant_model->addResturantRoomPricingComplimentary($restaurantRoomsPricingComplimentaryData, $pricing_inserted_id);
                                    }
                                }
                            }// add  room pricing facilities end
                            // add  room pricing details
                            if (isset($restaurantPricingRecord['price_detail'])) {
                                $roomPriceingDetails = $restaurantPricingRecord['price_detail'];
                                if ($pricing_inserted_id == '')
                                    $pricing_inserted_id = $prExId;
                                if (!empty($roomPriceingDetails) && count($roomPriceingDetails) > 0) {
                                    $nonDelPricingDetailRecordIds = array();
                                    // store  ids which need to keep after update
                                    foreach ($roomPriceingDetails as $roomPriceingDetail) {
                                        if (isset($roomPriceingDetail['pricing_detId']))
                                            $prDetExId = $roomPriceingDetail['pricing_detId'];
                                        else
                                            $prDetExId = '';
                                        if ($prDetExId != "")
                                            array_push($nonDelPricingDetailRecordIds, $prDetExId);
                                        if ($roomPriceingDetail['market_id'] != "") {
                                            $restaurantRroomsPricingDetailsData = array('pricing_id' => $pricing_inserted_id, 'market_id' => $roomPriceingDetail['market_id'], 'double_price' => $roomPriceingDetail['double_price'], 'triple_price' => $roomPriceingDetail['triple_price'], 'quad_price' => $roomPriceingDetail['quad_price'], 'breakfast_price' => $roomPriceingDetail['breakfast_price'], 'half_board_price' => $roomPriceingDetail['half_board_price'], 'all_incusive_adult_price' => $roomPriceingDetail['all_incusive'], 'extra_adult_price' => $roomPriceingDetail['extra_adult'], 'extra_child_price' => $roomPriceingDetail['extra_child'], 'extra_bed_price' => $roomPriceingDetail['extra_bed']);
                                            $npricingDetId = $this->restaurant_model->updateResturantRroomsPricingDetails($restaurantRroomsPricingDetailsData, $prDetExId, $pricing_inserted_id);
                                            array_push($nonDelPricingDetailRecordIds, $npricingDetId);
                                        }
                                    }
                                    // delete non  existing  pricing details
                                    if (count($nonDelPricingDetailRecordIds) > 0)
                                        $this->restaurant_model->deleteResturantResturantRroomsPricingDetails($nonDelPricingDetailRecordIds, $pricing_inserted_id);
                                }// end of pricing records if exists	
                            }
                        }
                        // add  room pricing details end
                    }// end if of if   pricing data added							
                    // delete other not existing procing  of the restaurant
                }// end of pricing records for	

                if (count($nonDelPricingRecordIds) > 0)
                    $this->restaurant_model->deleteResturantRoomsPricingData($nonDelPricingRecordIds, $hid);
            }// end of pricing records if exists	
            # add restaurant pricing section end
            $identity = $this->input->post('identity');
            $dbidentity = $this->restaurant_model->get_dbidentity($identity);
            if ($dbidentity && $identity == ($dbidentity->identifier)) {
                $data_identity = array('rest_id' => $hid);
                $this->restaurant_model->update_images($identity, $data_identity);
            }
            echo $return = 1;
            die;

            /*  zip folder code close   */
            $data['message'] = '<li>Restaurant information updated</li>';
            $flashdata = array(
                'flashdata' => $data['message'],
                'message_type' => 'sucess'
            );
            $this->session->set_userdata($flashdata);
        }
    }

    function menu_market($hid = null) {
        $this->load->library('upload');
        $market_id = $this->input->post('market_id');
        $market_file_name = '';
        $ext = '';
        $ext = pathinfo($_FILES['market_files']['name'], PATHINFO_EXTENSION);
        $responseArray = array();
        if (isset($_FILES['market_files']) && is_uploaded_file($_FILES['market_files']['tmp_name'])) {
            $config['upload_path'] = $this->marketFileDir;
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size'] = '0'; //2 mb , 0 for unlimited
            $config['max_width'] = '0'; // unlimited
            $config['max_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('market_files')) {
                $error = $this->upload->display_errors();
                $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the restaurant details!</li>';
            } else {
                $uploadedFileDetail = $this->upload->data('');
                $market_file_name = $uploadedFileDetail['file_name'];
            }
        }

        if (!empty($_FILES['market_files']) && !empty($_FILES['market_files']['tmp_name'])) {
            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'pdf') {
                $menuMarketData = array('market_id' => $market_id, 'rest_id' => $hid, 'files' => $market_file_name);
                if ($this->restaurant_model->addMarketFiles($menuMarketData)) {
                    if ($market_file_name != "") {
                        $data['message'] = '<li>Restaurant Market Details Added</li>';
                    } else {
                        $data['message'] = '<li>Restaurant Market Details</li> ';
                    }
                }
                $market_list = $this->restaurant_model->MarketFiles($hid);
                echo $market_list;
                exit;
            } else {
                $market_list = $this->restaurant_model->MarketFiles($hid);
                echo $market_list;
                exit;
            }
        } else {
            $market_list = $this->restaurant_model->MarketFiles($hid);
            echo $market_list;
            exit;
        }
    }

    function delete_market_images() {
//        $responsArray = array();
        $id = $market_id = $this->input->post('market_id');
        $data = $this->restaurant_model->delete_market_files($id);
        echo $data;
        die;
//        if($data){
//              $responsArray['success'] = true;
//        }
//        else{
//             $responsArray['success'] = false;
//        }
//        echo json_encode($responsArray);die;
    }

    function list_market($res_id) {
        if (!empty($res_id)) {
            $market_list = $this->restaurant_model->MarketFiles($res_id);
        }
        echo $market_list;
        exit;
    }

    function commonmenu() {
        if (!empty($_FILES)) {
            $commonmenuid = $this->input->post('commonmenuid');
            $resturantids = $this->input->post('resturantid');
            if (!empty($resturantids)) {
                $resturantid = $resturantids;
            } else {
                $resturantid = null;
            }
            //echo '<pre>'; print_r($_FILES["file"]["error"]); die;
            $ds = DIRECTORY_SEPARATOR;
            foreach ($_FILES["file"]["error"] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $tempFile = $_FILES['file']['tmp_name'][$key];
                    $name = $_FILES["file"]["name"][$key];
                    $file_extension = end((explode(".", $name))); # extra () to prevent notice
                    $targetPath = FCPATH . $this->resturantCommonfileDir . $ds;  //4
                    $targetFile = $targetPath . $name;
                    move_uploaded_file($tempFile, $targetFile);
                    $common_menu_data = array('rest_id' => $resturantid, 'market_id' => $commonmenuid, 'files' => $name);
                    $status = $this->restaurant_model->insert_common_menu($common_menu_data);
                }
            }
            $commonfiles = $this->restaurant_model->get_common_menu_files($resturantid);
            echo '<pre>';
            print_r($get_common_menu_files);
            die;
            //echo json_encode($commonfiles); 

            /* echo '<div class = "common-menu">';

              foreach($commonfiles as $cfile){
              ?><div id="common<?php echo $cfile->id; ?>">
              <img src="<?php echo base_url(); ?>common_menu_files/<?php echo $cfile->files; ?>" alt="<?php echo $cfile->files; ?>" height="50" width="50">
              <a  onclick='return deletecommonfiles(<?php echo $cfile->id; ?>,<?php echo $resturantids; ?>)'><img alt="delete" src="<?php echo base_url(); ?>assets/themes/default/images/gallery-close.png"> </a></div><?php
              }
              echo '</div>';
              die; */
        }
    }

    function deletecommonfiledata() {
        $cid = $this->input->post('cid');
        $restid = $this->input->post('restid');
        if ($this->restaurant_model->delete_common_menu($restid, $cid)) {
            return true;
        } else {
            return false;
        }
    }

    function deletemodalimg() {
        $imgid = $this->input->post('id');
        $marketid = $this->input->post('marketid');
        $ds = DIRECTORY_SEPARATOR;
        $targetPath = FCPATH . $this->resturantCommonfileDir . $ds;  //4
        $targetFile = $targetPath . $imgid;
        try {
            unlink($targetFile);
        } catch (Exception $e) {
            log_message('gallery image deletion ', $e->getMessage());
        }
        $this->restaurant_model->delete_commonimg($imgid, $marketid);
    }

}
