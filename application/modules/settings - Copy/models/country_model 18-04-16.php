<?php
/*
 * @data 15-mar-2016
 * @author tis
 * @functions:
	addCurrency()
	getCurrency()
	getCurrencyList()
	delCurrency()
	deleteCurencyCodesData()
	updateCurrency()
	importCurencyData()
	addCurencyData()
	addCountry()
	getCountry()
	checkCountryExists()
	checkCountryCodeExists()
	getCountryCode()
	getCountryName()
	getCountriesList()
	delCountry()
	updateCountry()
	addCity()
	getCity()
	checkCityExists()
	checkCityCodeExists()
	getCityByCode()
	getCitiesList()
	delCity()
	delCities()
	updateCity()
	getCityName()
	addDistrict()
	getDistrict()
	checkDistrictExists()
	getCityDistricts()
	getDistrictList()
	delDistrict()
	delCityDistricts()
	delCountryCityDistricts()
	updateDistrict()
	addRole()
	getRole()
	getRolesList()
	delRole()
	updateRole()
	addCompany()
	addCompanyMarkets()
	getCompanyMarkets()
	getCompany()
	getCompanyDetails()
	getCompanyLogo()
	getCompanyList()
	delCompany()
	delCompanyMarkets()
	updateCompany()
	delLoction_point()
	updateLoction()
	checkLoctionExists()
	addLoctionPoint()
	get_location_point()

 * @description :
 */

class Country_model extends AdminModel {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }     
	/**
     * @method addCurrency() method used to insert currency
	 * @param array() $data hold currency in the form of an array.
	 * @todo this method used to insert currency in currencies table
     * @return id|false
     */
	function addCurrency($data){
        if($this->db->insert('currencies', $data)) {
			return $this->db->insert_id();
		}
		return false;
    }
    /**
     * @method getCurrency() method used to get currency from currencies table using code, and currency usd value
     * @param string $code currency code.
     * @param string $usval currency usd value
     * @param int $eid currency id
	 * @todo this method used to get currency from currencies table. 
     * @return id|false
     */
	function getCurrency($code,$usval='',$eid='') {
		$this->db->select('id');
		$this->db->where("code",$code); 
		if($usval!='')
		$this->db->where("currency_usd_value",$usval); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0) {
			$row = $query->row(); 
			return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getCurrencyList() method used to get currency list
	 * @todo method used for get all currencies
     * @return will return currencies in the form of an array
     */
	function getCurrencyList() {
		$this->db->select('id,code,currency_usd_value'); 
		$this->db->order_by('code');
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0)	{
			return $query->result(); 
		}else{
			return false;
		}
	}
	/**
     * @method delCurrency() method used to delete currency using currency id
	 * @param string $dId currency id
	 * @todo method used for delete currency
     * @return will return true or false
     */
	function delCurrency($dId) {
		$this->db->where('id',$dId);
		return $this->db->delete('currencies');	
	}
/**
     * @method delCurrency() method used to delete currency using currency id
	 * @todo method used for delete currency
     * @return will return true or false
     */
	function deleteCurencyCodesData($delCodesArray)	{
		$this->db->where_in('code', $delCodesArray);
		return $this->db->delete('currencies');		
	}
/**
     * @method updateCurrency() method used to update currency using currency id
     * @param int $dbId  currency id
     * @param array() $newdata hold currency in the form of an array
	 * @todo this method used to update currency in currencies table
     */
	function updateCurrency($dbId,$newdata)	{
		$this->db->where('id', $dbId);
		$this->db->update('currencies', $newdata); 
	}	
	/**
     * @method importCurencyData() method used to import currency
	 * @param array() $data hold currency in the form of an array.
	 * @todo this method used to delete old currencies and insert new currencies in  currencies table
     * @return true|false
     */
	function importCurencyData($data) {
		 // clear old data
		 $this->db->truncate('currencies'); 
		 // import new data
		return $this->db->insert_batch('currencies',$data);
	}	
	/**
     * @method addCurencyData() method used to add currency
	 * @param array() $data hold currency in the form of an array.
	 * @todo this method used to insert currencies in  currencies table
     * @return true|false
     */
	function addCurencyData($data) {
      return $this->db->insert_batch('currencies',$data);
	}	
/**
     * @method addCountry() method used to add country
	 * @param array() $data hold country in the form of an array.
	 * @todo this method used to insert countries in  countries table
     * @return true|false
     */
	function addCountry($data) {
		if($this->db->insert('countries', $data)) {
			return $this->db->insert_id();
		}
		return false;
	}
    /**
     * @method getCountry() method used to get country from countries table using country name
     * @param string $cname country name.
	 * @todo this method used to get country from countries table. 
     * @return id|false
     */
	function getCountry($cname)	{
		$this->db->select('id');
		$this->db->where("country_name",$cname); 
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}
	
	/**
     * @method checkCountryExists() method used to check country exist or not using country name and country id
     * @param string $name country name.
	 * @todo this method used to check country exist or not using country name and country id in countries table. 
     * @return id|false
     */
	function checkCountryExists($Id='',$name) {
		$this->db->select('id');
		if($Id!='')
		$this->db->where("id !=",$Id); 
		$this->db->where("country_name",$name);	
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}	
	/**
     * @method checkCountryCodeExists() method used to check country exist or not using country code and country id
     * @param string $ccode country code.
     * @param int $Id country id.
	 * @todo this method used to check country exist or not using country code and country id in countries table. 
     * @return id|false
     */
	function checkCountryCodeExists($Id='',$ccode) {
		$this->db->select('id');
		if($Id!='')
		$this->db->where("id !=",$Id); 
		$this->db->where("country_code",$ccode);
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}	
	 /**
     * @method getCountryCode() method used to get country code from countries table using country id
     * @param int $cid country id.
	 * @todo this method used to get country code from countries table using country id from countries table. 
     * @return country code|false
     */
	function getCountryCode($cid) {
		$this->db->select('country_code');
		$this->db->where("id",$cid); 
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->country_code;
		}else{
			return false;
		}
		return false;
	}
	/**
     * @method getCountryName() method used to get country name from countries table using country id
     * @param int $cid country id.
	 * @todo this method used to get country name from countries table using country id from countries table. 
     * @return country name|false
     */
	function getCountryName($cid) {
		$this->db->select('country_name');
		$this->db->where("id",$cid); 
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->country_name;
		} else{
			return false;
		}
		return false;
	}
	/**
     * @method getCountriesList() method used to get country list from countries table
	 * @todo this method used to get country list from countries table 
     * @return country name and country code in the form of an array|false
     */
	function getCountriesList()	{
			$this->db->select('id,country_code as code,country_name'); 
			$this->db->order_by('country_name','ASC');
			$query = $this->db->get('countries');
			if ($query->num_rows() > 0)	{
				return $query->result(); 
			}else{
				return false;
			}
	}
	/**
     * @method delCountry() method used to delete country from countries table
	 * @param int $dId country id.
	 * @todo this method used to delete country from countries table 
     * @return true|false
     */
	function delCountry($dId) {
		$this->delCities($dId);// delete cities of the selected cuntry
		$this->db->where('id',$dId);
		return $this->db->delete('countries');	
	}
	/**
     * @method updateCountry() method used to update country from countries table
	 * @param int $dbId country id.
	 * @param array() $newdata hold country in the form of an array.
	 * @todo this method used to delete country from countries table 
     */
	function updateCountry($dbId,$newdata) {
		$this->db->where('id', $dbId);
		$this->db->update('countries', $newdata); 
	}	
/**
     * @method addCity() method used to add city
	 * @param array() $data hold city in the form of an array.
	 * @todo this method used to insert country city in  countries table
     * @return id|false
     */
	function addCity($data)	{
		if($this->db->insert('country_cities', $data))	{
			return $this->db->insert_id();
		}
		return false;
	}
    /**
     * @method getCity() method used to get country city name from country_cities table using city name
     * @param string $cname city name.
	 * @todo this method used to get city id from country_cities table using city name. 
     * @return id|false
     */
	function getCity($cname)   {
		$this->db->select('id');
		$this->db->where("city_name",$cname); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}
	
	/**
     * @method checkCityExists() method used to check city exist or not using city name, city id and country id.
     * @param string $name city name.
     * @param int $cid city id.
     * @param int $cuntryId country id.
	 * @todo this method used to check city exist or not using city name, city id and country id. 
     * @return id|false
     */
	function checkCityExists($name,$cid='',$cuntryId='') {
		$this->db->select('id');
		$this->db->where("city_name",$name); 
		if($cid!='')
		$this->db->where("id !=",$cid); 
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId);	
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	/**
     * @method checkCityCodeExists() method used to check city exist or not using city code, city id and country id.
     * @param string $ccode city code.
     * @param int $cid city id.
     * @param int $cuntryId country id.
	 * @todo this method used to check city exist or not using city code, city id and country id from country_cities. 
     * @return id|false
     */
	function checkCityCodeExists($ccode,$cid='',$cuntryId='')  {
		$this->db->select('id');
		if($cid!='')
		$this->db->where("id !=",$cid); 
	
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId); 
	
		$this->db->where("city_code",$ccode); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	/**
     * @method getCityByCode() method used to get city id using city code.
     * @param string $ccode city code.
	 * @todo this method used to get city id from country_cities. 
     * @return id|false
     */
	function getCityByCode($ccode){
		$this->db->select('id');
		$this->db->where("city_code",$ccode); 
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)
		{
		$row = $query->row(); 
		return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
	/**
     * @method getCitiesList() method used to get city id, city name and city code using country id
     * @param int $cuntryId country id.
	 * @todo this method used to get city id, city name and city code using country id from country_cities. 
     * @return city name and city code in the form of an array|false
     */
	function getCitiesList($cuntryId) {
		$this->db->select('id,city_name,city_code'); 
		$this->db->order_by('city_name','ASC');
		$this->db->where("country_id",$cuntryId); 
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)
		{
		return $query->result(); 
		}
		else{
			return false;
		}
	}
	/**
     * @method delCity() method used to delete country city using id
     * @param int $dId city id.
	 * @todo this method used to delete city city id from country_cities. 
     * @return true|false
     */
	function delCity($dId) {
		$this->delCityDistricts($dId);// delCityDistricts method used to delete district
		$this->db->where('id',$dId);
		return $this->db->delete('country_cities');	
	}
	/**
     * @method delCities() method used to delete cities country id
     * @param int $cuntryId country id.
	 * @todo this method used to delete cities using country id from country_cities. 
     * @return true|false
     */
	function delCities($cuntryId) {
		$this->delCountryCityDistricts($cuntryId);
		$this->db->where('country_id',$cuntryId);
		return $this->db->delete('country_cities');	
	}
/**
     * @method updateCity() method used to city using city id
     * @param int $dbId country id.
     * @param array $newdata hold  country id.
	 * @todo this method used to update city name using city id from country_cities. 
     * @return true|false
     */
	function updateCity($dbId,$newdata) {
		$this->db->where('id', $dbId);
		$this->db->update('country_cities', $newdata); 
	}

	/**
     * @method getCityName() method used to get city name using city id
     * @param int $cId city id.
	 * @todo this method used to get city name using city id from country_cities. 
     * @return city name in the form of an array|false
     */	
	function getCityName($cId) {
		$this->db->select('city_name');
		$this->db->where("id",$cId); 
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)	{
		$row = $query->row(); 
		return $row->city_name;
		} else{
			return false;
		}
		return false;
	}
/**
     * @method addDistrict() method used to add city name
     * @param array $data hold city name in the form of an array.
	 * @todo this method used to get city name using city id in city_districts. 
     * @return id|false
     */	
	function addDistrict($data)	{
		if($this->db->insert('city_districts', $data))	{
			return $this->db->insert_id();
		}
		return false;
	}
    /**
     * @method getDistrict() method used to get district id
     * @param string $dname hold district name
     * @param int $cid hold city id
	 * @todo this method used to get district id from city_districts table. 
     * @return id|false
     */
	function getDistrict($dname,$cid='') {
		$this->db->select('id');
		$this->db->where("district_name",$dname); 
		if($cid!='')
		$this->db->where("city_id",$cid); 
		$query = $this->db->get('city_districts');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		} else{
			return false;
		}
		return false;
	}
	
	/**
     * @method checkDistrictExists() method used to check  district name exist or not
     * @param string $name hold district name
     * @param int $cid hold city id
     * @param int $did hold district id
	 * @todo this method used to check  district name exist or not in city_districts table. 
     * @return id|false
     */
	function checkDistrictExists($name,$cuntryId='',$cid='',$did='') {
		$this->db->select('id');
		$this->db->where("district_name",$name);
		if($cid!='')
		$this->db->where("city_id",$cid); 
		if($did!='')
		$this->db->where("id !=",$did); 
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId);	
		$query = $this->db->get('city_districts');
		if ($query->num_rows() > 0)
		{
		$row = $query->row(); 
		return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	/**
     * @method getCityDistricts() method used to get city district name
     * @param int $cityId hold city id
     * @param int $did hold district id
	 * @todo this method used to get city district name city_districts table. 
     * @return will return id and district name in the form of an array|false
     */
	function getCityDistricts($cityId){
			$this->db->select('id,district_name');		
			$this->db->where("city_id",$cityId); 
			$this->db->order_by('district_name','ASC');
			$query = $this->db->get('city_districts');
			if ($query->num_rows() > 0){
			return $query->result(); 
			}
			else{
				return false;
			}
	}
/**
     * @method getDistrictList() method used to get district list using country id and city id
     * @param int $cuntryId hold country id
     * @param int $cityId hold city id
	 * @todo this method used to get district list from city_districts table. 
     * @return will return id and district name in the form of an array|false
     */	
	function getDistrictList($cuntryId,$cityId){
		$this->db->select('id,district_name');		
		$this->db->where("country_id",$cuntryId); 
		$this->db->where("city_id",$cityId); 
		$this->db->order_by('district_name','ASC');
		$query = $this->db->get('city_districts');
		if ($query->num_rows() > 0){
		return $query->result(); 
		}
		else{
			return false;
		}
	}
/**
     * @method delDistrict() method used to delete district using district id
     * @param int $dId hold district id
	 * @todo this method used to delete district using district id from city_districts table. 
     * @return true|false
     */	
	function delDistrict($dId){
		$this->db->where('id',$dId);
		return $this->db->delete('city_districts');	
	}
/**
     * @method delCityDistricts() method used to delete district using city id
     * @param int $city_id hold city id
	 * @todo this method used to delete district using city id from city_districts table. 
     * @return true|false
     */	
	function delCityDistricts($city_id)	{
		$this->db->where('city_id',$city_id);
		return $this->db->delete('city_districts');	
	}
	/**
     * @method delCountryCityDistricts() method used to delete district using country id
     * @param int $country_id hold country id
	 * @todo this method used to delete district using country id from city_districts table. 
     * @return true|false
     */	
	function delCountryCityDistricts($country_id){
		$this->db->where('country_id',$country_id);
		return $this->db->delete('city_districts');	
	}
/**
     * @method updateDistrict() method used to update district using district id
     * @param int $dbId hold district id
     * @param array() $newdata hold district name in the form of an array.
	 * @todo this method used to update district using district id from city_districts table. 
     */	
	function updateDistrict($dbId,$newdata){
		$this->db->where('id', $dbId);
		$this->db->update('city_districts', $newdata); 
	}	
	/**
     * @method addRole() method used to add role in to database
     * @param array() $data hold role name in the form of an array.
	 * @todo this method used to insert role into role table table. 
	 * @return id|false
     */	
	function addRole($data)	{
		if($this->db->insert('role', $data)) {
			return $this->db->insert_id();
		}
		return false;
	}
	/**
     * @method getRole() method used to get role from database
     * @param string $cname hold role name .
	 * @todo this method used to get role from role table. 
	 * @return id|false
     */		
	function getRole($cname,$eid=""){
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('role');
        if ($query->num_rows() > 0){
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getRolesList() method used to get role title,id and access level id from database
	 * @todo this  method used to get role title,id and access level id from role table. 
	 * @return title and access level id in the form of an array|false
     */	
	function getRolesList(){
		$this->db->select('id,title,access_level_id'); 
		$this->db->order_by('title');
        $query = $this->db->get('role');
        if ($query->num_rows() > 0){
        return $query->result(); 
		}
		else{
			return false;
		}
	}
/**
     * @method delRole() method used to delete role
	 * @todo this  method used to delete role from role table. 
	 * @return true|false
     */
	function delRole($dId){
		$this->db->where('id',$dId);
		return $this->db->delete('role');	
	}
	/**
     * @method updateRole() method used to update role
	 * @param int $dbId hold role id .
	 * @param array() $newdata hold role in the form of an array.
	 * @todo this  method used to update role inside role table. 
	 * @return true|false
     */
	function updateRole($dbId,$newdata){
		$this->db->where('id', $dbId);
		$this->db->update('role', $newdata); 
	}		
	/**
     * @method addCompany() method used to insert company
	 * @param array() $data hold company name in the form of an array.
	 * @todo this  method used to insert company into companies table. 
	 * @return id|false
     */
	function addCompany($data){
        if($this->db->insert('companies', $data)){
			return $this->db->insert_id();
		}
		return false;
    }
	/**
     * @method addCompanyMarkets() method used to insert company_markets table
	 * @param array() $data hold company market name in the form of an array.
	 * @todo this  method used to insert company market name into company_markets table. 
	 * @return true|false
     */
	function addCompanyMarkets($data) {
       if($this->db->insert_batch('company_markets', $data)){
		return true;
	  }
		return false;
    }
/**
     * @method getCompanyMarkets() method used to get market id company_markets table
	 * @param int $cId hold company id
	 * @todo this  method used to get company market id from company_markets table. 
	 * @return market ids in the form of an array |false
     */	
	function getCompanyMarkets($cId) {
		$this->db->select('market_id');
		$this->db->where("company_id",$cId); 
        $query = $this->db->get('company_markets');
        if ($query->num_rows() > 0){
			return $query->result(); 
		} else{
			return false;
		}
	}
    /**
     * @method getCompany() method used to get id from companies table
	 * @param string $cname hold company name
	 * @todo this  method used to get company name from companies table. 
	 * @return company ids in the form of an array |false
     */
	function getCompany($cname)  {
		$this->db->select('id');
		$this->db->where("company_name",$cname); 
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}
	 /**
     * @method getCompanyDetails() method used to get company details from companies table
	 * @param string $cid hold company id
	 * @todo this  method used to get company id from companies table. 
	 * @return companies detail in the form of an array |false
     */
	function getCompanyDetails($cid) {
		$this->db->select('id,company_name,address,country_code,city,head,company_logo,primary_company_id,status');
		$this->db->where("id",$cid); 
		$query = $this->db->get('companies');
		if ($query->num_rows() > 0){
		$row = $query->row(); 
		return $row;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getCompanyLogo() method used to get company logo from companies table
	 * @param string $cid hold company id
	 * @todo this  method used to get company id from companies table. 
	 * @return company logo in the form of an array |false
     */	
	function getCompanyLogo($cid){
		$this->db->select('company_logo');
		$this->db->where("id",$cid); 
		$query = $this->db->get('companies');
		if ($query->num_rows() > 0){
		$row = $query->row(); 
		return $row->company_logo;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getCompanyList() method used to get company name from companies table
	 * @todo this  method used to get company name from companies table. 
	 * @return company logo in the form of an array |false
     */
	function getCompanyList() {
		$this->db->select('id,company_name as title'); 
		$this->db->order_by('company_name');
		$query = $this->db->get('companies');
		if ($query->num_rows() > 0)
		{
		return $query->result(); 
		}
		else{
			return false;
		}
	}
	/**
     * @method delCompany() method used to delete company from companies table
	 * @param $did company
	 * @todo this  method used to delete company from companies table. 
	 * @return true|false
     */
	function delCompany($dId){
		$this->delCompanyMarkets($dId);
		$this->db->where('id',$dId);
		return $this->db->delete('companies');	
	}
	/**
     * @method delCompanyMarkets() method used to delete company market from company_markets table
	 * @param $cmpId company id
	 * @todo this  method used to delete company market from company_markets table
	 * @return true|false
     */
	function delCompanyMarkets($cmpId){
		$this->db->where('company_id',$cmpId);
		return $this->db->delete('company_markets');	
	}
	/**
     * @method updateCompany() method used to update company from companies table
	 * @param $dbId company id
	 * @param array $newdata hold company name in the form of an array
	 * @todo this  method used to update company market from company_markets table
	 * @return true|false
     */
	function updateCompany($dbId,$newdata){
		$this->db->where('id', $dbId);
		return $this->db->update('companies', $newdata); 
	}	
  	/**
     * @method delLoction_point() method used to delete hotel location from hotel_location_points table
	 * @param $dId company id
	 * @todo this method used to delete hotel location from hotel_location_points table
	 * @return true|false
     */
    function delLoction_point($dId) {
        $this->db->where('id', $dId);
        return $this->db->delete('hotel_location_points');
    }
	/**
     * @method updateLoction() method used to update hotel location from hotel_location_points table
	 * @param $dbId company id
	 * @param array() $newdata hold hotel location in the form of an array
	 * @todo this method used to update hotel location from hotel_location_points table
     */
    function updateLoction($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('hotel_location_points', $newdata);
    }
	/**
     * @method checkLoctionExists() method used to check  hotel location exist or not from hotel_location_points table
	 * @param  string $name location name
	 * @param  int $cuntryId country id
	 * @param  int $cid city id
	 * @param  int $did district id
	 * @todo this method used to check  hotel location exist or not from hotel_location_points table
	 * @return id|false
     */
    function checkLoctionExists($name, $cuntryId = '', $cid = '', $did = '') {
        $this->db->select('id');
        $this->db->where("point_name", $name);
        if ($cid != '')
            $this->db->where("city_id", $cid);
        if ($did != '')
            $this->db->where("id !=", $did);
        if ($cuntryId != '')
            $this->db->where("country_id", $cuntryId);
        $query = $this->db->get('hotel_location_points');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }
	/**
     * @method addLoctionPoint() method used to insert location point into hotel_location_points table
	 * @param  array() $data hold location name in the form of an array.
	 * @todo this  method used to insert location point hotel_location_points table
	 * @return id|false
     */
    function addLoctionPoint($data) {
        if ($this->db->insert('hotel_location_points', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
	/**
     * @method get_location_point() method used to get location  point hotel_location_points table
	 * @param  int $cuntryId  hold country id
	 * @param  int $cityId  hold city id
	 * @todo this method used to get location  point hotel_location_points table
	 * @return result in the form of an array|false
     */
    function get_location_point($cuntryId, $cityId) {
        $this->db->select('id,point_name');
        $this->db->where("country_id", $cuntryId);
        $this->db->where("city_id", $cityId);
        $this->db->order_by('point_name', 'ASC');
        $query = $this->db->get('hotel_location_points');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}