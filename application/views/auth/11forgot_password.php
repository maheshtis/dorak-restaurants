<div id="infoMessage"><?php echo $message;?></div>
<div class="lofin-part">
<div class="frm_contener login">
    	<div class="logo-login"><div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/forgot_password",'class="login"');?>

      <p>
      	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php echo form_input($identity);?>
      </p>

      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

<?php echo form_close();?>
</div>
</div>
</div>

