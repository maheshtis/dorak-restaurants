--
-- Triggers `hotel_bank_accounts`
--
DROP TRIGGER IF EXISTS `bank_account_delete_log`;
DELIMITER //
CREATE TRIGGER `bank_account_delete_log` AFTER DELETE ON `hotel_bank_accounts`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO bank_accounts_log
   ( hotel_id,
	 iban_code,
	 account_number,
	 account_name,
	 bank_name,
	 branch_name,
	 branch_code,
	 bank_ifsc_code,
	 swift_code,
	 status,
	 last_updated)
   VALUES
   ( OLD.hotel_id,
     OLD.iban_code,	
	 OLD.account_number,
	 OLD.account_name,
	 OLD.bank_name,
	 OLD.branch_name,
	 OLD.branch_code,
	 OLD.bank_ifsc_code,
	 OLD.swift_code,
	 OLD.status,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `bank_account_log`;
DELIMITER //
CREATE TRIGGER `bank_account_log` AFTER INSERT ON `hotel_bank_accounts`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO bank_accounts_log
   ( hotel_id,
	 iban_code,
	 account_number,
	 account_name,
	 bank_name,
	 branch_name,
	 branch_code,
	 bank_ifsc_code,
	 swift_code,
	 status,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.iban_code,	
	 NEW.account_number,
	 NEW.account_name,
	 NEW.bank_name,
	 NEW.branch_name,
	 NEW.branch_code,
	 NEW.bank_ifsc_code,
	 NEW.swift_code,
	 NEW.status,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `bank_log_after_update`;
DELIMITER //
CREATE TRIGGER `bank_log_after_update` AFTER UPDATE ON `hotel_bank_accounts`
 FOR EACH ROW BEGIN

IF NEW.last_updated <> OLD.last_updated
THEN
   -- Insert record into audit table
   INSERT INTO bank_accounts_log
   ( hotel_id,
	 iban_code,
	 account_number,
	 account_name,
	 bank_name,
	 branch_name,
	 branch_code,
	 bank_ifsc_code,
	 swift_code,
	 status,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.iban_code,	
	 NEW.account_number,
	 NEW.account_name,
	 NEW.bank_name,
	 NEW.branch_name,
	 NEW.branch_code,
	 NEW.bank_ifsc_code,
	 NEW.swift_code,
	 NEW.status,
     CURRENT_TIMESTAMP);
END IF;

END
//
DELIMITER ;