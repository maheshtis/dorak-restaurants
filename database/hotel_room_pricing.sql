--
-- Triggers `hotel_rooms_pricing`
--
DROP TRIGGER IF EXISTS `pricing_after_delete`;
DELIMITER //
CREATE TRIGGER `pricing_after_delete` AFTER DELETE ON `hotel_rooms_pricing`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_log
   ( pricing_id,
     hotel_id,
	 room_type,
	 inclusions,
	 curency_code,
	 max_adult,
	 period_from,
	 period_to,
	 max_child,
	 last_updated)
   VALUES
   ( OLD.pricing_id,
     OLD.hotel_id,
     OLD.room_type,	
	 OLD.inclusions,
	 OLD.curency_code,
	 OLD.max_adult,
	 OLD.period_from,
	 OLD.period_to,
	 OLD.max_child,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pricing_after_insert`;
DELIMITER //
CREATE TRIGGER `pricing_after_insert` AFTER INSERT ON `hotel_rooms_pricing`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_log
   ( pricing_id,
     hotel_id,
	 room_type,
	 inclusions,
	 curency_code,
	 max_adult,
	 period_from,
	 period_to,
	 max_child,
	 last_updated)
   VALUES
   ( NEW.pricing_id,
     NEW.hotel_id,
     NEW.room_type,	
	 NEW.inclusions,
	 NEW.curency_code,
	 NEW.max_adult,
	 NEW.period_from,
	 NEW.period_to,
	 NEW.max_child,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pricing_after_update`;
DELIMITER //
CREATE TRIGGER `pricing_after_update` AFTER UPDATE ON `hotel_rooms_pricing`
 FOR EACH ROW BEGIN

IF NEW.last_updated <> OLD.last_updated
THEN
   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_log
   ( pricing_id,
     hotel_id,
	 room_type,
	 inclusions,
	 curency_code,
	 max_adult,
	 period_from,
	 period_to,
	 max_child,
	 last_updated)
   VALUES
   ( NEW.pricing_id,
     NEW.hotel_id,
     NEW.room_type,	
	 NEW.inclusions,
	 NEW.curency_code,
	 NEW.max_adult,
	 NEW.period_from,
	 NEW.period_to,
	 NEW.max_child,
     CURRENT_TIMESTAMP);

END IF;

END
//
DELIMITER ;