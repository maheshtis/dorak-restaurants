--
-- Triggers `hotel_rooms_pricing_details`
--
DROP TRIGGER IF EXISTS `price_detail_after_delete`;
DELIMITER //
CREATE TRIGGER `price_detail_after_delete` AFTER DELETE ON `hotel_rooms_pricing_details`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_details_log
   ( pricing_id,
     market_id,
	 double_price,
	 triple_price,
	 quad_price,
	 breakfast_price,
	 half_board_price,
	 all_incusive_adult_price,
	 extra_adult_price,
	 extra_child_price,
	 extra_bed_price,
	 last_updated)
   VALUES
   ( OLD.pricing_id,
     OLD.market_id,
     OLD.double_price,	
	 OLD.triple_price,
	 OLD.quad_price,
	 OLD.breakfast_price,
	 OLD.half_board_price,
	 OLD.all_incusive_adult_price,
	 OLD.extra_adult_price,
	 OLD.extra_child_price,
	 OLD.extra_bed_price,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pricing_detail_after_insert`;
DELIMITER //
CREATE TRIGGER `pricing_detail_after_insert` AFTER INSERT ON `hotel_rooms_pricing_details`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_details_log
   ( pricing_id,
     market_id,
	 double_price,
	 triple_price,
	 quad_price,
	 breakfast_price,
	 half_board_price,
	 all_incusive_adult_price,
	 extra_adult_price,
	 extra_child_price,
	 extra_bed_price,
	 last_updated)
   VALUES
   ( NEW.pricing_id,
     NEW.market_id,
     NEW.double_price,	
	 NEW.triple_price,
	 NEW.quad_price,
	 NEW.breakfast_price,
	 NEW.half_board_price,
	 NEW.all_incusive_adult_price,
	 NEW.extra_adult_price,
	 NEW.extra_child_price,
	 NEW.extra_bed_price,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pricing_details_after_update`;
DELIMITER //
CREATE TRIGGER `pricing_details_after_update` AFTER UPDATE ON `hotel_rooms_pricing_details`
 FOR EACH ROW BEGIN

IF NEW.last_updated <> OLD.last_updated or NEW.pricing_id <> OLD.pricing_id
THEN
   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_details_log
   ( pricing_id,
     market_id,
	 double_price,
	 triple_price,
	 quad_price,
	 breakfast_price,
	 half_board_price,
	 all_incusive_adult_price,
	 extra_adult_price,
	 extra_child_price,
	 extra_bed_price,
	 last_updated)
   VALUES
   ( NEW.pricing_id,
     NEW.market_id,
     NEW.double_price,	
	 NEW.triple_price,
	 NEW.quad_price,
	 NEW.breakfast_price,
	 NEW.half_board_price,
	 NEW.all_incusive_adult_price,
	 NEW.extra_adult_price,
	 NEW.extra_child_price,
	 NEW.extra_bed_price,
     CURRENT_TIMESTAMP);

END IF;

END
//
DELIMITER ;